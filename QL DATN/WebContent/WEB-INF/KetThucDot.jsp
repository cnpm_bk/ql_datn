<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	// Lấy dữ liệu nhận được từ COntroller QuanLiHeThong
	Course pendingCourse = (Course) request.getAttribute("pendingCourse");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%/*@ include file="_headtags.jsp"*/%>
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>Kết Thúc Đợt Hoạt Động - Trang chờ...</title>
</head>
<body>
	<div class="container">
		<h1>
			<span class="text text-warning">Kết thúc đợt hướng dẫn đồ án</span>
		</h1>
		<hr>
		<div class="text text-info">
			Theo mặc định, mỗi đợt sẽ được tự động kết thúc khi đến hạn, hãy thận
			trọng với thao tác này!<br>Khi kết thúc đợt, chỉ các thao tác bị
			dừng, dữ liệu bảo toàn hiện trạng.
		</div>
		<h2>
			<span class="text text-primary">Thông tin cơ bản</span>
		</h2>
		<form action="" method="get" class="form-horizontal" role="form">
			<div class="form-group">
				<label class="col-sm-4 control-label">Mã đợt</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="courseId"
						value="<%=pendingCourse.getCourseId()%>" readonly>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label">Tên đợt</label>
				<div class="col-sm-8">
					<input type="text" class="form-control"
						value="<%=pendingCourse.getCourseName()%>" readonly>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label">Thời gian bắt đầu</label>
				<div class="col-sm-8">
					<input type="text" class="form-control"
						value="<%=pendingCourse.getStartDate()%>" readonly>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label">Thời gian kết thúc</label>
				<div class="col-sm-8">
					<input type="text" class="form-control"
						value="<%=pendingCourse.getStopDate()%>" readonly>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8">
					<input type="checkbox" name="confirmed" value="confirmed">
					Xác nhận kết thúc đợt này, chấp nhận tất cả các hoạt động liên quan
					của đợt này sẽ bị dừng trước thời hạn kết thúc của đợt. Chấp nhận
					thao tác này không thể phục hồi. <input type="hidden" name="act"
						value="stopCourseById">
				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-2 col-sm-offset-4">
					<input type="submit" class="btn btn-warning btn-block"
						value="Đồng
						ý kết thúc">
				</div>
				<div class="col-md-2 col-sm-offset-1">
					<a type="button" class="btn btn-primary btn-block"
						href="QuanLiHeThong?act=viewCourseById&courseId=<%=pendingCourse.getCourseId()%>">Quay
						về</a>
				</div>
			</div>
		</form>

	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>