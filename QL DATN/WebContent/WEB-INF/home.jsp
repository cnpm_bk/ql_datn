<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Notice"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Utils.SupportTool"%>
<%
	ArrayList<Notice> notices = (ArrayList<Notice>) request.getAttribute("notices");
	SupportTool tool = new SupportTool();
%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="_headtags.jsp"%>
<title>Trang chủ</title>
</head>
<body>
	<div class="container">
		<div class="text text-danger text-lg text-right">
			<mark>
				Thời gian hệ thống (yyyy-MM-dd HH:mm:ss): 
				<strong><%=tool.getCurrentDateTime().toString().substring(0, 19)%></strong>
			</mark>
		</div>
		<%
			if (session.getAttribute("userId") != null) {
		%>
		<p class="text-primary">
			Chào mừng <b><%=session.getAttribute("fullName")%></b> (<%=session.getAttribute("userId")%>).
			<%
			if (session.getAttribute("userType").equals("instructorAsManager")) {
					out.println(
							"<br><span>Bạn đang đăng nhập với tư cách <strong>Giáo Vụ</strong>. Để làm việc với các chức năng giảng viên vui lòng <a class=\"text text-danger\" href=\"QuanLiCaNhan?act=switchMode\">Chuyển sang chế độ giảng viên</a></span>");
				}
				if (session.getAttribute("userType").equals("managerAsInstructor")) {
					out.println(
							"<br><span>Bạn đang đăng nhập với tư cách <strong>Giảng Viên</strong>. Để làm việc với các chức năng giáo vụ vui lòng <a class=\"text text-danger\" href=\"QuanLiCaNhan?act=switchMode\">Chuyển sang chế độ giáo vụ</a></span>");
				}
				out.print("</p><hr class=\"divider\">");
			}
		%>
		
		<h3>
			<strong>Danh sách thông báo:</strong>
		</h3>
	</div>
	<%
		for (int i = 0; i < notices.size(); i++) {
			Notice notice = notices.get(i);
			boolean j = (i % 2 == 1);
	%>
	<div class="panel <%=j ? "panel-info" : "panel-primary"%>">
		<div class="panel-heading" title="Người gửi: <%=notice.getUserId()%>">
			<small><%=notice.getTime().substring(0, 10)%></small> [Khóa
			<%=notice.getCourseId()%>] <strong><%=notice.getTitle()%></strong>
		</div>
		<div class="panel-body">
			<%=notice.getContent()%>
		</div>
	</div>
	<%
		}
	%>

	<%@ include file="_footer.jsp"%>
</body>
</html>