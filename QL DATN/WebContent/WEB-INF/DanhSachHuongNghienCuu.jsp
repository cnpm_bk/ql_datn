<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.StudyField"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	// Lấy dữ liệu nhận được từ COntroller QuanLiCaNhan
	ArrayList<StudyField> studyFields = (ArrayList<StudyField>) request.getAttribute("studyFields");
	ArrayList<Integer> instructorStudyFieldIds = (ArrayList<Integer>) request
			.getAttribute("instructorStudyFieldIds");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="_headtags.jsp"%>
<title>Danh sách hướng nghiên cứu</title>
</head>
<body>
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3>
				Giảng viên: <strong><%=session.getAttribute("fullName")%></strong>
			</h3>
		</div>
		<div class="panel-body">
			<form action="QuanLiCaNhan" method="post">
				<input type="hidden" name="act" value="changeStudyField">
				<input type="hidden" name="change" value="yes">
				<input type="hidden" name="instructorId" value="<%=session.getAttribute("userId")%>">
				<table class="table">
					<tr>
						<th>STT</th>
						<th>Hướng NC</th>
						<th>Chi tiết</th>
						<th>Chọn</th>
					</tr>
					<%	// In ra danh sách tất cả các hướng nghiên cứu
						for (int i = 0, j = 0; i < studyFields.size(); i++) {
							StudyField studyField = studyFields.get(i);
							int studyFieldByInstructor = -1;
							if (j < instructorStudyFieldIds.size()) {
								studyFieldByInstructor = (int) instructorStudyFieldIds.get(j);
							}
							boolean chosen = false; // xác định hướng đã chọn
							if (studyField.getFieldId() == studyFieldByInstructor) {
								chosen = true; // khớp
								j++; 
							}
					%>
					<tr>
						<td><%=(i + 1) %></td>
						<td><%=studyField.getFieldName()%></td>
						<td><%=studyField.getFieldDetail()%></td>
						<td><input type="checkbox" name="chosenFields[]"
							value="<%=studyField.getFieldId() %>" <%=chosen ? "checked" : ""%>></td>
					</tr>
					<%
						}
					%>
					<tr>
						<td colspan="4"><input type="submit" value="Lưu lại"
							class="btn btn-success btn-block"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>