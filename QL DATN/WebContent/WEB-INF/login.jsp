<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	// Nếu không phải từ Controller gọi thì hiển thị trang trắng xóa!

	// Kiểm tra xem nó chuyển về từ Controller hay 
	if (session.getAttribute("uid") == null)
		response.sendRedirect("/login"); //Nhảy đến Controller "login"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/_headtags.jsp"%>
<title>Trang đăng nhập</title>
</head>
<body>
	<div class="container">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3>
					<span class="glyphicon glyphicon-log-in"></span> Đăng nhập hệ thống
				</h3>
			</div>
			<div class="panel-body">
				<form action="login" method="post">
					<div class="col-md-6 col-md-offset-3">
						<label class="control-label">Tài Khoản</label> <input
							class="form-control" type="text" name="userId">
					</div>
					<div class="col-md-6 col-md-offset-3">
						<label class="control-label">Mật Khẩu </label> <input
							class="form-control" type="password" name="password">
					</div>
					<div class="col-md-4 col-md-offset-4">
						<input type="hidden" name="act" value="login"> <input
							class="btn btn-danger btn-block" type="submit" value="Đăng Nhập">
					</div>
				</form>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/_footer.jsp"%>
</body>
</html>