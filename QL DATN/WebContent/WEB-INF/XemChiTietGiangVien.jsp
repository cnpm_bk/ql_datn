<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.User"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	ArrayList<User> studentList = (ArrayList<User>) request.getAttribute("studentList");
	String thisCourseId = (String) request.getAttribute("courseId");
	String instructorId = (String) request.getAttribute("instructorId");
	String instructorName = (String) request.getAttribute("instructorName");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Xem chi tiết giảng viên</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- style="margin-top: 5px; padding:1px; -->
	<div class="container-fluid">
		<%
			if (studentList.size() == 0) {
		%>
		<div class="row">
			<p>
				Giảng viên <b><%=instructorName%></b> không hướng dẫn sinh viên
				trong đợt này
		</div>
		<div class="row">
			<form action="QuanLiNguoiDung" method="get">
				<input type="hidden" name="act" value="deleteInstructor"> <input
					type="hidden" name="instructorId" value="<%=instructorId%>">
					<input type="hidden" name="courseId" value="<%=thisCourseId%>">
					<input class="btn btn-danger" type="submit" value="Xóa giảng viên" onmouseup="javascript:alert('Xóa giảng viên thành công!');">
			</form>
		</div>
		<%
			} else {
		%>
		<div class="row" style="margin-bottom: 10px;">Giảng viên <b><%=instructorName %></b></div>
		<table class="table table-bordered table-striped">
			<!-- HEAD -->
			<thead>
				<th>STT</th>
				<th>Mã SV</th>
				<th>Họ Tên</th>
				<th>Giới tính</th>
				<th>Email</th>
				<th>Số điện thoại</th>
				<th>Thay đổi giảng viên hướng dẫn</th>
			</thead>
			<!-- /HEAD -->

			<!-- BODY -->
			<%
				for (int i = 0; i < studentList.size(); i++) {
						User student = studentList.get(i);
			%>
			<tr>
				<td><%=i + 1%></td>
				<td><%=student.getUserId()%></td>
				<td><%=student.getFullName()%></td>
				<td><%=student.getGender()%></td>
				<td><%=student.getEmail() != null ? student.getEmail() : ""%></td>
				<td><%=student.getPhoneNumber() != null ? student.getPhoneNumber() : ""%></td>
				<td><a class="btn btn-info" type="button" href="QuanLiNguoiDung?act=viewListInstructorToChange&studentId=<%=student.getUserId() %>&courseId=<%=thisCourseId%>&instructorId=<%=instructorId%>">Thay
						đổi</a></td>
			</tr>
			<%
				}
			%>
		</table>
		<%
			}
		%>
		<div class="row">
			<div class="col-sm-1 col-sm-offset-11">
				<a
					href="QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=<%=thisCourseId%>"
					type="button" class="btn btn-warning">Quay lại</a>
			</div>
		</div>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>