<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Thông tin nhóm phát triển</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<div class="row">
			<h4>
				<span class="text text-primary"><strong>Hệ thống
						quản lí đồ án tốt nghiệp</strong></span> <small>Đề tài môn học: <strong>Công
						nghệ phần mềm</strong></small> <small class="text text-warning">GVHD: <strong>ThS.
						Lê Thị Mỹ Hạnh</strong></small>
			</h4>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-2">
				<i>Thông tin nhóm: </i>
				<ul>
					<li><strong>Nguyễn Bá Anh</strong>: nhóm trưởng</li>
					<li><strong>Huỳnh Kim Chính</strong>: thành viên</li>
					<li><strong>Nguyễn Phúc Hảo</strong>: thành viên</li>
					<li><strong>Ngô Trường Phạm Quang</strong>: thành viên</li>
					<li><strong>Nguyễn Công Y</strong>: thành viên</li>
				</ul>
			</div>
			<div class="col-md-10 col-md-offset-2">
				<i>Phân tích đặc tả: </i>
				<ul>
					<li>Kế hoạch dự án: <strong>Nguyễn Bá Anh</strong></li>
					<li>Đặc tả chức năng: <strong>Huỳnh Kim Chính, Nguyễn
							Phúc Hảo, Ngô Trường Phạm Quang, Nguyễn Công Y</strong></li>
				</ul>
			</div>
			<div class="col-md-10 col-md-offset-2">
				<i>Phân tích thiết kế: </i>
				<ul>
					<li>Thiết kế cơ sở dữ liệu: <strong>Nguyễn Bá Anh</strong></li>
					<li>Xây dựng chức năng: <strong>Nguyễn Bá Anh, Ngô
							Trường Phạm Quang, Nguyễn Công Y</strong></li>
					<li>Xây dựng giao diện: <strong>Nguyễn Bá Anh, Huỳnh
							Kim Chính, Nguyễn Phúc Hảo</strong></li>
				</ul>
			</div>
			<div class="col-md-10 col-md-offset-2">
				<i>Triển khai: </i>
				<ul>
					<li>Mẫu phát triển: <strong>Nguyễn Bá Anh</strong></li>
					<li>Lập trình: <strong>Nguyễn Bá Anh, Huỳnh Kim
							Chính, Nguyễn Phúc Hảo, Ngô Trường Phạm Quang, Nguyễn Công Y</strong></li>
				</ul>
			</div>
			<div class="col-md-10 col-md-offset-2">
				<i>Kiểm lỗi: </i>
				<ul>
					<li>Kiểm lỗi giao diện: <strong>Nguyễn Bá Anh, Huỳnh
							Kim Chính, Nguyễn Phúc Hảo</strong></li>
					<li>Kiểm lỗi chức năng: <strong>Ngô Trường Phạm
							Quang, Nguyễn Công Y</strong></li>
				</ul>
			</div>
		</div>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>