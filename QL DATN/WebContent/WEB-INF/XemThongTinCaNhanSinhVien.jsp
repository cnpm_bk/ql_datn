<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Student"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	// Lấy dữ liệu nhận được từ COntroller QuanLiCaNhan
	Student thisStudent = (Student) request.getAttribute("thisStudent");

	/* Tiếp tục trích xuất dữ liệu bằng <% =DỮ_LIỆU% > trực tiếp trong HTML từ thisStudent*/
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="_headtags.jsp"%>
<title>Quản lí cá nhân dành cho sinh viên</title>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Quản lí thông tin cá nhân sinh viên</h3>
			</div>
			<div class="panel-body">
				<form class="form-holizontal" role="form" action="QuanLiCaNhan"
					method="get">
					<input type="hidden" name="act" value="saveInfo">
					<div class="form-group">
						<label class="control-label col-md-2">Họ và tên</label>
						<div class="col-md-4">
							<input name="fullName" type="text" class="form-control"
								value="<%=thisStudent.getFullName()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Giới tính</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control" value="<%=thisStudent.getGender()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Tài khoản</label>
						<div class="col-md-4">
							<input name="userId" readonly="readonly" type="text"
								class="form-control" value="<%=thisStudent.getUserId()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Khoa</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control" value="<%=thisStudent.getDepartment()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Ngày sinh</label>
						<div class="col-md-4">
							<input name="dateOfBirth" type="text" class="form-control"
								value="<%=thisStudent.getDateOfBirth()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Số điện thoại</label>
						<div class="col-md-4">
							<input name="phoneNumber" type="text" class="form-control"
								value="<%=thisStudent.getPhoneNumber()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Địa chỉ</label>
						<div class="col-md-10">
							<input name="address" type="text" class="form-control"
								value="<%=thisStudent.getAddress()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Email 1</label>
						<div class="col-md-4">
							<input name="email" type="text" class="form-control"
								value="<%=thisStudent.getEmail()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Email 2</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control"
								value="<%=thisStudent.getUserId()%>@sv.dut.udn.vn">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-10 col-md-2">
							<input type="submit" class="btn btn-success btn-block"
								value="Lưu lại">
						</div>
					</div>
				</form>
			</div>
		</div>


		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Đổi mật khẩu</h3>
			</div>
			<div class="panel-body">
				<form class="form-holizontal" role="form" action="QuanLiCaNhan"
					method="post">
					<input type="hidden" name="act" value="changePassword">
					<div class="form-group">
						<label class="control-label col-md-2">Mật khẩu cũ</label>
						<div class="col-md-4">
							<input name="oldPassword" type="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Mật khẩu mới</label>
						<div class="col-md-4">
							<input name="newPassword" type="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Xác nhận mật khẩu</label>
						<div class="col-md-4">
							<input name="confirmedPassword" type="password"
								class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-2">
							<input name="" type="submit" class="btn btn-success btn-block"
								value="Đổi mật khẩu">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>