<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.QuantityTable"%>
<%@ page import="Model.BEAN.StudyField"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Utils.SupportTool"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	ArrayList<Instructor> instructorList = (ArrayList<Instructor>) request.getAttribute("instructorList");
	Course course = (Course) request.getAttribute("course");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Danh sách giảng viên trong đợt</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<p class="text text-warning">Lưu ý: Mặc định khi 1 giảng viên được
			nhập vào thì số lượng sinh viên mà giảng viên đó có thể hướng dẫn là
			01 sinh viên. Đặt số lượng sinh viên cho giảng viên bằng 0 sẽ loại bỏ
			giảng viên ra khỏi danh sách! Giảng viên nào đã có sinh viên đăng kí
			thì không được phép xóa.</p>
		<table class="table table-bordered table-striped">
			<!-- HEAD -->
			<thead>
				<th>STT</th>
				<th>Mã GV</th>
				<th>Họ Tên</th>
				<th>Trình độ</th>
				<th>Hướng NC</th>
				<th>Chi tiết</th>
				<th>SLSV</th>
				<th>Lưu SL</th>
			</thead>
			<!-- /HEAD -->

			<!-- BODY -->
			<%
				for (int i = 0; i < instructorList.size(); i++) {
					Instructor instructor = instructorList.get(i);
			%>
			<tr>
				<td><%=i + 1%></td>
				<td><%=instructor.getUserId()%></td>
				<td><%=instructor.getFullName()%></td>
				<td><%=instructor.getLevelOfStudy()%></td>
				<td><%=instructor.getAllStudyFieldNames()%></td>
				<td><a class="btn btn-info" type="button"
					href="QuanLiNguoiDung?act=viewInstructorDetails&instructorId=<%=instructor.getUserId()%>&courseId=<%=course.getCourseId()%>">Xem
						chi tiết</a></td>
				<form action="QuanLiNguoiDung" method="get">
					<td><%=instructor.getCurrentNumberOfStudentsByCourseId(course.getCourseId())%>
						/ <input style="height: 2em; width: 40px;" type="number"
						name="maxNumberOfStudents"
						value="<%=instructor.getMaxNumberOfStudentsByCourseId(course.getCourseId())%>">
					</td>
					<td><input type="hidden" name="act"
						value="changeMaxNumberOfStudents"> <input type="hidden"
						name="userCategory" value="instructor"> <input
						type="hidden" name="courseId" value="<%=course.getCourseId()%>">
						<input type="hidden" name="instructorId"
						value="<%=instructor.getUserId()%>"> <input
						class="btn btn-success btn-sm btn-block" type="submit" value="Lưu">
					</td>
				</form>
			</tr>
			<%
				}
			%>
		</table>
		<!-- /BODY -->

		<!-- BODY2 -->
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form action="QuanLiNguoiDung" method="get">
					<input type="hidden" name="act" value="addInstructors"> <input
						type="hidden" name="courseId" value="<%=course.getCourseId()%>">
					<% // Kiểm tra xem có thêm được giảng viên nữa không
						// nếu đợt còn kích hoạt và bây giờ chưa chính thức vào đợt
						boolean readyToAdd = true;
						SupportTool tool = new SupportTool();
						readyToAdd = course.isActived() && tool.checkValidTime(tool.getCurrentDateTime().toString(), course.getStartDate());
						%>
					<input type="submit" class="btn btn-success btn-block"
						value="Thêm Giảng Viên" <%=readyToAdd? "" : "disabled"%>>
				</form>
			</div>
		</div>
		<!-- /BODY2 -->
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>