<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Model.BEAN.Instructor"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	String instructorFullName = (String) request.getAttribute("instructorFullName");
	String instructorId = (String) request.getAttribute("instructorId");
	String studentId = (String) request.getAttribute("studentId");
	String courseId = (String) request.getAttribute("courseId");
	ArrayList<Instructor> instructorList = (ArrayList<Instructor>) request.getAttribute("instructorList");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Thay đổi giảng viên</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- style="margin-top: 5px; padding:1px; -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">Giảng viên hướng dẫn hiện tại</div>
			<div>
				<b><%=instructorFullName%></b>
			</div>
		</div>
		<table class="table table-bordered table-striped">
			<!-- HEAD -->
			<thead>
				<th>STT</th>
				<th>Mã GV</th>
				<th>Họ Tên</th>
				<th>Trình độ</th>
				<th>Hướng NC</th>
				<th>SLSV</th>
				<th>Chọn</th>
			</thead>
			<!-- /HEAD -->

			<!-- BODY -->
			<%
				for (int i = 0; i < instructorList.size(); i++) {
					Instructor instructor = instructorList.get(i);
			%>
			<tr>
				<td><%=i + 1%></td>
				<td><%=instructor.getUserId()%></td>
				<td><%=instructor.getFullName()%></td>
				<td><%=instructor.getLevelOfStudy()%></td>
				<td><%=instructor.getAllStudyFieldNames()%></td>
				<td><%=instructor.getCurrentNumberOfStudentsByCourseId(courseId)%>
					/ <%=instructor.getMaxNumberOfStudentsByCourseId(courseId)%></td>
				<%
				String disabled = "";
				if (instructorId.equals(instructor.getUserId()) || (instructor.getCurrentNumberOfStudentsByCourseId(courseId) == instructor.getMaxNumberOfStudentsByCourseId(courseId)))
					disabled = "disabled";
				%>
				<td>
					<form action="QuanLiNguoiDung" method="get">
						<input type="hidden" name="act" value="changeInstructor">
						<input type="hidden" name="studentId" value="<%=studentId%>">
						<input type="hidden" name="courseId" value="<%=courseId%>">
						<input type="hidden" name="oldInstructorId"
							value="<%=instructorId%>"> <input type="hidden"
							name="newInstructorId" value="<%=instructor.getUserId()%>">
						<input type="submit" class="btn btn-info" value="Chọn"
							<%=disabled%>>
					</form>
			</tr>
			<%
				}
			%>
		</table>
		<div class="row">
			<div class="col-sm-1 col-sm-offset-11">
				<a
					href="QuanLiNguoiDung?act=viewInstructorDetails&instructorId=<%=instructorId%>&courseId=<%=courseId%>"
					type="button" class="btn btn-warning">Quay lại</a>
			</div>
		</div>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>