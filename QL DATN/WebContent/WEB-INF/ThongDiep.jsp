<!-- Trang hiển thị thông điệp -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Utils.Message"%>
<%
	// Thông tin thông điệp
	Message message = (Message) request.getAttribute("message");
%>
<!DOCTYPE html>
<html lang="vi">
<head>
<%
	if (!"".equals(message.getRedirectURL())) {
		out.println("<meta http-equiv=\"refresh\" content=\"10; url="+message.getRedirectURL()+"\">");
	}
%>
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>Trang Quản lí hệ thống dành cho Giáo Vụ / Quản lí</title>
</head>
<body>
	<!-- Nguyễn Bá Anh -->
	<!--Container-->
	<div class="container">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h4 style="margin: auto center;">
					<span class="glyphicon glyphicon-info-sign"></span> Thông điệp
				</h4>
			</div>
			<div class="panel-body">
				<%=message.getMessage()%>
				<%
					if (!"".equals(message.getRedirectURL())) {
				%>
				<hr>
				<span><a href="<%=message.getRedirectURL()%>" type="button"
					class="btn btn-info btn-sm"><span
						class="glyphicon glyphicon-chevron-left"></span> <%=message.getAction()%></a></span>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<!--/Container-->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<%@ include file="_footer.jsp"%>
</body>
</html>
