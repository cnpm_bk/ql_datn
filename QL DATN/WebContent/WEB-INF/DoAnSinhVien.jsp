<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.Project"%>
<%@ page import="Model.BEAN.Progress"%>
<%@ page import="Model.BEAN.CoursePlan"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Utils.SupportTool"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null) {
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	}
	// Danh sách đồ án
	ArrayList<Project> projectList = (ArrayList<Project>) request.getAttribute("projectList");
	// Thông tin đợt
	Course thisCourse = (Course) request.getAttribute("thisCourse");
	// Thông tin giảng viên (nếu có)
	Instructor thisInstructor = (Instructor) request.getAttribute("thisInstructor");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Trang đồ án cá nhân sinh viên</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<%
			// Tìm đồ án của đợt nào đang xem
			Project thisProject = null;
			findThisProject: for (Project p : projectList) {
				if (thisCourse.getCourseId().equals(p.getCourseId())) {
					thisProject = p;
					break findThisProject;
				}
			}
		%>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h4>Trang đồ án cá nhân sinh viên</h4>
				<div class="btn-group" role="group">
					<button
						class="btn <%=thisProject == null ? "btn-danger" : "btn-info"%> btn-block dropdown-toggle"
						type="link" data-toggle="dropdown">
						<%=thisProject == null ? "Không có đồ án đang hoạt động"
					: ("Đợt " + thisProject.getCourseId() + ": " + thisProject.getProjectTitle()
							+ "&nbsp;<span class=\"caret\"></span>")%>

					</button>
					<%
						if (projectList != null) {
							out.println("<ul class=\"dropdown-menu\">");
							for (Project project : projectList) {
								out.println("<li><a href=\"QuanLiDoAn?act=viewMyProject&courseId=" + project.getCourseId()+ "\">" + project.getCourseId() + ": "
										+ project.getProjectTitle() + "</a></li>");
							}
							out.println("</ul>");
						}
					%>

				</div>
			</div>
			<div class="panel-body">
				<form action="QuanLiDoAn" method="get">
					<input type="hidden" name="act" value="updateMyProject"> <input
						type="hidden" name="courseId"
						value="<%=thisCourse.getCourseId()%>">
					<h4>
						<strong>Thông tin chung</strong>
					</h4>

					<div class="form-group">
						<label>Tên đồ án:</label><input class="form-control" type="text"
							name="projectTitle" value="<%=thisProject.getProjectTitle()%>">
					</div>
					<div class="form-group">
						<label>Mô tả:</label><input class="form-control" type="text"
							name="projectDescription"
							value="<%=thisProject.getProjectDescription()%>">
					</div>
					<div class="form-group">
						<label>Giảng viên hướng dẫn:</label> <i><%=thisInstructor.getLevelOfStudy()%></i>
						<%=thisInstructor.getFullName()%>
						<input type="hidden" name="instructorId"
							value="<%=thisInstructor.getUserId()%>">
					</div>
					<div class="form-group">
						<label>Tình trạng: </label> <span class="well well-sm">
							Tiến Độ <%=thisProject.getCurrentRateOfProgress()%> / <%=thisCourse.getProgressRate()%>
						</span> &nbsp;
						<%
							// Nếu đã hết thời gian của đợt
							SupportTool tool = new SupportTool();
							String status = "running";
							// Đã kết thúc
							if (tool.checkValidTime(thisCourse.getStopDate(), String.valueOf(tool.getCurrentDateTime()))) {
								out.print("<span class=\"well well-sm\">Đã kết thúc</span>");
								status = "ended";
							}
							// Đang hoạt động
							if (tool.checkValidTime(thisCourse.getStartDate(), String.valueOf(tool.getCurrentDateTime()))) {
								// Nếu còn kích hoạt
								if (thisProject.isActived()) {
									out.print("<span class=\"well well-sm\">Đang hoạt động</span>");
									status = "running";
								} else {
									out.print("<span class=\"well well-sm\">Đang bị cấm</span>");
									status = "blocked";
								}
							} else { // Đang chờ
								out.print("<span class=\"well well-sm\">Chưa sẵn sàng</span>");
								status = "pending";
							}
						%>
					</div>
					<div class="form-group">
						<label>Lịch đồ án:</label>
						<ul>
							<%
								ArrayList<CoursePlan> plans = thisCourse.getPlan();
								for (CoursePlan p : plans) {
									if (p.getOrder() == 0) {
										out.print("<li>Lịch nộp đồ án: ");
									} else {
										out.print("<li>Lịch nộp tiến độ lần " + p.getOrder() + ": ");
									}
									out.print(p.getBeginReportDate() + " - " + p.getEndReportDate());
								}
							%>
						</ul>
					</div>
					<%
						if ("running".equals(status)) {
					%>
					<div class="form-group">
						<div class="form-group">
							<div class="col-md-2 col-md-offset-2">
								<input type="submit" value="Lưu"
									class="btn btn-success btn-block">
							</div>
							<div class="col-md-2 col-md-offset-1">
								<%
									if (thisProject.getCurrentRateOfProgress() < thisCourse.getProgressRate()) {
											if (status.equals("running")) {
								%>
								<a href="" class="btn btn-success btn-block">Nộp tiến độ lần
									<%=thisProject.getCurrentRateOfProgress() + 1%></a>
								<%
									} else {
								%>
								<span class="btn btn-warning btn-block" disabled>Nộp tiến
									độ</span>
								<%
									}
										}
								%>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<%
									if (thisProject.getCurrentRateOfProgress() == thisCourse.getProgressRate()) {
								%>
								<a href="" class="btn btn-success btn-block">Nộp đồ án</a>
								<%
									} else {
								%>
								<span class="btn btn-warning btn-block" disabled>Nộp đồ
									án</span>
								<%
									}
								%>
							</div>
						</div>
					</div>
					<%
						}
					%>
				</form>
			</div>

			<div class="list-group">
				<div class="list-group-item list-group-item-success">
					<h4>
						<strong>Báo cáo chi tiết</strong>
					</h4>
				</div>
				<%
					if (!thisProject.getProgresses().isEmpty()) {
						ArrayList<Progress> thisProgress = new ArrayList<Progress>();
						for (Progress progress : thisProject.getProgresses()) {
							if (thisProject.getCourseId().equals(progress.getCourseId())) {
								thisProgress.add(progress);
							}
						}
						// In ra các thông tin cơ bản của tiến độ
						for (Progress progress : thisProgress) {
				%>
				<div class="list-group-item">
					<h4 class="list-group-item-heading">
						[<%=progress.getRateOfProgress() > 0 ? progress.getRateOfProgress() : "NỘP ĐA"%>]
						<strong><%=progress.getReportTitle()%></strong> <small> [<%=progress.isApprovedByStudent() ? "Đã nộp" : "Bản nháp"%>]
							[<%=progress.isApprovedByInstructor() ? "GVHD đã xác nhận" : "GVHD chưa xác nhận"%>]
							[<%=progress.isApprovedByManager() ? "Giáo vụ đã xác nhận" : "Giáo vụ chưa xác nhận"%>]
						</small>
					</h4>
					<h5>
						Link: <a href="<%=progress.getOptionalLink()%>"><%=progress.getOptionalLink()%></a>
					</h5>
					<blockquote class="list-group-item-text"><%=progress.getReportContent()%></blockquote>
				</div>
				<%
					}
					} else {
				%>
				<div class="list-group-item list-group-item-warning">
					<h4 class="list-group-item-heading">Chưa có báo cáo chi tiết
						nào!</h4>
					<blockquote class="list-group-item-text">Thông tin sẽ
						được cập nhật khi sinh viên nộp tiến độ.</blockquote>
				</div>
				<%
					}
				%>
			</div>
		</div>
	</div>
	<%
		
	%>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>