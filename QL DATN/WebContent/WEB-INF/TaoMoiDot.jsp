<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%
	//@ include file="_headtags.jsp"
%>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<title>Tạo mới đợt hướng dẫn đồ án</title>
</head>
<body>
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="text text-success">Tạo mới đợt</h3>
		</div>
		<div class="panel-body">
			<form action="QuanLiHeThong" method="get" class="form-horizontal"
				role="form">
				<input type="hidden" name="act" value="createNewCourse">

				<div class="form-group col-sm-2">
					<label class="control-label">Mã đợt</label> <input type="text"
						name="courseId" class="form-control" id="inputEmail3"
						placeholder="[Kxx]">
				</div>
				<div class="form-group col-sm-10">
					<label class="control-label">Tên đợt</label> <input type="text"
						name="courseName" class="form-control" id="inputEmail3"
						placeholder="Đợt hướng dẫn đồ án kì X năm học XXXX-XXXX">
				</div>

				<div class="form-group col-sm-6">
					<label class="control-label">Thời gian bắt đầu (không thể
						đổi sau khi tạo)</label> <input type="text" name="startDate"
						class="form-control"
						placeholder="yyyy-MM-dd HH:mm:ss.nnnnnn" id="startDate"
						onClick="$('#startDate').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
				</div>
				<div class="form-group col-sm-6">
					<label class="control-label">Thời gian kết thúc (không thể
						đổi sau khi tạo)</label> <input type="text" name="stopDate"
						class="form-control" id="stopDate"
						placeholder="yyyy-MM-dd HH:mm:ss.nnnnnn"
						onClick="$('#stopDate').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
				</div>

				<div class="form-group col-sm-6">
					<label class="control-label">Thời gian bắt đầu đăng kí (dự
						kiến)</label> <input type="text" name="beginRegistrationTime"
						class="form-control" id="beginRegistrationTime"
						placeholder="yyyy-MM-dd HH:mm:ss.nnnnnn"
						onClick="$('#beginRegistrationTime').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
				</div>
				<div class="form-group col-sm-6">
					<label class="control-label">Thời gian kết thúc đăng kí (dự
						kiến)</label> <input type="text" name="endRegistrationTime"
						class="form-control" id="endRegistrationTime"
						placeholder="yyyy-MM-dd HH:mm:ss.nnnnnn"
						onClick="$('#endRegistrationTime').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
				</div>

				<div class="form-group col-sm-12">
					<label class="control-label">Số lần báo cáo tiến độ</label> <input
						type="number" name="progressRate" class="form-control"
						id="inputEmail3" value="2">
				</div>
				<div class="form-group col-sm-12">
					<input type="checkbox" name="confirmed" value="confirmed">
					<label class="control-label">Xác nhận.</label> Chú ý: Sau khi tạo
					mới đợt, cần nhập danh sách Giảng Viên và Sinh Viên, cập nhật lịch
					đăng kí (nếu cần), cập nhật lịch báo cáo tiến độ và lịch nộp đồ án.
				</div>

				<div class="form-group col-sm-12">
					<button type="submit" class="btn btn-success btn-block">Tạo
						mới</button>
				</div>
			</form>
		</div>
	</div>
	<%@ include file="_footer.jsp"%>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.vi.js"
		charset="UTF-8"></script>
</body>
</html>