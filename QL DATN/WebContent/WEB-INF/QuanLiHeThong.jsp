﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="Model.BEAN.CoursePlan"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	// Lấy dữ liệu nhận được từ COntroller QuanLiHeThong
	Course thisCourse = (Course) request.getAttribute("thisCourse");

	// Lấy danh sách các đợt hướng dẫn
	ArrayList<Course> basicCourseList = (ArrayList<Course>) request.getAttribute("basicCourseList");
%>
<!DOCTYPE html>
<html lang="vi">
<head>
<%
	/*@ include file="_headtags.jsp"*/
%>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<title>Trang Quản lí hệ thống dành cho Giáo Vụ / Quản lí</title>
</head>
<body>
	<!--Container-->
	<div class="container">
		<%
			if (thisCourse.getCourseId() == null) { // không được so sánh (thisCourse == null)
				out.print("Chưa có đợt nào!");
				out.print("<a type=\"button\" class=\"btn btn-default btn-block\""
						+ "href=\"QuanLiHeThong?act=createNewCourse\"><strong>Tạo mới</strong></a>");
			} else {
		%>
		<!-- Tiêu đề -->
		<h1>
			<strong>Quản lí hệ thống</strong>
		</h1>
		<!-- /Tiêu đề -->
		<!-- Danh sách đợt -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="row">
					<strong class="col-md-10">Chọn đợt hướng dẫn</strong> <span
						class="col-md-2"><a type="button"
						class="btn btn-default btn-block"
						href="QuanLiHeThong?act=createNewCourse"><strong>Tạo
								mới</strong></a></span>
				</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="dropdown">
							<button class="btn btn-info btn-block dropdown-toggle"
								data-toggle="dropdown">
								Đợt đang hoạt động<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<!-- In ra các đợt đang hoạt động -->
								<%
									for (int i = 0; i < basicCourseList.size(); i++) {
											Course basicCourse = basicCourseList.get(i);
											if (basicCourse.isActived()) {
												out.println("<li><a href=\"QuanLiHeThong?act=viewCourseById&courseId="
														+ basicCourse.getCourseId() + "\">[" + basicCourse.getCourseId() + "] "
														+ basicCourse.getCourseName() + "</a></li>");
											}

										}
								%>
								<!-- /In ra các đợt đang hoạt động -->
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="dropdown">
							<button class="btn btn-warning btn-block dropdown-toggle"
								type="link" data-toggle="dropdown">
								Đợt đã kết thúc<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<!-- In ra các đợt đã kết thúc -->
								<%
									for (int i = 0; i < basicCourseList.size(); i++) {
											Course basicCourse = basicCourseList.get(i);
											if (!basicCourse.isActived()) {
												out.println("<li><a href=\"QuanLiHeThong?act=viewCourseById&courseId="
														+ basicCourse.getCourseId() + "\">[" + basicCourse.getCourseId() + "] "
														+ basicCourse.getCourseName() + "</a></li>");
											}

										}
								%>
								<!-- /In ra các đợt đã kết thúc -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Danh sách đợt -->

		<!-- Chi tiết đợt -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>
					[<%=thisCourse.getCourseId()%>] <strong><%=thisCourse.getCourseName()%></strong>
				</h3>
			</div>
			<div class="panel-body">
				<!--Thông tin chung-->
				<div class="row">
					<div class="col-md-3">
						<label>Ngày bắt đầu đợt</label> <input class="form-control"
							type="text" name=""
							value="<%=thisCourse.getStartDate() == null ? "" : thisCourse.getStartDate().substring(0, 10)%>"
							disabled="disabled">
					</div>
					<div class="col-md-3">
						<label>Ngày kết thúc đợt</label> <input class="form-control"
							type="text" name=""
							value="<%=thisCourse.getStopDate() == null ? "" : thisCourse.getStopDate().substring(0, 10)%>"
							disabled="disabled">
					</div>
					<div class="col-md-6">
						<form action="QuanLiHeThong">
							<label
								ondblclick="document.getElementById('changeRegistrationTime').removeAttribute('disabled'); document.getElementById('updateRegistrationTime').setAttribute('name','updateRegistrationTime');">Khoảng
								thời gian đăng kí</label>
							<div class="input-group">
								<input class="form-control" id="beginRegistrationTime"
									type="text" name="beginRegistrationTime"
									value="<%=thisCourse.getBeginRegistrationTime() == null ? ""
						: thisCourse.getBeginRegistrationTime().substring(0, 19)%>"
									<%=thisCourse.isActived() ? "" : "readonly"%>
									onFocus="$('#beginRegistrationTime').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
								<span class="input-group-btn" style="width: 0;"></span> <input
									class="form-control" type="text" id="endRegistrationTime"
									name="endRegistrationTime"
									value="<%=thisCourse.getEndRegistrationTime() == null ? ""
						: thisCourse.getEndRegistrationTime().substring(0, 19)%>"
									<%=thisCourse.isActived() ? "" : "readonly"%>
									onFocus="$('#endRegistrationTime').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
								<span class="input-group-btn"> <input type="hidden"
									name="act" value="changeRegistrationTime"> <input
									type="hidden" name="courseId"
									value="<%=thisCourse.getCourseId()%>"> <input
									type="hidden" name="" id="updateRegistrationTime" value="yes">
									<button class="btn btn-success" type="submit"
										id="changeRegistrationTime"
										<%=thisCourse.isActived() ? "" : "disabled"%>
										<%=thisCourse.getBeginRegistrationTime() == null ? "" : "disabled"%>>Lưu
										lại</button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<hr>
				<!-- Dữ liệu đợt -->
				<div class="row">
					<div class="col-md-3">
						<label>Giảng viên</label>
						<div class="input-group">
							<span class="input-group-addon">SLGV</span> <input
								class="form-control" type="number" name=""
								value="<%=thisCourse.getNumberOfInstructors()%>"
								disabled="disabled"> <span class="input-group-btn">
								<button class="btn btn-info" type="button" data-toggle="modal"
									data-target="#instructorList">Danh sách</button>
							</span>
						</div>
						<!-- Bảng hiển thị dữ liệu khi nhấn nút -->
						<div id="instructorList" class="modal fade" role="dialog">
							<div class="modal-dialog modal-lg"
								style="width: 96%; height: 400px;">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">
											Danh sách giảng viên trong đợt [<%=thisCourse.getCourseId()%>]
										</h4>
									</div>
									<div class="modal-body">
										<iframe
											src="QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=<%=thisCourse.getCourseId()%>"
											style="border: none; width: 100%; height: 300px;"></iframe>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" onFocus="location.reload();">Đóng</button>
									</div>
								</div>

							</div>
						</div>
						<!-- /Bảng hiển thị dữ liệu -->
					</div>
					<div class="col-md-3">
						<label>Sinh viên</label>
						<div class="input-group">
							<span class="input-group-addon">SLSV</span> <input
								class="form-control" type="number" name=""
								value="<%=thisCourse.getNumberOfStudents()%>"
								disabled="disabled"> <span class="input-group-btn">
								<button class="btn btn-info" type="button" data-toggle="modal"
									data-target="#studentList">Danh sách</button>
							</span>
						</div>
						<!-- Bảng hiển thị dữ liệu khi nhấn nút -->
						<div id="studentList" class="modal fade" role="dialog">
							<div class="modal-dialog modal-lg"
								style="width: 96%; height: 400px;">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">
											Danh sách sinh viên trong đợt [<%=thisCourse.getCourseId()%>]
										</h4>
									</div>
									<div class="modal-body">
										<iframe
											src="QuanLiNguoiDung?act=viewUserList&userCategory=student&courseId=<%=thisCourse.getCourseId()%>"
											style="border: none; width: 100%; height: 300px;"></iframe>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" onFocus="location.reload();">Đóng</button>
									</div>
								</div>

							</div>
						</div>
						<!-- /Bảng hiển thị dữ liệu -->
					</div>
					<div class="col-md-3">
						<form action="QuanLiHeThong" method="get">
							<input type="hidden" name="act" value="changeProgressRate">
							<input type="hidden" name="courseId"
								value="<%=thisCourse.getCourseId()%>"> <label>Số
								lần báo cáo</label>
							<div class="input-group">
								<input class="form-control" type="number" name="progressRate"
									value="<%=thisCourse.getProgressRate()%>"
									<%=thisCourse.isActived() ? "" : "disabled"%>> <span
									class="input-group-btn">
									<button class="btn btn-success" type="submit"
										<%=thisCourse.isActived() ? "" : "disabled"%>>Lưu lại</button>
								</span>
							</div>
						</form>
					</div>
					<div class="col-md-3">
						<form action="QuanLiHeThong" method="get">
							<input type="hidden" name="act" value="stopCourseById"> <input
								type="hidden" name="courseId"
								value="<%=thisCourse.getCourseId()%>"> <label>Kết
								thúc đợt</label>
							<div class="input-group">
								<span class="input-group-addon"> Xác nhận </span> <span
									class="input-group-addon"> <input type="checkbox"
									name="confirmed" value="confirmed"
									<%=thisCourse.isActived() ? "" : "disabled"%>>
								</span> <span class="input-group-btn"> <input
									class="btn btn-danger btn-block" type="submit" value="Kết thúc"
									<%=thisCourse.isActived() ? "" : "disabled"%>>
								</span>
							</div>
						</form>
					</div>
				</div>

				<div class="divider">
					<hr>
				</div>
				<!-- Kế hoạch báo cáo-->
				<div class="row">
					<div class="col-md-6">
						<label>Kế hoạch báo cáo tiến độ</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-1 col-md-offset-1">
						<label>Lần</label>
					</div>
					<div class="col-md-3">
						<label>Thời gian bắt đầu</label>
					</div>
					<div class="col-md-3">
						<label>Thời gian kết thúc</label>
					</div>
					<div class="col-md-1">
						<label>T.Báo</label>
					</div>
				</div>
				<!-- Lần báo cáo -->
				<%
					ArrayList<CoursePlan> plans = thisCourse.getPlan();
						int size = thisCourse.getProgressRate();
						boolean activeNext = false; // Mở khóa đặt lịch cho lần kế tiếp nếu lần hiện tại đã đặt xong
						for (int i = 0; i < size; i++) {
							CoursePlan plan = null; // giả sử ban đầu chưa có kế hoạch nào
							if (plans != null && i < plans.size()) {
								plan = plans.get(i); // Có kế hoạch đã đặt từ trước thì lấy ra
							}
				%>
				<div class="row">
					<form action="QuanLiHeThong" method="get">
						<input type="hidden" name="act" value="updateCoursePlan">
						<input type="hidden" name="courseId"
							value="<%=thisCourse.getCourseId()%>">
						<input type="hidden" name="update" value="<%=(plan != null ? "yes" : "no")%>">
						<div class="col-md-1 col-md-offset-1">
							<input class="form-control" type="number" name="order"
								value="<%=(i + 1)%>" readonly>
						</div>
						<div class="col-md-3">
							<input class="form-control" type="text" name="beginReportDate"
								id="beginReportDate<%=i%>"
								value="<%=(plan != null ? plan.getBeginReportDate() : "")%>"
								onFocus="$('#beginReportDate<%=i%>').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
						</div>
						<div class="col-md-3">
							<input class="form-control" type="text" name="endReportDate"
								id="endReportDate<%=i%>"
								value="<%=(plan != null ? plan.getEndReportDate() : "")%>"
								onFocus="$('#endReportDate<%=i%>').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
						</div>
						<div class="col-md-1">
							<div class="checkbox text text-center">
								<input type="checkbox" name="" value="" checked>
							</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-success btn-block"
								type="submit" value="Lưu lại"
								<%=thisCourse.isActived() ? "" : "disabled"%>>
						</div>
					</form>
				</div>
				<br>
				<%
					}
				%>
				<!-- /Lần báo cáo -->

				<!-- /Kế hoạch báo cáo-->

				<!-- Kế hoạch nộp đồ án-->
				<hr>
				<div class="row">
					<div class="col-md-6">
						<label>Kế hoạch nộp đồ án</label>
					</div>
				</div>
				<div class="row">
					<form action="QuanLiHeThong" method="get">
						<input type="hidden" name="act" value="updateCoursePlan">
						<input type="hidden" name="courseId"
							value="<%=thisCourse.getCourseId()%>"> <input
							type="hidden" name="order" value="0" readonly>
						<!-- order=0 là lịch nộp đồ án -->
						<div class="col-md-3 col-md-offset-2">
							<input class="form-control" type="text" name="beginReportDate"
								id="beginReportDate_" value=""
								onFocus="$('#beginReportDate_').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
						</div>
						<div class="col-md-3">
							<input class="form-control" type="text" name="endReportDate"
								id="endReportDate_" value=""
								onFocus="$('#endReportDate_').datetimepicker({format:'yyyy-mm-dd hh:ii:ss',startDate:'2015-01-01 00:00'});">
						</div>
						<div class="col-md-1">
							<div class="checkbox text text-center">
								<input type="checkbox" name="" value="" checked>
							</div>
						</div>
						<div class="col-md-2">
							<input class="form-control btn btn-success btn-block"
								type="submit" value="Lưu lại"
								<%=thisCourse.isActived() ? "" : "disabled"%>>
						</div>
					</form>
				</div>
				<!-- /Kế hoạch nộp đồ án-->
			</div>
		</div>
		<!-- /Chi tiết đợt -->
		<%
			}
		%>

	</div>
	<!--/Container-->
	<%@ include file="_footer.jsp"%>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.vi.js"
		charset="UTF-8"></script>
</body>
</html>