<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	Course course = (Course) request.getAttribute("course");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Thêm sinh viên</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h4>
					Nhập danh sách sinh viên đợt
					<mark><%=course.getCourseId()%>:
						<%=course.getCourseName()%></mark>
				</h4>
			</div>
			<div class="panel-body">
				<p class="text text-info">Chú ý: Nhập sinh viên từ file XLSX,
					theo bố trí như sau:...</p>
				<p class="text text-warning">Nếu sinh viên đã tồn tại sẵn trong
					hệ thống thì sinh viên đó vẫn được giữ nguyên trạng và thêm vào
					đợt. Nếu sinh viên chưa tồn tại trong hệ thống thì sẽ nạp dữ liệu
					từ file Excel và thêm vào hệ thống trước khi thêm vào danh sách
					sinh viên của đợt.</p>
				<script type="text/javascript">
					function ValidateExtension() {
						var allowedFiles = [ ".xls" ];
						var fileUpload = document.getElementById("fileUpload");
						var errorLabel = document.getElementById("errorLabel");
						var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
								+ allowedFiles.join('|') + ")$");
						if (!regex.test(fileUpload.value.toLowerCase())) {
							errorLabel.innerHTML = "Vui lòng upload đúng định dạng: <b>"
									+ allowedFiles + "</b>.";
							return false;
						}
						errorLabel.innerHTML = "";
						return true;
					}
				</script>
				<form
					action="QuanLiNguoiDung?act=addStudents&courseId=<%=course.getCourseId()%>&confirmed=confirmed"
					method="post" enctype="multipart/form-data">
					<label>Chọn
						file: </label><input type="file" name="file" id="fileUpload"> <span
						id="errorLabel" style="color: red;"></span><br> <a
						class="btn btn-default"
						href="QuanLiNguoiDung?act=viewUserList&userCategory=student&courseId=<%=course.getCourseId()%>"><span
						class="glyphicon glyphicon-chevron-left"></span> Quay về DSSV </a> <input
						type="submit" class="btn btn-info" value="Upload File"
						onclick="return ValidateExtension()">
				</form>
			</div>
		</div>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>