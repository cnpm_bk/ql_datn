<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Project"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BO.InstructorBO"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	String thisCourseId =(String) request.getAttribute("courseId");
	Project project = (Project) request.getAttribute("project");
	byte maxProgressRate = (byte) request.getAttribute("maxProgressRate");
	String instructorFullName = (String) request.getAttribute("instructorFullName");
	String studentFullName = (String) request.getAttribute("studentFullName");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Xem chi tiết sinh viên</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!-- style="margin-top: 5px; padding:1px; -->
	<div class="container-fluid">
		<%
			if (project != null) {
		%>
		<div class="row" style="margin-top: 5px; padding: 1px;">
			<p>Họ và tên sinh viên: <b><%=studentFullName %></b>
		</div>
		<div class="row" style="margin-top: 5px; padding: 1px;">
			<div class="col-sm-3">
				<b>Giảng viên hướng dẫn</b>
			</div>
			<div class="col-sm-3">
				<input type="text" readonly="readonly" class="form-control"
					value="<%=instructorFullName%>">
			</div>
			<div class="col-sm-3">
				<b>Mã đợt</b>
			</div>
			<div class="col-sm-3">
				<input type="text" readonly="readonly" class="form-control"
					value="<%=project.getCourseId()%>">
			</div>
		</div>
		<div class="row" style="margin-top: 5px; padding: 1px;">
			<%
				String approvedByManager 	= project.isApprovedByManager() == true ? "checked" : "";
				String actived 				= project.isActived() == true ? "checked" : "";
			%>
			<div class="col-sm-3">
				<b>Hiệu lực</b>
			</div>
			<div class="col-sm-3">
				<input type="checkbox" disabled="disabled" value="" <%=actived%>>
			</div>
			<div class="col-sm-3">
				<b>Giáo vụ xác nhận</b>
			</div>
			<div class="col-sm-3">
				<input type="checkbox" disabled="disabled" value=""
					<%=approvedByManager%>>
			</div>
		</div>
		<div class="row" style="margin-top: 8px; padding: 1px;">
			<div class="col-sm-3">
				<b>Tên đồ án</b>
			</div>
			<div class="col-sm-3">
				<input type="text" readonly="readonly" class="form-control"
					value="<%=project.getProjectTitle() != null ? project.getProjectTitle() : ""%>">
			</div>
			<div class="col-sm-3">
				<b>Tiến độ hiện tại</b>
			</div>
			<div class="col-sm-3">
				<input type="text" readonly="readonly" class="form-control"
					value="<%=project.getCurrentRateOfProgress()%>/<%=maxProgressRate%>">
			</div>
		</div>
		<div class="row" style="margin-top: 8px; padding: 1px;">
			<div class="col-sm-3">
				<b>Mô tả đồ án</b>
			</div>
			<div class="col-sm-9">
				<input type="text" readonly="readonly" class="form-control"
					value="<%=project.getProjectDescription() != null ? project.getProjectDescription() : ""%>">
			</div>
		</div>
	</div>
	<%
		} else {
	%>
	<div>
		<p class="text txt-danger">Sinh viên chưa thực hiện đồ án nào cả!</p>
		<button type="button" class="btn btn-success"
			onFocus="location.reload();">Quay lại</button>
	</div>
	<%
		}
	%>
	<!-- 
	<div class="row" style="margin-top: 10px; padding: 1px;">
		<div class="col-sm-1 col-sm-offset-11">
			<a
				href="QuanLiNguoiDung?act=viewUserList&userCategory=student&courseId=<%=thisCourseId%>"
				type="button" class="btn btn-warning btn-block">Quay lại</a>
		</div>
		</div>
		 -->
		<%@ include file="_footer.jsp"%>
</body>
</html>