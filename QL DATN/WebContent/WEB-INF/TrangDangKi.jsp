<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Utils.SupportTool"%>
<%@ page import="Model.DAO.InstructorDAO"%>
<%@ page import="java.sql.Timestamp"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null) {
		response.sendRedirect("login");
	}
	// Trạng thái chọn
	boolean chosen = false;
	// Lấy mã giảng viên đã chọn (nếu có)
	String chosenInstructorId = (String) request.getAttribute("chosenInstructorId");
	if (chosenInstructorId != null) {
		chosen = true;
	}
	// Lấy thông tin đợt hiện tại
	Course thisCourse = (Course) request.getAttribute("thisCourse");
	// Lấy toàn bộ danh sách giảng viên trong đợt
	ArrayList<Instructor> instructorList = (ArrayList<Instructor>) request.getAttribute("instructorList");
	SupportTool tool = new SupportTool();
	String currentDateTime = tool.getCurrentDateTime().toString().substring(0, 19);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<script src="js/jquery.min.js"></script>
<title>Danh sách giảng viên trong đợt</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.popover {
	color: black;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<!-- TÊN ĐỢT -->
		<div class="container">
			<h2>
				<span class="text text-primary"><strong>Đăng kí</strong></span> Đợt
				<%=thisCourse.getCourseId()%>
				:
				<%=thisCourse.getCourseName()%>
			</h2>
			<%
				// Kiểm tra xem có nằm trong thời gian đăng kí hay không
				Timestamp regTime = Timestamp.valueOf(currentDateTime);
				Timestamp beginTime = Timestamp.valueOf(thisCourse.getBeginRegistrationTime());
				Timestamp endTime = Timestamp.valueOf(thisCourse.getEndRegistrationTime());
				boolean ready = false;
				if (regTime.after(beginTime) && regTime.before(endTime)) {
					ready = true;
				}
			%>
			<h5>
				Thời gian đăng kí: từ
				<mark><%=thisCourse.getBeginRegistrationTime()%></mark>
				đến
				<mark><%=thisCourse.getEndRegistrationTime()%></mark>
				(Tình trạng: <strong><mark><%=ready ? "<span class=\"text text-success\">ĐANG MỞ</span>"
					: "<span class=\"text text-danger\">ĐÃ ĐÓNG</span>"%></mark></strong>. Thời gian hệ
				thống:
				<mark><%=currentDateTime%></mark>
				)
			</h5>
		</div>
		<!-- /TÊN ĐỢT -->

		<!-- KẾT QUẢ TẠM THỜI -->
		<%
			if (chosen) {
				Instructor chosenInstructor = new Instructor();
				for (Instructor i : instructorList) {
					if (i.getUserId().equals(chosenInstructorId)) {
						chosenInstructor = i;
						break;
					}
				}
		%>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<h4 class="col-md-6">Kết quả đăng kí <%=ready?"tạm thời":"" %></h4>
				</div>
			</div>
			<div class="panel-body">
				<!--THEAD-->
				<table class="table table-striped">
					<tr>
						<th>Họ tên SV</th>
						<th>Họ tên GVHD</th>
						<th>Trình độ</th>
						<th>Hướng nghiên cứu</th>
						<th>SLĐK</th>
						<th>Thao tác</th>
					</tr>
					<!--END THEAD-->
					<!--TBODY-->
					<tr>
						<td><%=(String) session.getAttribute("fullName")%></td>
						<td><span> <%=chosenInstructor.getFullName()%></span></td>
						<td><span> <%=chosenInstructor.getLevelOfStudy()%></span></td>
						<td><span> <%=chosenInstructor.getAllStudyFieldNames()%>
						</span></td>
						<td><span> <%=chosenInstructor.getCurrentNumberOfStudentsByCourseId(thisCourse.getCourseId())%>
								/ <%=chosenInstructor.getMaxNumberOfStudentsByCourseId(thisCourse.getCourseId())%>
						</span></td>
						<td>
							<form action="QuanLiDoAn" method="get">
								<input type="hidden" name="act" value="removeInstructor">
								<input type="hidden" name="courseId"
									value="<%=thisCourse.getCourseId()%>"> <input
									type="hidden" name="instructorId"
									value="<%=chosenInstructor.getUserId()%>"> <input
									type="submit" class="btn btn-sm btn-danger btn-block"
									value="Xóa" <%=ready ? "" : "disabled"%>>
							</form>
						</td>
					</tr>
					<!--END TBODY-->
				</table>
			</div>
		</div>
		<%
			}
		%>
		<!-- /KẾT QUẢ TẠM THỜI -->

		<!-- DANH SÁCH GIẢNG VIÊN-->
		<%
			if (ready) {
		%>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h4>Danh sách giảng viên hướng dẫn</h4>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<tr>
						<!--tiêu đề -->
						<th>STT</th>
						<th>Họ tên</th>
						<th>Trình độ</th>
						<th>Hướng nghiên cứu</th>
						<th>SLĐK</th>
						<th>Thao tác</th>
					</tr>
					<!--/tiêu đề -->
					<%
						int i = 1;
							for (Instructor instructor : instructorList) {
					%>
					<tr>
						<!-- bản ghi -->
						<td><%=(i++)%></td>
						<td><%=instructor.getFullName()%></td>
						<td><span> <%=instructor.getLevelOfStudy()%></span></td>
						<td><%=instructor.getAllStudyFieldNames()%></td>
						<td><%=instructor.getCurrentNumberOfStudentsByCourseId(thisCourse.getCourseId())%>
							/ <%=instructor.getMaxNumberOfStudentsByCourseId(thisCourse.getCourseId())%>
						</td>
						<!-- Khóa nút đăng kí khi đầy -->
						<%
							boolean full = instructor.isQTableFull(thisCourse.getCourseId());
						%>
						<td>
							<form action="QuanLiDoAn" method="get">
								<input type="hidden" name="act" value="chooseInstructor">
								<input type="hidden" name="courseId"
									value="<%=thisCourse.getCourseId()%>"> <input
									type="hidden" name="instructorId"
									value="<%=instructor.getUserId()%>"> <input
									type="submit" class="btn btn-sm btn-success btn-block"
									value="Đăng kí" <%=chosen || full || !ready ? "disabled" : ""%>>
							</form>
						</td>
					</tr>
					<%
						}
					%>
					<!-- / bản ghi -->
				</table>
			</div>
		</div>
		<%
			}
		%>
		<!-- /DANH SÁCH GIẢNG VIÊN-->
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<%@ include file="_footer.jsp"%>
</body>
</html>