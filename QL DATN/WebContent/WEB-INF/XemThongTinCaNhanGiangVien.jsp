<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.QuantityTable"%>
<%@ page import="Model.BEAN.StudyField"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	// Lấy dữ liệu nhận được từ COntroller QuanLiCaNhan
	Instructor thisInstructor = (Instructor) request.getAttribute("thisInstructor");

	/* Tiếp tục trích xuất dữ liệu bằng <% =DỮ_LIỆU% > trực tiếp trong HTML từ thisInstructor*/
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="_headtags.jsp"%>
<title>Quản lí cá nhân dành cho Giảng viên</title>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Quản lí thông tin cá nhân giảng viên</h3>
			</div>
			<div class="panel-body">
				<form class="form-holizontal" role="form" action="QuanLiCaNhan"
					method="get">
					<input type="hidden" name="act" value="saveInfo">
					<div class="form-group">
						<label class="control-label col-md-2">Họ và tên</label>
						<div class="col-md-4">
							<input name="fullName" type="text" class="form-control"
								value="<%=thisInstructor.getFullName()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Giới tính</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control" value="<%=thisInstructor.getGender()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Tài khoản</label>
						<div class="col-md-4">
							<input name="userId" readonly="readonly" type="text"
								class="form-control" value="<%=thisInstructor.getUserId()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Khoa</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control" value="<%=thisInstructor.getDepartment()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Ngày sinh</label>
						<div class="col-md-4">
							<input name="dateOfBirth" type="text" class="form-control"
								value="<%=thisInstructor.getDateOfBirth()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Số điện thoại</label>
						<div class="col-md-4">
							<input name="phoneNumber" type="text" class="form-control"
								value="<%=thisInstructor.getPhoneNumber()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Địa chỉ</label>
						<div class="col-md-10">
							<input name="address" type="text" class="form-control"
								value="<%=thisInstructor.getAddress()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Trình độ</label>
						<div class="col-md-4">
							<input name="levelOfStudy" type="text" class="form-control"
								value="<%=thisInstructor.getLevelOfStudy()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Hướng nghiên cứu</label>
						<div class="col-md-4">
							<!-- In bảng chọn danh sách các hướng nghiên cứu -->
							<div class="dropdown">
								<button class="btn btn-default btn-block dropdown-toggle"
									data-toggle="dropdown">
									Xem<span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<%
										ArrayList<StudyField> studyField = (ArrayList<StudyField>) thisInstructor.getStudyField();
										for (int i = 0; i < studyField.size(); i++) {
											StudyField sf = studyField.get(i);
											out.print("<li><a>" + sf.getFieldName() + " : " + sf.getFieldDetail() + "</a></li>");
										}
									%>
									<li><hr class="divider"></li>
									<li><a href="" data-toggle="modal"
									data-target="#changeStudyField">Sửa	đổi</a></li>
								</ul>
							</div>
							<!-- Bảng hiển thị dữ liệu khi nhấn nút Sửa đổi -->
							<div id="changeStudyField" class="modal fade" role="dialog">
								<div class="modal-dialog modal-lg"
									style="width: 96%; height: 300px;">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">
												Sửa đổi hướng nghiên cứu giảng viên
											</h4>
										</div>
										<div class="modal-body">
											<iframe
												src="QuanLiCaNhan?act=changeStudyField"
												style="border: none; width: 100%; height: 300px;">
											</iframe>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal" onFocus="location.reload();">Đóng</button>
										</div>
									</div>

								</div>
							</div>
							<!-- /Bảng hiển thị dữ liệu -->
							<!-- /In bảng chọn danh sách các hướng nghiên cứu -->
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Email 1</label>
						<div class="col-md-4">
							<input name="email" type="text" class="form-control"
								value="<%=thisInstructor.getEmail()%>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Email 2</label>
						<div class="col-md-4">
							<input name="" readonly="readonly" type="text"
								class="form-control"
								value="<%=thisInstructor.getUserId()%>@gv.dut.udn.vn">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-10 col-md-2">
							<input type="submit" class="btn btn-success btn-block"
								value="Lưu thông tin">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Đổi mật khẩu</h3>
			</div>
			<div class="panel-body">
				<form class="form-holizontal" role="form" action="QuanLiCaNhan"
					method="post">
					<input type="hidden" name="act" value="changePassword">
					<div class="form-group">
						<label class="control-label col-md-2">Mật khẩu cũ</label>
						<div class="col-md-4">
							<input name="oldPassword" type="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Mật khẩu mới</label>
						<div class="col-md-4">
							<input name="newPassword" type="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Xác nhận mật khẩu</label>
						<div class="col-md-4">
							<input name="confirmedPassword" type="password"
								class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-2">
							<input name="" type="submit" class="btn btn-success btn-block"
								value="Đổi
							mật khẩu">
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>