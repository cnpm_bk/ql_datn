<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Notice"%>
<%@ page import="java.util.ArrayList"%>
<%
	ArrayList<Notice> notices = (ArrayList<Notice>) request
			.getAttribute("notices");
%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="_headtags.jsp"%>
<title>Danh sách thông báo</title>
</head>
<body>
	<div class="container">
		<h1>
			<span class="text text-warning">Danh sách thông báo được phép
				quản lí</span>
		</h1><p>Chức năng đang xây dựng....</p>
	</div>

	<%
		for (int i = 0; i < notices.size(); i++) {
			Notice notice = notices.get(i);
			boolean j = (i % 2 == 1);
	%>
	<div class="panel <%=j ? "panel-warning" : "panel-danger"%>">
			<div class="panel-heading" title="Người gửi: <%=notice.getUserId()%>">
					<small><%=notice.getTime().substring(0, 16)%></small>
					[Khóa <%=notice.getCourseId()%>] <strong><%=notice.getTitle()%></strong>
					<span class="text text-right"><strong><u>Sửa</u> | <u>Xóa</u></strong></span>
			</div>
			<div class="panel-body">
				<%=notice.getContent()%>
			</div>
		</div>
	<%
		}
	%>
	<%@ include file="_footer.jsp"%>
</body>
</html>