<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.QuantityTable"%>
<%@ page import="Model.BEAN.StudyField"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"
	ArrayList<Instructor> instructorList = (ArrayList<Instructor>) request.getAttribute("instructorList");
	Course course = (Course) request.getAttribute("course");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Danh sách giảng viên trong đợt</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<form action="QuanLiNguoiDung" method="get">
			<table class="table table-striped table-bordered">
				<tr>
					<!--tiêu đề -->
					<th>STT</th>
					<th>Mã GV</th>
					<th>Họ tên</th>
					<th>Trình độ</th>
					<th>Hướng Nghiên Cứu</th>
					<th>Chọn</th>
				</tr>
				<!--/tiêu đề -->
				<%
					for (int i = 0; i < instructorList.size(); i++) {
						Instructor instructor = instructorList.get(i);
				%>
				<tr>
					<!-- bản ghi -->
					<td><%=(i + 1)%></td>
					<td><%=instructor.getUserId()%></td>
					<td><%=instructor.getFullName()%></td>
					<td><%=instructor.getLevelOfStudy()%></td>
					<td>
						<%
							ArrayList<StudyField> studyFields = instructor.getStudyField();
								for (int j = 0; j < studyFields.size(); j++) {
									StudyField sf = studyFields.get(j);
									out.print(sf.getFieldName() + ", ");
								}
						%>
					</td>
					<td><input type="checkbox" name="instructor[]"
						value="<%=instructor.getUserId()%>"></td>
				</tr>
				<%
					}
				%>
				<!-- / bản ghi -->
			</table>
			<div class="row">
				<div class="col-md-3">
					<a
						href="QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=<%=course.getCourseId()%>"
						type="button" class="btn btn-info btn-block">Quay lại</a>
				</div>
				<div class="col-md-3 col-md-offset-6">
					<input type="hidden" name="act" value="addInstructors"> <input
						type="hidden" name="confirmed" value="confirmed"> <input
						type="hidden" name="courseId" value="<%=course.getCourseId()%>"><input
						type="submit" class="btn btn-success btn-block" value="Nhập danh sách đã chọn" <%=course.isActived()?"":"disabled" %>>
				</div>
			</div>
		</form>
	</div>
	<%@ include file="_footer.jsp"%>
</body>
</html>