<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Model.BEAN.Student"%>
<%@ page import="Model.BEAN.Instructor"%>
<%@ page import="Model.BEAN.Course"%>
<%@ page import="Model.BEAN.Project"%>
<%@ page import="Model.BEAN.QuantityTable"%>
<%@ page import="Model.BEAN.StudyField"%>
<%@ page import="java.util.ArrayList"%>
<%
	// Chưa đăng nhập thì quay về đăng nhập
	if (session.getAttribute("userId") == null)
		response.sendRedirect("login"); //Nhảy đến Controller "login"

	ArrayList<Student> studentList = (ArrayList<Student>) request.getAttribute("studentList");
	ArrayList<Instructor> instructorList = (ArrayList<Instructor>) request.getAttribute("instructorList");
	ArrayList<Project> projectList = (ArrayList<Project>) request.getAttribute("projectList");
	Course course = (Course) request.getAttribute("course");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Danh sách sinh viên trong đợt</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container-fluid">
		<!-- HEAD -->
		<table class="table table-bordered table-striped">
			<tr>
				<th>STT</th>
				<th>Mã SV</th>
				<th>Họ tên</th>
				<th>Lớp</th>
				<th>GVHD</th>
				<th>Tiến độ</th>
				<th>Xem chi tiết</th>
				<th>Xóa sinh viên</th>
			</tr>
			<!-- /HEAD -->
			<!--
				<%for (Student std : studentList) {
				out.print(std.getFullName() + ", ");
			}%>
				 -->
			<!-- BODY -->
			<%
				int i = 1;
				for (Student student : studentList) {
			%>
			<tr>
				<td><%=(i++)%></td>
				<td><%=student.getUserId()%></td>
				<td><%=student.getFullName()%></td>
				<td><%=student.getStudentClass()%></td>
				<%
					String instructorId = "";
						boolean projectNull = true;
						byte currentPogress = 0;
						project: for (Project p : projectList) {
							if (student.getUserId().equals(p.getStudentId())) {
								instructorId = p.getInstructorId();
								currentPogress = p.getCurrentRateOfProgress();
								break project;
							}
						}
						String instructorName = "";
						ins: for (Instructor ins : instructorList) {
							if (ins.getUserId().equals(instructorId)) {
								instructorName = "<i>" + ins.getLevelOfStudy() + "</i> " + ins.getFullName();
								projectNull = false;
								break ins;
							}
						}
				%>
				<td><%=instructorName%></td>
				<td><%=currentPogress%> / <%=course.getProgressRate()%></td>
				<td>
					<!-- 
				<form action="QuanLiNguoiDung" method="get">
								<input type="hidden" name="act" value="ViewStudentDetails">
								<input type="hidden" name="studentId"
									value="<%=student.getUserId()%>"> <input
									type="hidden" name="courseId"
									value="<%=course.getCourseId()%>"> <input
									type="submit" target = "_main" class="btn btn-info"
									value="Xem chi tiết" <%=projectNull ? "disabled" : ""%>>
							</form>
							 --> <%
 	if (projectNull == false) {
 %> <a type="button" class="btn btn-info" data-toggle="modal"
					data-target="#studentDetailed"
					onClick="javascript:document.getElementById('detailedStudent').src='QuanLiNguoiDung?act=ViewStudentDetails&studentId=<%=student.getUserId()%>&courseId=<%=course.getCourseId()%>'">
						Xem chi tiết</a> <%
 	}
 %>
				</td>
				<td>
					<form action="QuanLiNguoiDung" method="get">
						<input type="hidden" name="act"
							value="deleteStudentInCurrentCourse"> <input
							type="hidden" name="studentId" value="<%=student.getUserId()%>">
						<input type="hidden" name="courseId"
							value="<%=course.getCourseId()%>"> <input type="submit"
							class="btn btn-danger" value="Xóa khỏi đợt"
							onmouseup="javascript:alert('Xóa sinh viên thành công!');">
					</form>
				</td>
			</tr>
			<%
				}
			%>
		</table>
		<!-- Modal chi tiết sinh viên -->
		<div id="studentDetailed" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg" style="width: 96%; height: 195px;">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" onFocus="location.reload();" class="close"
							data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Xem chi tiết sinh viên</h4>
					</div>
					<form action="QuanLiThongBao">
						<div class="modal-body">
							<iframe src="#" id="detailedStudent"
								style="border: none; width: 100%; height: 195px;"> </iframe>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Modal chi tiết sinh viên -->
		<!-- /BODY -->

		<!-- BODY2 -->
		<br>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form action="QuanLiNguoiDung" method="get">
					<input type="hidden" name="act" value="addStudents"> <input
						type="hidden" name="courseId" value="<%=course.getCourseId()%>">
					<button type="submit" class="btn btn-success btn-block"
						<%=course.isActived() ? "" : "disabled"%>>Nhập sinh viên
						vào đợt</button>
				</form>
			</div>
		</div>
		<!-- /BODY2 -->
	</div>

	<%@ include file="_footer.jsp"%>
</body>
</html>