<!-- Trang này đóng vai trò như điều hướng, là trang người dùng có thể 
truy xuất trực tiếp dưới dạng file jsp trên thanh địa chỉ. Những trang khác chỉ
có thể truy cập thông qua Controller -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	// Chưa đăng nhập thì quay về trang dành cho khách
	if (session.getAttribute("userId") == null)
		response.sendRedirect("index.jsp");
%>
<!DOCTYPE html>
<html lang="vi">
<head>
<%@ include file="/WEB-INF/_headtags.jsp"%>
<title>Hệ Thống quản lí đồ án tốt nghiệp</title>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.myBody {
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#000000+0,000000+100&0+0,1+100 */
	background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 0%,
		rgba(0, 0, 0, 1) 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 0%,
		rgba(0, 0, 0, 1) 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%,
		rgba(0, 0, 0, 1) 100%);
	/* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',
		endColorstr='#000000', GradientType=0); /* IE6-9 */
}
</style>
</head>
<body class="myBody">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="">
					<h1>
						Hệ Thống Quản Lí Đồ Án Tốt Nghiệp <small>Khoa CNTT - ĐHBK</small>
					</h1>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="text text-right">
					<span
						class="
						<%if ("student".equals(session.getAttribute("userType"))) {
				out.print("glyphicon glyphicon-education");
			} else {
				out.print("glyphicon glyphicon-user");
			}%>"></span>
					<span class=" text-primary"><%=session.getAttribute("fullName")%>
						[<%=session.getAttribute("userId")%>] </span> <span
						class="glyphicon glyphicon-chevron-right"></span> <a type="button"
						class="btn btn-sm btn-warning" href="login?act=logout">Thoát
						&nbsp;<span class="glyphicon glyphicon-log-out"></span>
					</a>
				</h4>
			</div>
		</div>
	</div>
	<!-- ---------------------------------------------------------- -->
	<div class="btn-group" role="group" aria-label="...">
		<a href="index.jsp" type="button" class="btn-group btn btn-success">Trang
			chủ</a>

		<div class="btn-group" role="group">
			<a type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Quản lí hệ thống <span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><a href="QuanLiHeThong" target="_main">Quản lý hệ thống</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="QuanLiThongBao" target="_main">Quản lý thông
						báo</a></li>
			</ul>
		</div>

		<div class="btn-group ">
			<a type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Quản lý cá nhân <span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><a href="QuanLiCaNhan" target="_main">Xem thông tin cá
						nhân</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="login?act=logout">Đăng xuất</a></li>
			</ul>
		</div>
		<div class="btn-group">
			<a type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Quản lý đồ án <span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><a href="QuanLiDoAn?act=registerProject" target="_main">Đăng
						kí</a></li>
				<li><a href="QuanLiDoAn?act=viewMyProject" target="_main">Xem
						chi tiết đồ án</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="QuanLiDoAn?act=viewAllProjects" target="_main">Xem
						danh sách đồ án</a></li>
			</ul>
		</div>
		<div class="btn-group">
			<a type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Hướng dẫn<span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><a href="homePage?mod=about" target="_main">Thông tin
						nhóm phát triển</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="homePage?mod=manual" target="_main">Tài liệu
						hướng dẫn</a></li>
			</ul>
		</div>
	</div>
	<!-- ---------------------------------------------------------- -->

	<div id="feature1">
		<div class="container" style="width: 100%; height: 500px">

			<!-- Khung làm việc chính, hiển thị các trang được chọn -->
			<iframe src="homePage" name="_main"
				style="border: none; width: 100%; height: 100%;"> </iframe>

		</div>
	</div>
	<!--#feature1-->


	<div class="container-fluid" style="width: 100%; height: 50px">
		<div class="row">
			<p class="col-md-9 bg-primary text-info">
				Đề tài nghiên cứu môn học: Công nghệ phần mềm<br> GVHD: ThS. Lê
				Thị Mỹ Hạnh.<br> Nhóm SV thực hiện: Nguyễn Bá Anh - Huỳnh Kim
				Chính - Nguyễn Phúc Hảo - Ngô Trường Phạm Quang - Nguyễn Công Y.<br>
			</p>
			<p class="col-md-3 bg-info text-success text-right">
				Đại học Đà Nẵng<br> Trường Đại học Bách Khoa<br> Khoa Công
				nghệ thông tin
			</p>
		</div>
	</div>

	<%@ include file="/WEB-INF/_footer.jsp"%>
</body>
</html>