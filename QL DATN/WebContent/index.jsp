<!-- Trang này đóng vai trò như điều hướng, là trang người dùng có thể 
truy xuất trực tiếp dưới dạng file jsp trên thanh địa chỉ. Những trang khác chỉ
có thể truy cập thông qua Controller -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	// Đã đăng nhập thì quay về trang dành cho người dùng
	if (session.getAttribute("userId") != null) {
		response.sendRedirect("default.jsp");
	}
%>
<!DOCTYPE html>
<html lang="vi">
<head>
<%@ include file="/WEB-INF/_headtags.jsp"%>
<title>Hệ Thống Quản Lí Đồ Án Tốt Nghiệp</title>
</head>
<style>
.myBody {
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#000000+0,000000+100&0+0,1+100 */
background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,1) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,1) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */

}
</style>
<body class="myBody">
	<!-- ------------------------ -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<h1>
					Hệ Thống Quản Lí Đồ Án Tốt Nghiệp <small>Khoa CNTT - ĐHBK</small>
				</h1>
			</div>
			
			<div class="col-md-4">
				<form method="post" action="login?act=login">
					<div class="input-group">
						<input type="text" name="userId" class="form-control"
							placeholder="Tài khoản"> <span class="input-group-btn"
							style="width: 0px;"></span> <input type="password"
							name="password" class="form-control" placeholder="Mật khẩu">
						<div class="input-group-btn">
							<button type="submit" class="btn btn-default">Đăng nhập</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!-- ---------------------------------------------------------- -->
	<div class="btn-group" role="group" aria-label="...">
		<a href="index.jsp" type="button" class="btn-group btn btn-success">Trang
			chủ</a>

		<div class="btn-group">
			<a type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Hướng dẫn<span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li><a href="homePage?mod=about" target="_main">Thông tin
						nhóm phát triển</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="homePage?mod=manual" target="_main">Tài liệu
						hướng dẫn</a></li>
			</ul>
		</div>
	</div>
	<!-- ---------------------------------------------------------- -->
	<div id="feature1">
		<div class="container" style="width: 100%; height: 500px">

			<!-- Khung làm việc chính, hiển thị các trang được chọn -->
			<iframe src="homePage" name="_main"
				style="border: none; width: 100%; height: 100%;"> </iframe>

		</div>
	</div>
	<!--#feature1-->
	<!-- </div> -->
	<div class="container-fluid" style="width: 100%; height: 50px">
		<div class="row">
			<p class="col-md-9 bg-primary text-info">
				Đề tài nghiên cứu môn học: Công nghệ phần mềm<br> GVHD: ThS. Lê Thị Mỹ
				Hạnh.<br> Nhóm SV thực hiện: Nguyễn Bá Anh - Huỳnh Kim Chính -
				Nguyễn Phúc Hảo - Ngô Trường Phạm Quang - Nguyễn Công Y.<br>
			</p>
			<p class="col-md-3 bg-info text-success text-right">
				Đại học Đà Nẵng<br> Trường Đại học Bách Khoa<br> Khoa Công
				nghệ thông tin
			</p>
		</div>
	</div>

	<%@ include file="/WEB-INF/_footer.jsp"%>
</body>
</html>