package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.BEAN.Course;
import Model.BEAN.CoursePlan;
import Model.BEAN.Notice;
import Model.BO.CourseBO;
import Model.BO.NoticeBO;
import Utils.Message;
import Utils.SupportTool;

/**
 * Servlet implementation class QuanLiHeThong
 */

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class QuanLiHeThong extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiHeThong() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);

		// Tạo đối tượng nghiệp vụ BO
		CourseBO courseBO = new CourseBO();

		SupportTool tool = new SupportTool();

		// Xác định kiểu người dùng từ Session (đc tạo lúc Login)
		String userType = (String) session.getAttribute("userType");

		// ===== XÁC ĐỊNH QUYỀN HẠN NGƯỜI DÙNG =====
		// Chỉ quản lí mới được phép dùng các chức năng trong trang này!
		if (userType == null || !("manager".equals(userType) || "instructorAsManager".equals(userType))) {
			request.setAttribute("message",
					new Message("homePage", "Quay về trang chủ", "Bạn không đủ quyền hạn để thực hiện chức năng này"));
			request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			return; // Ra ngoài!
		}
		// ===== XÁC ĐỊNH HÀNH ĐỘNG =====
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}
		// ===== ĐIỀU HƯỚNG HÀNH ĐỘNG =====
		switch (act) {
		// ===== TẠO MỚI ĐỢT ============================================
		case "createNewCourse": {
			String confirmed = request.getParameter("confirmed");
			if (confirmed != null && confirmed.equals("confirmed")) {
				// Lấy param từ form
				String courseId = request.getParameter("courseId");
				String courseName = request.getParameter("courseName");
				boolean actived = true;
				String startDate = request.getParameter("startDate");
				String stopDate = request.getParameter("stopDate");
				String beginRegistrationTime = request.getParameter("beginRegistrationTime");
				String endRegistrationTime = request.getParameter("endRegistrationTime");
				byte progressRate = 2; // mặc định 2
				try { // bắt lỗi nếu có, có lỗi thì giữ 2
					progressRate = Byte.parseByte(request.getParameter("progressRate"));
				} catch (NumberFormatException nfe) {
				}
				// Nạp đợt mới
				Course newCourse = new Course(courseId, courseName, actived, startDate, stopDate, beginRegistrationTime,
						endRegistrationTime, progressRate, 0, 0, null);
				// Gọi phương thức tạo mới và chờ mã phản hồi
				switch (courseBO.createNewCourse(newCourse)) {
				case SUCCESS: {
					// Phát thông báo lên trang chủ
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId,
							"Thông báo kế hoạch dự kiến đợt hướng dẫn " + courseId,
							"[Hệ thống] Đợt hướng dẫn <mark>" + courseName + "</mark> được mở từ <mark>"
									+ startDate.substring(0, 10) + "</mark> đến <mark>" + stopDate.substring(0, 10)
									+ "</mark>. Thời gian đăng kí dự kiến từ <mark>"
									+ beginRegistrationTime.substring(0, 19) + "</mark> đến <mark>"
									+ endRegistrationTime.substring(0, 19) + "</mark>.",
							tool.getCurrentDateTime().toString());
					NoticeBO noticeBO = new NoticeBO();
					noticeBO.addNewNotice(notice);
					// Hiển thị thông điệp
					request.setAttribute("message", new Message("QuanLiHeThong", "Quay về trang quản lí hệ thống",
							"Thêm đợt [" + courseId + ": " + courseName + "]  thành công"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case FAILED: {
					request.setAttribute("message", new Message("QuanLiHeThong?act=createNewCourse",
							"Quay về trang tạo mới đợt", "Thêm đợt [" + courseId + ": " + courseName + "]  thất bại"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case EMPTY: {
					request.setAttribute("message", new Message("QuanLiHeThong?act=createNewCourse",
							"Quay về trang tạo mới đợt", "Dữ liệu nhập vào không đầy đủ!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case NULL: {
					request.setAttribute("message", new Message("QuanLiHeThong?act=createNewCourse",
							"Quay về trang tạo mới đợt", "Dữ liệu không tồn tại!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case ILLOGICAL: {
					request.setAttribute("message", new Message("QuanLiHeThong?act=createNewCourse",
							"Quay về trang tạo mới đợt",
							"Ràng buộc ngày tháng không hợp lí: ngày bắt đầu ở sau ngày kết thúc, khoảng thời gian đăng kí lấn vào thời gian hoạt động của đợt!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case TIME_NOT_VALID: {
					request.setAttribute("message",
							new Message("QuanLiHeThong?act=createNewCourse", "Quay về trang tạo mới đợt",
									"Thời gian không đúng định dạng: yyyy-dd-mm hh:ii:ss[.ffffff]"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				default: {
					request.setAttribute("message", new Message("homePage", "Quay về trang chủ", "Không xác định!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
				}
			} else { // Chưa xác nhận
				request.getRequestDispatcher("/WEB-INF/TaoMoiDot.jsp").include(request, response);
			}
			break;
		}

			// ===== NGỪNG ĐỢT ĐANG HOẠT ĐỘNG ==================================
		case "stopCourseById": {
			// Lấy param
			String confirmed = request.getParameter("confirmed");
			String courseId = request.getParameter("courseId");
			Course pendingCourse = courseBO.getCourseById(courseId);
			// Kiểm tra dữ liệu param
			if ((confirmed == null || (!"confirmed".equals(confirmed)) && courseId != null)) {
				// Nếu đợt này đã dừng
				if (!pendingCourse.isActived()) {
					request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
							"Quay về đợt " + courseId, "Đợt đã được kết thúc từ trước"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else { // Nếu đợt chưa dừng và nút checkbox quên bấm thì gọi
							// giao diện xác nhận
					request.setAttribute("pendingCourse", pendingCourse);
					request.getRequestDispatcher("/WEB-INF/KetThucDot.jsp").include(request, response);
				}
			} else {
				if (courseBO.stopCourseById(courseId)) {
					NoticeBO noticeBO = new NoticeBO();
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId,
							"Thông báo kết thúc đợt " + courseId, "[Hệ thống] Đợt hướng dẫn [<mark>" + courseId + ": "
									+ pendingCourse.getCourseName() + "</mark>] chính thức kết thúc. ",
							tool.getCurrentDateTime().toString());
					noticeBO.addNewNotice(notice);

					request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
							"Quay về đợt " + courseId, "Kết thúc đợt " + courseId + " thành công"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else {
					request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
							"Quay về đợt " + courseId, "Kết thúc đợt " + courseId + " thất bại"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
			}
			break;
		}
			// ===== ĐỔI SỐ LẦN NỘP TIẾN ĐỘ ==================================
		case "changeProgressRate": {
			// Lấy param
			String courseId = request.getParameter("courseId");
			byte progressRate = Byte.parseByte(request.getParameter("progressRate"));
			switch (courseBO.changeProgressRate(courseId, progressRate)) {
			case SUCCESS: {
				/*
				 * request.setAttribute("message", new
				 * Message("QuanLiHeThong?act=viewCourseById&courseId=" +
				 * courseId, "Quay về đợt " + courseId,
				 * "Thay đổi lần báo cáo thành công."));
				 * request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").
				 * include(request, response);
				 */
				response.sendRedirect("QuanLiHeThong");
				break;
			}
			case FAILED: {
				request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
						"Quay về đợt " + courseId, "Thay đổi lần báo cáo thất bại."));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case ILLOGICAL: {
				request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
						"Quay về đợt " + courseId, "Giá trị nhập vào không hợp lí!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			default: {
			}
			}
			break;
		}
			// ===== ĐỔI THỜI GIAN ĐĂNG KÍ ==================================
		case "changeRegistrationTime": {
			// Lấy 3 params
			String beginRegistrationTime = request.getParameter("beginRegistrationTime");
			String endRegistrationTime = request.getParameter("endRegistrationTime");
			String courseId = request.getParameter("courseId");
			String updateRegistrationTime = request.getParameter("updateRegistrationTime");
			// Xử lí dữ liệu
			switch (courseBO.changeRegistrationTime(beginRegistrationTime, endRegistrationTime, courseId)) {
			case SUCCESS: {
				NoticeBO noticeBO = new NoticeBO();
				// Thành công thì tự động phát thông báo
				// Nếu là lần đầu tiên đặt thời gian
				if (updateRegistrationTime == null || "".equals(updateRegistrationTime)) {
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId,
							"Thông báo lịch đăng kí cho đợt " + courseId,
							"[Hệ thống] Các sinh viên trong đợt [<mark>" + courseId
									+ "</mark>] lưu ý: Thời gian <mark>đăng kí</mark>: từ <strong>"
									+ beginRegistrationTime.substring(0, 19) + " đến "
									+ endRegistrationTime.substring(0, 19) + "</strong>.",
							tool.getCurrentDateTime().toString());

					noticeBO.addNewNotice(notice);
					// Tải giao diện
					request.setAttribute("message",
							new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
									"Quay về đợt " + courseId,
									"Đặt thời gian đăng kí thành công! Hệ thống đã tự động thông báo lịch đăng kí."));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else { // Lần thứ 2 trở đi (sửa đổi thời gian)
					// Xóa thông báo cũ
					noticeBO.deteleNoticeByTitle("Thông báo lịch đăng kí cho đợt " + courseId);
					// Phát thông báo mới
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId,
							"Thông báo lịch đăng kí cho đợt " + courseId,
							"[Hệ thống] Các sinh viên trong đợt [<mark>" + courseId
									+ "</mark>] lưu ý: <mark>Thay đổi thời gian đăng kí</mark> từ <strong>"
									+ beginRegistrationTime.substring(0, 19) + " ~~ "
									+ endRegistrationTime.substring(0, 19) + "</strong>.",
							tool.getCurrentDateTime().toString());
					noticeBO.addNewNotice(notice);
					// Tải giao diện thông điệp
					request.setAttribute("message",
							new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
									"Quay về đợt "
											+ courseId,
							"Đặt thời gian đăng kí thành công! Hệ thống đã tự động cập nhật thông báo lịch đăng kí."));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
				break;
			}
			case FAILED: {
				request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
						"Quay về đợt " + courseId, "Đặt thời gian đăng kí thất bại!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case ILLOGICAL: {
				request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
						"Quay về đợt " + courseId, "Thời gian không hợp lí!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case TIME_NOT_VALID: {
				request.setAttribute("message", new Message("QuanLiHeThong?act=viewCourseById&courseId=" + courseId,
						"Quay về đợt " + courseId, "Sai định dạng thời gian: yyyy-dd-mm hh:ii:ss[.ffffff]"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			default: {
			}
			}
			break;
		}
			// ===== XEM CHI TIẾT ĐỢT ==================================
		case "viewCourseById": {
			String courseId = request.getParameter("courseId");
			Course thisCourse = courseBO.getCourseById(courseId);
			ArrayList<Course> basicCourseList = courseBO.getCourseIdAndName();
			if (thisCourse != null) {
				request.setAttribute("thisCourse", thisCourse);
				request.setAttribute("basicCourseList", basicCourseList);
				request.getRequestDispatcher("/WEB-INF/QuanLiHeThong.jsp").include(request, response);
			} else {
				request.setAttribute("message", new Message("homePage", "Quay về trang chủ", "Không tìm thấy mã đợt."));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}
			// ===== CẬP NHẬT KẾ HOẠCH NỘP TIẾN ĐỘ
			// ==================================
		case "updateCoursePlan": {
			// TODO
			String courseId = request.getParameter("courseId");
			Byte order = Byte.parseByte(request.getParameter("order"));
			String beginReportDate = request.getParameter("beginReportDate");
			String endReportDate = request.getParameter("endReportDate");
			String update = request.getParameter("update");
			CoursePlan coursePlan = new CoursePlan(courseId, order, beginReportDate, endReportDate);
			switch (courseBO.changeCoursePlan(coursePlan, update)) {
			default: // tuột về SUCCESS
			case SUCCESS: {
				// Phát thông báo
				String title = "Thông báo nộp tiến độ lần " + order;
				if (order == 0) {
					title = "Thông báo NỘP ĐỒ ÁN";
				}

				if ("yes".equals(update)) {
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId, title,
							"[Hệ thống] Các sinh viên trong đợt [<mark>" + courseId
									+ "</mark>] lưu ý: <mark>Thời gian nộp </mark> từ <strong>" + beginReportDate
									+ " đến " + endReportDate + "</strong>.",
							tool.getCurrentDateTime().toString());
					new NoticeBO().deteleNoticeByTitle(notice.getTitle());
					new NoticeBO().addNewNotice(notice);
				} else {
					Notice notice = new Notice("", (String) session.getAttribute("userId"), courseId, title,
							"[Hệ thống] Các sinh viên trong đợt [<mark>" + courseId
									+ "</mark>] lưu ý: <mark>Thời gian nộp </mark> từ <strong>" + beginReportDate
									+ " đến " + endReportDate + "</strong>.",
							tool.getCurrentDateTime().toString());
					new NoticeBO().addNewNotice(notice);
				}
				// Quay về trang quản lí hệ thống
				response.sendRedirect("QuanLiHeThong");
				break;
			}
			case FAILED: {
				request.setAttribute("message",
						new Message("QuanLiHeThong", "Quay về trang quản lí hệ thống", "Cập nhật thất bại!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case ILLOGICAL: {
				request.setAttribute("message",
						new Message("QuanLiHeThong", "Quay về trang quản lí hệ thống", "Thời gian không hợp lí!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case NULL: {
				request.setAttribute("message",
						new Message("QuanLiHeThong", "Quay về trang quản lí hệ thống", "Lỗi NULL!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			}
			break;
		}
			// ===== MẶC ĐỊNH ==================================
		default: { // Mặc định hiển thị trang quản lí chung
			Course thisCourse = courseBO.getTheNewestCourse();
			ArrayList<Course> basicCourseList = courseBO.getCourseIdAndName();
			if (thisCourse != null) {
				request.setAttribute("thisCourse", thisCourse);
				request.setAttribute("basicCourseList", basicCourseList);
				request.getRequestDispatcher("/WEB-INF/QuanLiHeThong.jsp").include(request, response);
			} else {
				request.setAttribute("message", new Message("homePage", "Quay về trang chủ", "Không tìm thấy mã đợt."));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
		}
		}
		// ===================================================================
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
