package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.BEAN.Notice;
import Model.BO.NoticeBO;
import Utils.Message;

/**
 * Servlet implementation class QuanLiThongBao
 */

/**
 * @author NguyenBaAnh, NguyenCongY
 *
 */
public class QuanLiThongBao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiThongBao() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);

		// Xác định kiểu người dùng từ Session (đc tạo lúc Login)
		String userType = (String) session.getAttribute("userType");
		String userId = (String) session.getAttribute("userId");
		NoticeBO noticeBO = new NoticeBO();
		
		// Nếu người dùng không phải là người quản lí hoặc giảng viên thì không cho thực hiện
		// chức năng nào cả
		switch(userType) {
		case "instructorAsManager": // nhảy xuống 
		case "managerAsInstructor": // nhảy xuống tiếp
		case "manager": { // Giáo vụ có quyền thay đổi tất cả
			ArrayList<Notice> notices = noticeBO.getAllNotices();
			request.setAttribute("notices", notices);
			request.getRequestDispatcher("/WEB-INF/DanhSachThongBao.jsp")
			.include(request, response);
			break;
		}
		case "instructor": { // Giảng viên chỉ có thể thay đổi của mình
			ArrayList<Notice> notices = noticeBO.getUserNotices(userId);
			request.setAttribute("notices", notices);
			request.getRequestDispatcher("/WEB-INF/DanhSachThongBao.jsp")
			.include(request, response);		
			break;
		}
		default: {
			request.setAttribute("message",
					new Message("homePage",
							"Quay về trang chủ", "Bạn không đủ quyền hạn cho chức năng này!"));
			request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
