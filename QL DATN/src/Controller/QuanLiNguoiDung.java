package Controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import Model.BEAN.Course;
import Model.BEAN.Instructor;
import Model.BEAN.Project;
import Model.BEAN.Student;
import Model.BEAN.User;
import Model.BO.CourseBO;
import Model.BO.CourseUserBO;
import Model.BO.InstructorBO;
import Model.BO.ProjectBO;
import Model.BO.StudentBO;
import Model.BO.UserBO;
import Utils.Message;
import Utils.SupportTool;

/**
 * Servlet implementation class QuanLiNguoiDung
 */

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class QuanLiNguoiDung extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiNguoiDung() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng out để in ra thông báo
		// PrintWriter out = response.getWriter();

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);

		// Tạo đối tượng nghiệp vụ BO
		CourseBO courseBO = new CourseBO();
		CourseUserBO courseUserBO = new CourseUserBO();
		InstructorBO instructorBO = new InstructorBO();
		StudentBO studentBO = new StudentBO();
		ProjectBO projectBO = new ProjectBO();

		// Xác định kiểu người dùng từ Session (đc tạo lúc Login)
		String userType = (String) session.getAttribute("userType");

		// Nếu người dùng không phải là người quản lí thì không cho thực hiện
		// chức năng nào cả
		if (userType == null || !("manager".equals(userType) || "instructorAsManager".equals(userType))) {
			request.setAttribute("message",
					new Message("homePage", "Quay lại trang chủ", "Không đủ quyền thực hiện chức năng này!"));
			request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			return;
		}
		// Xác định hành động act
		String act = request.getParameter("act"); // Hành động
		if (act == null) {
			act = "";
		}
		// Nhóm người dùng cần xử lí
		String userCategory = request.getParameter("userCategory");
		if (userCategory == null) {
			userCategory = "";
		}

		// ===== ĐIỀU HƯỚNG HÀNH ĐỘNG ============
		switch (act) { // Lọc thao tác người dùng gửi lên
		/**
		 * Xem danh sách người dùng của từng đợt
		 */
		case "viewUserList": {
			if ("student".equals(userCategory)) { // DANH SÁCH SINH VIÊN
				// FIXME
				String courseId = request.getParameter("courseId");
				ArrayList<Student> studentList = studentBO.getStudentListByCourseId(courseId);
				// Lấy danh sách giảng viên mà các sinh viên đã đăng kí
				ArrayList<Instructor> instructorList = instructorBO.getInstructorListByCourseId(courseId);
				// Lấy thông tin các đồ án mà sv đã đăng kí nếu có
				ArrayList<Project> projectList = projectBO.getProjectListByCourseId(courseId);
				Course course = courseBO.getCourseById(courseId);
				// Gửi về giao diện
				request.setAttribute("studentList", studentList);
				request.setAttribute("instructorList", instructorList);
				request.setAttribute("projectList", projectList);
				request.setAttribute("course", course);
				request.getRequestDispatcher("/WEB-INF/DanhSachSinhVienCuaDotHuongDan.jsp").include(request, response);
			}
			if ("instructor".equals(userCategory)) { // DANH SÁCH GIẢNG VIÊN
				String courseId = request.getParameter("courseId");
				Course course = courseBO.getCourseById(courseId);
				request.setAttribute("course", course);
				ArrayList<Instructor> instructorList = instructorBO.getInstructorListByCourseId(courseId);
				request.setAttribute("instructorList", instructorList);
				request.getRequestDispatcher("/WEB-INF/DanhSachGiangVienCuaDotHuongDan.jsp").include(request, response);
			}
			break;
		} // end viewUserList

			/**
			 * Thêm các giảng viên vào danh sách của đợt
			 */
		case "addInstructors": {
			String confirmed = request.getParameter("confirmed");
			String courseId = request.getParameter("courseId");
			if ("confirmed".equals(confirmed)) {
				// Xử lí thêm
				String instructorId[] = request.getParameterValues("instructor[]");
				if (instructorId == null) {
					request.setAttribute("message",
							new Message("QuanLiNguoiDung?act=addInstructors&courseId=" + courseId,
									"Quay lại trang nhập danh sách giảng viên", "Bạn chưa chọn ai cả!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else {
					String messageBuffer = "<ul>\n";
					for (int i = 0; i < instructorId.length; i++) {
						// Duyệt và thêm theo Id của giảng viên và đợt
						if (courseUserBO.addInstructorIntoCourse(courseId, instructorId[i])) {
							messageBuffer += "<li>Thêm " + instructorId[i] + " thành công.</li>\n";
						} else {
							messageBuffer += "<li>Thêm " + instructorId[i] + " thất bại.</li>\n";
						}
					}
					messageBuffer += "</ul>";
					request.setAttribute("message",
							new Message("QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId,
									"Quay lại trang danh sách giảng viên", messageBuffer));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}

			} else {
				// Tải giao diện chọn lên
				// Lấy tất cả giáo viên có trong hệ thống
				ArrayList<Instructor> instructorList = new ArrayList<Instructor>();
				instructorList = instructorBO.getAllInstructors();
				Course course = courseBO.getCourseById(courseId);
				request.setAttribute("course", course);
				request.setAttribute("instructorList", instructorList);
				request.setAttribute("courseId", courseId);
				request.getRequestDispatcher("/WEB-INF/ThemGiangVien.jsp").include(request, response);
			}
			break;
		}

			/**
			 * Thêm sinh viên vào danh sách
			 */
		case "addStudents": {
			// phương thức thêm: nếu sinh viên đã tồn tại mã SV trong hệ thống
			// thì thêm ngay vào đợt, nếu sinh viên chưa tồn tại trong hệ thống
			// thì thêm sinh viên vào hệ thống rồi thêm vào đợt
			String confirmed = request.getParameter("confirmed");
			String courseId = request.getParameter("courseId");
			if ("confirmed".equals(confirmed)) { // Xử lí file upload lên
				SupportTool tool = new SupportTool();
				String baseURL = tool.getBaseURL(request);
				boolean isMultipart;
				String filePath = baseURL + "\\files\\";
				long maxFileSize = 100000 * 1024; // 100KB
				int maxMemSize = 50000 * 1024; // 50KB
				File file;
				// Đợi 1 yêu cầu upload file
				isMultipart = ServletFileUpload.isMultipartContent(request);
				if (!isMultipart) {
					request.setAttribute("message", new Message("QuanLiNguoiDung?act=addStudents&courseId=" + courseId,
							"Quay lại trang nhập DSSV", "Không tải lên tập tin nào cả!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					return;
				}
				DiskFileItemFactory factory = new DiskFileItemFactory();
				// Lưu vào bộ nhớ
				factory.setSizeThreshold(maxMemSize);
				// Lưu vào đĩa
				factory.setRepository(new File("D:\\git\\QL_DATN\\QL DATN\\WebContent\\files\\temp\\"));

				// Tiến hành upload
				ServletFileUpload upload = new ServletFileUpload(factory);
				upload.setSizeMax(maxFileSize);
				try {
					List fileItems = upload.parseRequest(request);
					Iterator i = fileItems.iterator();
					while (i.hasNext()) {
						FileItem fi = (FileItem) i.next();
						if (!fi.isFormField()) {
							// Lấy thông tin cơ bản
							String fieldName = fi.getFieldName();
							String fileName = fi.getName();
							String contentType = fi.getContentType();
							boolean isInMemory = fi.isInMemory();
							long sizeInBytes = fi.getSize();
							// Ghi tệp
							if (fileName.lastIndexOf("\\") >= 0) {
								file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
							} else {
								file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
							}
							fi.write(file);

							// Xử lí file để nhập vào CSDL
							studentBO.addStudentsFromVector(courseId, tool.readExcelFile(file.getPath()));
							String logFile = "D:/" + courseId + "-students.txt";
							request.setAttribute("message",
									new Message("QuanLiNguoiDung?act=addStudents&courseId="
											+ courseId, "Quay lại trang nhập DSSV",
									"Đã nhập xong, xin xem ghi chép chi tiết tại: " + logFile));
							request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);

						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			} else {
				// Nạp giao diện thêm sinh viên
				Course course = courseBO.getCourseById(courseId);
				request.setAttribute("course", course);
				request.getRequestDispatcher("/WEB-INF/ThemSinhVien.jsp").include(request, response);
			}
			break;
		}

			/**
			 * Sửa số lượng sinh viên tối đa 1 giảng viên có thể hướng dẫn trong
			 * 1 đợt
			 */
		case "changeMaxNumberOfStudents": {
			if ("instructor".equals(userCategory)) {
				byte maxNumberOfStudents = Byte.parseByte(request.getParameter("maxNumberOfStudents"));
				String courseId = request.getParameter("courseId");
				String instructorId = request.getParameter("instructorId");
				if (maxNumberOfStudents == 0) {
					// Xóa GV ra khỏi danh sách
					if (courseUserBO.removeInstructorFromCourse(courseId, instructorId)) {
						response.sendRedirect(
								"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId);
					} else {
						request.setAttribute("message",
								new Message(
										"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId,
										"Quay lại trang danh sách giảng viên", "Không thể xóa giảng viên này!"));
						request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					}
				} else if (maxNumberOfStudents < 1 || maxNumberOfStudents > 10) {
					request.setAttribute("message",
							new Message("QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId,
									"Quay lại trang danh sách giảng viên",
									"Số nhập vào không hợp lí: số âm, số quá lớn!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else {
					if (instructorBO.changeMaxNumberOfStudents(courseId, instructorId, maxNumberOfStudents)) {
						response.sendRedirect(
								"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId);
					} else {
						request.setAttribute("message",
								new Message(
										"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId,
										"Quay lại trang danh sách giảng viên", "Thao tác thay đổi số lượng thất bại!"));
						request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					}
				}
			} else { // Thao tác Không phải dành cho giảng viên
				response.sendRedirect("homePage");
			}
			break;
		} // end changeMaxNumberOfStudents

		// ===================================================================
		// Quang edited from here
		
			/**
			 * Xem thông tin chi tiết của 1 giảng viên trong 1 đợt: thông tin cơ
			 * bản, tình hình hướng dẫn từng sinh viên đang phụ trách
			 */
		case "viewInstructorDetails": {
			String courseId = request.getParameter("courseId");
			String instructorId = request.getParameter("instructorId");
			User instructor = instructorBO.getInstructorById(instructorId);
			String instructorName = instructor.getFullName();
			ArrayList<User> studentList = projectBO.getStudentByInstructorIdAndCourseId(instructorId, courseId);
			if (studentList != null) {
				request.setAttribute("instructorId", instructorId);
				request.setAttribute("studentList", studentList);
				request.setAttribute("courseId", courseId);
				request.setAttribute("instructorName", instructorName);
				request.getRequestDispatcher("/WEB-INF/XemChiTietGiangVien.jsp").include(request, response);
			}
			break;
		} // end viewInstructorDetails

			/**
			 * Xem thông tin chi tiết của 1 sinh viên trong 1 đợt: thông tin cơ
			 * bản, tình hình thực hiện đồ án
			 */
		case "ViewStudentDetails": {
			String studentId = request.getParameter("studentId");
			String courseId = request.getParameter("courseId");
			Course course = courseBO.getCourseById(courseId);
			byte maxProgressRate = course.getProgressRate();
			Project project = projectBO.getProjectByStudentIdAndCourseId(studentId, courseId);
			String instructorFullName = "";
			if (project != null) {
				Instructor instructor = instructorBO.getInstructorById(project.getInstructorId());
				instructorFullName = instructor.getFullName();
			}
			Student student = studentBO.getStudentById(studentId);
			String studentFullName = student.getFullName();
			if (project != null) {
				request.setAttribute("studentFullName", studentFullName);
				request.setAttribute("courseId", courseId);
				request.setAttribute("project", project);
				request.setAttribute("maxProgressRate", maxProgressRate);
				request.setAttribute("instructorFullName", instructorFullName);
				request.getRequestDispatcher("/WEB-INF/XemChiTietSinhVien.jsp").include(request, response);
			}
			
			else {
				boolean projectNull = true;
				request.setAttribute("projectNull", projectNull);
			}
			break;			
		} // end viewStudentDetails
		
			/**
			 * Xóa giảng viên: Xóa giảng viên không có sinh viên hướng dẫn
			 * ra khỏi đợt đồ án
			 */
		case "deleteInstructor": {
			String instructorId = request.getParameter("instructorId");
			String courseId = request.getParameter("courseId");
			if (courseUserBO.removeInstructorFromCourse(courseId, instructorId)) {
				response.sendRedirect(
						"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId);
			} else {
				request.setAttribute("message",
						new Message(
								"QuanLiNguoiDung?act=viewUserList&userCategory=instructor&courseId=" + courseId,
								"Quay lại trang danh sách giảng viên", "Không thể xóa giảng viên này!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		} // end deleteInstructor

			/**
			 * Hiển thị danh sách giảng viên để thay đổi: Hiển thị khi muốn thay
			 * đổi giảng viên hướng dẫn của một sinh viên
			 */
		case "viewListInstructorToChange": {
			String studentId = request.getParameter("studentId");
			String courseId = request.getParameter("courseId");
			String instructorId = request.getParameter("instructorId");
			Instructor instructor = instructorBO.getInstructorById(instructorId);
			String instructorFullName = instructor.getFullName();
			ArrayList<Instructor> instructorList = instructorBO.getInstructorListByCourseId(courseId);
			request.setAttribute("instructorList", instructorList);
			request.setAttribute("instructorFullName", instructorFullName);
			request.setAttribute("instructorId", instructorId);
			request.setAttribute("studentId", studentId);
			request.setAttribute("courseId", courseId);
			request.getRequestDispatcher("/WEB-INF/ThayDoiGiangVien.jsp").include(request, response);
			break;
		} // end viewListInstructorToChange
		
			/**
			 * Thay đổi giảng viên hướng dẫn: Thay đổi giảng viên hướng dẫn của sinh viên
			 */
		case "changeInstructor": {
			String studentId = request.getParameter("studentId");
			String courseId = request.getParameter("courseId");
			String newInstructorId = request.getParameter("newInstructorId");
			String oldInstructorId = request.getParameter("oldInstructorId");
			if (projectBO.changeInstructorByStudentIdAndCourseId(studentId, courseId, newInstructorId, oldInstructorId)) {
				response.sendRedirect(
						"QuanLiNguoiDung?act=viewListInstructorToChange&studentId=" + studentId + 
						"&courseId=" + courseId + "&instructorId=" + newInstructorId);
			}
			else {
				request.setAttribute("message",
						new Message(
								"QuanLiNguoiDung?act=viewListInstructorToChange&studentId=" + studentId + 
								"&courseId=" + courseId + "&instructorId=" + oldInstructorId,
								"Quay lại trang thay đổi giảng viên", "Không thể thay đổi giảng viên của sinh viên này!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}// end changeInstructor

			/**
			 * Xóa sinh viên không đăng ký đồ án ra khỏi đợt
			 */
		case "deleteStudentInCurrentCourse": {
			String studentId = request.getParameter("studentId");
			String courseId = request.getParameter("courseId");
			if (courseUserBO.removeStudentFromCourse(courseId, studentId)) {
				response.sendRedirect(
						"QuanLiNguoiDung?act=viewUserList&userCategory=student&courseId=" + courseId);
			}
			else {
				request.setAttribute("message",
						new Message(
								"QuanLiNguoiDung?act=viewUserList&userCategory=student&courseId=" + courseId,
								"Quay lại trang danh sách sinh viên", "Không thể xóa sinh viên này!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}// end deleteStudentInCurrentCourse

		default: { // Chức năng không phù hợp, hiển thị trang rỗng
		}
		} // end switch act

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
