package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.BEAN.Notice;
import Model.BO.NoticeBO;

/**
 * Servlet implementation class homePage
 */

/**
 * @author NguyenBaAnh
 *
 */
public class homePage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public homePage() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng out để in ra thông báo
		PrintWriter out = response.getWriter();
		
		String mod = request.getParameter("mod");
		if (mod == null) {
			mod = "";
		}
		switch (mod) {
		case "about": {
			request.getRequestDispatcher("/WEB-INF/ThongTin.jsp").include(request, response);
			break;
		}
		case "manual": {
			out.print("Đang xây dựng chức năng.");
			request.getRequestDispatcher("/WEB-INF/HuongDan.jsp");
			break;
		}
		default: {
			// Chức năng thông báo
			NoticeBO noticeBO = new NoticeBO();
			ArrayList<Notice> notices = noticeBO.getAllNotices();
			request.setAttribute("notices", notices);
			// Gọi View từ trang chủ, trả kết quả cho người dùng
			request.getRequestDispatcher("/WEB-INF/home.jsp").include(request, response);
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
