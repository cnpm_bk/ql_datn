package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.BEAN.Instructor;
import Model.BEAN.QuantityTable;
import Model.BEAN.Student;
import Model.BEAN.StudyField;
import Model.BO.InstructorBO;
import Model.BO.StudentBO;
import Model.BO.StudyFieldBO;
import Model.BO.UserBO;
import Utils.Message;

/**
 * Servlet implementation class QuanLiCaNhan
 */

/**
 * @author NguyenBaAnh
 *
 */
public class QuanLiCaNhan extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiCaNhan() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);

		// Xác định kiểu người dùng từ Session (đc tạo lúc Login)
		String userType = (String) session.getAttribute("userType");
		String userId = (String) session.getAttribute("userId");

		// Tạo đối tượng BO
		UserBO userBO = new UserBO();
		InstructorBO instructorBO = new InstructorBO();

		// Kiểm tra pẩm act
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}

		// Hành động đổi mật khẩu từ tất cả người dùng
		if (act.equals("changePassword")) {
			// Lấy 3 trường dữ liệu mật khẩu
			String oldPassword = request.getParameter("oldPassword");
			String newPassword = request.getParameter("newPassword");
			String confirmedPassword = request.getParameter("confirmedPassword");

			switch (userBO.changePassword(userId, oldPassword, newPassword, confirmedPassword)) {
			case SUCCESS: {
				request.setAttribute("message", new Message("", "", "Đổi mật khẩu thành công!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case FAILED: {
				request.setAttribute("message", new Message("", "", "Đổi mật khẩu thất bại!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case EMPTY: {
				request.setAttribute("message", new Message("", "", "Nhập thiếu!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			case PASSWORD_NOT_MATCH: {
				request.setAttribute("message", new Message("", "", "Nhập lại mật khẩu mới không khớp!"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				break;
			}
			default: {
			}
			}
		}
		// Bước 3: Tùy theo kiểu người dùng mà gọi Bean và View Thích hợp.
		boolean switched = false;
		switch (userType) {
		case "student": { // Người dùng đang sử dụng hệ thống là sinh viên
			// Thu thập action từ form
			switch (act) {
			case "saveInfo": { // Nếu nút nhấn lưu thông tin cá nhân được bấm
				// Tiến hành lấy các dữ liệu có thể thay đổi từ người dùng
				String fullName = request.getParameter("fullName");
				String dateOfBirth = request.getParameter("dateOfBirth");
				String phoneNumber = request.getParameter("phoneNumber");
				String address = request.getParameter("address");
				String email = request.getParameter("email");
				// Tạo đối tượng BO
				StudentBO studentBO = new StudentBO();
				// Gọi phương thức sửa đổi từ BO
				if (!studentBO.updateInformation(userId, fullName, dateOfBirth, phoneNumber, address, email)) {
					request.setAttribute("message",
							new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân", "Cập nhật thất bại!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				} else {
					request.setAttribute("message",
							new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân", "Cập nhật thành công!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
				break;
			}
			default: { // Nếu không phải 2 act trên thì hiển thị màn hình thông
						// tin
				// Tạo Bean và BO
				StudentBO studentBO = new StudentBO();
				Student thisStudent = studentBO.getStudentById(userId);

				// Gửi thisStudent về View để hiển thị
				if (thisStudent != null) {
					request.setAttribute("thisStudent", thisStudent);
					request.getRequestDispatcher("/WEB-INF/XemThongTinCaNhanSinhVien.jsp").include(request, response);
				} else {
					request.setAttribute("message", new Message("", "", "Không thể lấy thông tin!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
			}
			} // END Switch act
			break;
		} // END case student

		case "instructorAsManager": {
			if ("switchMode".equals(request.getParameter("act"))) {
				session.setAttribute("userType", "managerAsInstructor");
				switched = true; // đánh dấu chuyển
				response.sendRedirect("homePage");
			}
		} // Không break cho trôi về instructor

		case "managerAsInstructor": {
			// Nếu vừa mới chuyển xong thì bỏ qua bước này
			if ("switchMode".equals(request.getParameter("act")) && !switched) {
				session.setAttribute("userType", "instructorAsManager");
				response.sendRedirect("homePage");
			}
		} // Không break cho trôi về instructor

		case "instructor": {
			// Thu thập action từ form
			switch (act) {
			case "saveInfo": { // Nếu nút nhấn lưu thông tin cá nhân được bấm
				// Tiến hành lấy các dữ liệu có thể thay đổi từ người dùng
				String fullName = request.getParameter("fullName");
				String dateOfBirth = request.getParameter("dateOfBirth");
				String phoneNumber = request.getParameter("phoneNumber");
				String address = request.getParameter("address");
				String email = request.getParameter("email");
				String levelOfStudy = request.getParameter("levelOfStudy");
				// STUDYFIELD - FIXME
				ArrayList<StudyField> studyField = null; // Thêm sau

				Instructor instructor = new Instructor(userId, fullName, (String) null, address, email, (String) null,
						userType, dateOfBirth, phoneNumber, (String) null, (String) null, levelOfStudy, studyField,
						(ArrayList<QuantityTable>) null);
				switch (instructorBO.updateInformation(instructor)) {
				case SUCCESS: {
					request.setAttribute("message",
							new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân", "Cập nhật thành công!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case FAILED: {
					request.setAttribute("message",
							new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân", "Cập nhật thất bại!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				case EMPTY: {
					request.setAttribute("message", new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân",
							"Họ tên bỏ trống vui lòng kiểm tra lại!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
					/*
					 * case TIME_NOT_VALID: { out.print(
					 * "Định dạng ngày sinh không đúng vui lòng nhập lại!" +
					 * back); break; }
					 */
				case NULL: {
					request.setAttribute("message",
							new Message("QuanLiCaNhan", "Quay lại trang quản lí cá nhân", "Lỗi NULL!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					break;
				}
				default: {
				}
				}
				break;
			}
				/**
				 * Duyệt và thay đổi hướng nghiên cứu của giảng viên
				 */
			case "changeStudyField": {
				// Lấy param
				String change = request.getParameter("change");
				StudyFieldBO studyFieldBO = new StudyFieldBO();
				// Lấy tất cả hướng nghiên cứu có trong hệ thống
				ArrayList<StudyField> studyFields = studyFieldBO.getAllStudyFields();
				if ("yes".equals(change)) { // có thao tác thay đổi
					String chosenFields_[] = request.getParameterValues("chosenFields[]");
					int chosenFields[] = new int[chosenFields_.length];
					for (int i = 0; i < chosenFields_.length; i++) {
						chosenFields[i] = Integer.valueOf(chosenFields_[i]);
					}
					// Cập nhật và quay về
					studyFieldBO.updateStudyFields(userId, chosenFields);
					response.sendRedirect("QuanLiCaNhan?act=changeStudyField");
				} else { // hiển thị danh sách
					// Lấy tất cả hướng nghiên cứu của giảng viên
					ArrayList<Integer> instructorStudyFieldIds = studyFieldBO.getInstructorStudyFieldIds(userId);
					request.setAttribute("studyFields", studyFields);
					request.setAttribute("instructorStudyFieldIds", instructorStudyFieldIds);
					request.getRequestDispatcher("/WEB-INF/DanhSachHuongNghienCuu.jsp").include(request, response);
				}
				break;
			}
			default: { // Hiển thị trang cá nhân
				Instructor thisInstructor = instructorBO.getInstructorById(userId);
				// Gửi thisInstructor về View để hiển thị
				if (thisInstructor != null) {
					request.setAttribute("thisInstructor", thisInstructor);
					request.getRequestDispatcher("/WEB-INF/XemThongTinCaNhanGiangVien.jsp").include(request, response);
				} else {
					request.setAttribute("message", new Message("", "", "Xảy ra lỗi không xác định!"));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}
			}
			}
			break;
		}

		default:
			request.setAttribute("message", new Message("", "", "Người dùng đặc biệt không có chức năng cá nhân!"));
			request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
