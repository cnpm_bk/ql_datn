package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.BEAN.Course;
import Model.BEAN.Instructor;
import Model.BEAN.Progress;
import Model.BEAN.Project;
import Model.BO.CourseBO;
import Model.BO.CourseUserBO;
import Model.BO.InstructorBO;
import Model.BO.ProjectBO;
import Utils.Message;
import Utils.SupportTool;

/**
 * Servlet implementation class QuanLiDoAn
 */

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class QuanLiDoAn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuanLiDoAn() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);

		// Tạo đối tượng nghiệp vụ BO
		CourseBO courseBO = new CourseBO();
		InstructorBO instructorBO = new InstructorBO();
		// Xác định kiểu người dùng từ Session (đc tạo lúc Login)
		String userType = (String) session.getAttribute("userType");
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}

		switch (act) {
		case "viewInstructorList": {
			if ("student".equals(userType)) {
				request.getRequestDispatcher("/WEB-INF/TrangDangKi.jsp").include(request, response);
			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho sinh viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}
			/**
			 * Trang đăng kí đồ án mới, hiển thị danh sách giảng viên và kết quả
			 * đăng kí
			 */
		case "registerProject": {
			if ("student".equals(userType)) {
				// Lấy mã người dùng sinh viên
				String studentId = (String) session.getAttribute("userId");
				CourseUserBO courseUserBO = new CourseUserBO();
				// Kiểm tra xem sinh viên có đợt đăng kí nào không
				if (courseUserBO.isActivedUser(studentId)) {
					// Lấy mã đợt sinh viên được phép đăng kí
					String courseId = courseUserBO.getCourseIdByUserId(studentId);
					// Lấy mã giảng viên mà sinh viên đã chọn (nếu có)
					ProjectBO projectBO = new ProjectBO();
					String chosenInstructorId = projectBO.getInstructorId(courseId, studentId);
					// Lấy danh sách giảng viên có trong đợt
					ArrayList<Instructor> instructorList = new ArrayList<Instructor>();
					instructorList = instructorBO.getInstructorListByCourseId(courseId);
					// Lấy thông tin của đợt đăng kí
					Course thisCourse = courseBO.getCourseById(courseId);
					// Trả về giao diện
					request.setAttribute("chosenInstructorId", chosenInstructorId);
					request.setAttribute("instructorList", instructorList);
					request.setAttribute("thisCourse", thisCourse);
					request.getRequestDispatcher("/WEB-INF/TrangDangKi.jsp").include(request, response);
				} else {
					Message message = new Message("homePage", "Quay về trang chủ", "Không có đợt đăng kí nào cho bạn!");
					request.setAttribute("message", message);
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}

			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho sinh viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		} // end registerProject
			/**
			 * Chức năng chọn giảng viên, chọn thành công thì quay về trang đăng
			 * kí (case "registerProject")
			 */
		case "chooseInstructor": {
			// Lấy params
			String instructorId = request.getParameter("instructorId");
			String courseId = request.getParameter("courseId");
			String studentId = (String) session.getAttribute("userId");

			// Đăng kí 1 đồ án mới (actived = false)
			ProjectBO projectBO = new ProjectBO();
			if (projectBO.registerProject(courseId, instructorId, studentId)) {
				response.sendRedirect("QuanLiDoAn?act=registerProject");
			} else {
				Message message = new Message("QuanLiDoAn?act=registerProject", "Quay về trang đăng kí",
						"Thao tác đăng kí giảng viên thất bại!");
				request.setAttribute("message", message);
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}
			/**
			 * Chức năng hủy bỏ giảng viên, thành công thì quay về lại trang
			 * đăng kí
			 */
		case "removeInstructor": {
			// Lấy params
			String instructorId = request.getParameter("instructorId");
			String courseId = request.getParameter("courseId");
			String studentId = (String) session.getAttribute("userId");

			ProjectBO projectBO = new ProjectBO();
			if (projectBO.removeProject(courseId, instructorId, studentId)) {
				response.sendRedirect("QuanLiDoAn?act=registerProject");
			} else {
				Message message = new Message("QuanLiDoAn?act=registerProject", "Quay về trang đăng kí",
						"Thao tác xóa giảng viên thất bại");
				request.setAttribute("message", message);
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}
		case "viewMyProject": {
			if ("student".equals(userType)) {
				String courseId = request.getParameter("courseId");
				String studentId = (String) session.getAttribute("userId");
				ProjectBO projectBO = new ProjectBO();
				// Lấy ra danh sách tất cả đồ án của sinh viên
				ArrayList<Project> projectList = projectBO.getProjectListByStudentId(studentId);
				if (projectList == null) {
					Message message = new Message("homePage", "Quay về trang chủ",
							"Không tìm thấy đồ án nào của bạn trong hệ thống.");
					request.setAttribute("message", message);
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					return;
				}
				Course thisCourse = null;
				if (courseId == null) {
					// Tìm giảng viên tương ứng với thisCourse
					thisCourse = courseBO.getTheNewestCourse();
				} else { // Nếu người dùng chọn một đồ án từ danh sách
					thisCourse = courseBO.getCourseById(courseId);
				}
				String instructorId = "";
				if (projectList != null) {
					for (Project p : projectList) {
						if (thisCourse.getCourseId().equals(p.getCourseId())) {
							instructorId = p.getInstructorId();
						}
					}
				}
				Instructor thisInstructor = instructorBO.getInstructorById(instructorId);
				request.setAttribute("thisCourse", thisCourse);
				request.setAttribute("thisInstructor", thisInstructor);
				request.setAttribute("projectList", projectList);
				request.getRequestDispatcher("/WEB-INF/DoAnSinhVien.jsp").include(request, response);
			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho sinh viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}

		case "viewAllProjects": {
			if ("instructor".equals(userType) || "managerAsInstructor".equals(userType)
					|| "instructorAsManager".equals(userType)) {
				// TODO
				request.setAttribute("message",
						new Message("", "", "Chức năng xem đồ án cho giảng viên đang được thiết kế"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);

			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho giảng viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}

		case "updateMyProject": {
			if ("student".equals(userType)) {
				String courseId = request.getParameter("courseId");
				String projectTitle = request.getParameter("projectTitle");
				String projectDescription = request.getParameter("projectDescription");
				String studentId = (String) session.getAttribute("userId");
				Project projectInfo = new Project("", studentId, courseId, false, projectTitle, projectDescription,
						(byte) 0, false);
				ProjectBO projectBO = new ProjectBO();
				if (projectBO.updateProjectInformation(projectInfo)) {
					response.sendRedirect("QuanLiDoAn?act=viewMyProject");
				} else {
					Message message = new Message("QuanLiDoAn?act=viewMyProject&courseId=" + courseId,
							"Quay về trang đồ án cá nhân", "Không tìm thấy đồ án nào của bạn trong hệ thống.");
					request.setAttribute("message", message);
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}

			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho sinh viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}

		case "updateMyProgress": {
			if ("student".equals(userType)) {
				// Get param
				String courseId = request.getParameter("courseId");
				String studentId = (String) session.getAttribute("userId");
				Byte rateOfProgress = Byte.parseByte(request.getParameter("rateOfProgress"));
				String reportDate = new SupportTool().getCurrentDateTime().toString(); 
				String reportTitle = request.getParameter("reportTitle");
				String reportContent = request.getParameter("reportContent");
				String optionalLink = request.getParameter("optionalLink");
				boolean approvedByStudent = false; // Mặc định sv chưa được nộp
				if ("yes".equals(request.getParameter("ready"))) {
					approvedByStudent = true;
				}	
				Progress progress;
// TEST
		if ("yes".equals(request.getParameter("test"))) {
			progress = new Progress("K00", "sv001", rateOfProgress, reportDate, "Nộp ĐA",
					"blah blah final", "http://facebook.com", approvedByStudent, false, false);
		} else {
			progress = new Progress(courseId, studentId, rateOfProgress, reportDate, reportTitle,
					reportContent, optionalLink, approvedByStudent, false, false);
		}
// __END
				
				ProjectBO projectBO = new ProjectBO();
				if (projectBO.updateProgress(progress)) {
					response.sendRedirect("QuanLiDoAn?act=viewMyProject");
				} else {
					Message message = new Message("QuanLiDoAn?act=viewMyProject&courseId=" + courseId,
							"Quay về trang đồ án cá nhân", "Nộp thất bại.");
					request.setAttribute("message", message);
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
				}

			} else {
				request.setAttribute("message", new Message("", "", "Chức năng này chỉ dành cho sinh viên"));
				request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			}
			break;
		}

		default: {
		}
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
