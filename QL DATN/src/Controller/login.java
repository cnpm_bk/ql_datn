package Controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.BEAN.User;
import Model.BO.LoginBO;
import Utils.Message;

/**
 * Servlet implementation class login
 */

/**
 * @author NguyenBaAnh
 *
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public login() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Thiết lập kiểu tài liệu trả về, mã hóa trang
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// Tạo đối tượng session để lưu thông tin phiên sử dụng
		HttpSession session = request.getSession(true);
		// Kiểm tra param điều hướng đầu vào
		String act = request.getParameter("act");
		if (act == null) {
			act = "";
		}
		switch (act) {
		case "logout": { // ĐĂNG XUẤT
			session.invalidate(); // Hủy phiên sử dụng
			request.setAttribute("message", new Message("index.jsp", "Quay về trang chủ", "Đã đăng xuất thành công!"));
			request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
			break;
		} // CASE LOGOUT
		case "login": { // ĐĂNG NHẬP
			String userId, password;
			LoginBO loginBO = null;
			User currentUser = null;
			userId = request.getParameter("userId"); // Tài khoản
			password = request.getParameter("password"); // Mật khẩu
			loginBO = new LoginBO(); // Tạo đối tượng BO
			try {
				// KIỂM TRA NGƯỜI DÙNG CÓ TỒN TẠI TRONG CSDL KHÔNG
				if (loginBO.validate(userId, password)) {
					currentUser = loginBO.getCurrentUser();
					session = request.getSession();
					session.setAttribute("userId", currentUser.getUserId());
					session.setAttribute("userType", currentUser.getUserType());
					session.setAttribute("fullName", currentUser.getFullName());
					response.sendRedirect("default.jsp"); // Chuyển hướng về Trang chủ 
				} else {
					// BÁO THẤT BẠI VÀ QUAY LẠI TRANG ĐĂNG NHẬP
					request.setAttribute("message",
							new Message("", "", "Sai tài khoản hoặc mật khẩu. Xin đăng nhập lại."));
					request.getRequestDispatcher("/WEB-INF/ThongDiep.jsp").include(request, response);
					request.removeAttribute("message"); // Hủy attr chứa thông báo
					request.getRequestDispatcher("/WEB-INF/login.jsp").include(request, response);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		} // CASE LOGIN
		default: { // Không phải 2 chức năng trên thì tải trang đăng nhập
			request.getRequestDispatcher("/WEB-INF/login.jsp").include(request, response);
		} // CASE DEFAULT
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
