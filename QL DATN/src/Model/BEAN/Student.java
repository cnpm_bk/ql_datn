/**
 * 
 */
package Model.BEAN;

/**
 * @author NguyenBaAnh
 *
 */
public class Student extends User {

	String studentClass, department, organization;
	
	public Student() {
		
	}
	public Student(String userId, String fullName, String gender, String address, String email, String password,
			String userType, String dateOfBirth, String phoneNumber, String studentClass, String department,
			String organization) {
		super(userId, fullName, gender, address, email, password, userType, dateOfBirth, phoneNumber);
		this.studentClass = studentClass;
		this.department = department;
		this.organization = organization;
	}

	public String getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(String studentClass) {
		this.studentClass = studentClass;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
}
