/**
 * 
 */
package Model.BEAN;

import java.util.ArrayList;

/**
 * @author NguyenBaAnh
 *
 */
public class Project {
	String instructorId, studentId, courseId;
	boolean actived;
	String projectTitle, projectDescription;
	byte currentRateOfProgress;
	boolean approvedByManager;
	ArrayList<Progress> progresses;
	public Project() {
		super();
	}

	public Project(String instructorId, String studentId, String courseId, boolean actived, String projectTitle,
			String projectDescription, byte currentRateOfProgress, boolean approvedByManager) {
		super();
		this.instructorId = instructorId;
		this.studentId = studentId;
		this.courseId = courseId;
		this.actived = actived;
		this.projectTitle = projectTitle;
		this.projectDescription = projectDescription;
		this.currentRateOfProgress = currentRateOfProgress;
		this.approvedByManager = approvedByManager;
	}

	public String getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(String instructorId) {
		this.instructorId = instructorId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public boolean isActived() {
		return actived;
	}

	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public byte getCurrentRateOfProgress() {
		return currentRateOfProgress;
	}

	public void setCurrentRateOfProgress(byte currentRateOfProgress) {
		this.currentRateOfProgress = currentRateOfProgress;
	}

	public boolean isApprovedByManager() {
		return approvedByManager;
	}

	public void setApprovedByManager(boolean approvedByManager) {
		this.approvedByManager = approvedByManager;
	}

	public ArrayList<Progress> getProgresses() {
		return progresses;
	}


	public void setProgresses(ArrayList<Progress> progresses) {
		this.progresses = progresses;
	}
	
}
