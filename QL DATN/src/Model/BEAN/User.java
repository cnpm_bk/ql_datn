package Model.BEAN;


/**
 * @author Nguyen Ba Anh
 * MAPPING DATABASE FOR TABLE "USER"
 */
public class User {
	// Attributes of table USER
	private String userId, fullName, gender, address, email, password, userType, dateOfBirth, phoneNumber;
	
	public User() {
		super();
	}
	
	public User(String userId, String fullName, String gender) {
		this.userId = userId;
		this.fullName = fullName;
		this.gender = gender;
	}

	public User(String userId, String fullName, String gender, String address,
			String email, String password, String userType, String dateOfBirth, String phoneNumber) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.gender = gender;
		this.address = address;
		this.email = email;
		this.password = password;
		this.userType = userType;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	
	
}
