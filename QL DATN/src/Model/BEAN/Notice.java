/**
 * 
 */
package Model.BEAN;

/**
 * @author NguyenBaAnh
 *
 */
public class Notice {

	String noticeId, userId, courseId, title, content, time;
	public Notice() {
		
	}
	public Notice(String noticeId, String userId, String courseId,
			String title, String content, String time) {
		super();
		this.noticeId = noticeId;
		this.userId = userId;
		this.courseId = courseId;
		this.title = title;
		this.content = content;
		this.time = time;
	}
	public String getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	

}
