package Model.BEAN;

/**
 * @author Nguyen Ba Anh
 *
 */
public class StudyField {
	private int fieldId;
	private String fieldName, fieldDetail;

	public StudyField(int fieldId, String fieldName, String fieldDetail) {
		this.fieldId = fieldId;
		this.fieldName = fieldName;
		this.fieldDetail = fieldDetail;
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldDetail() {
		return fieldDetail;
	}

	public void setFieldDetail(String fieldDetail) {
		this.fieldDetail = fieldDetail;
	}

}
