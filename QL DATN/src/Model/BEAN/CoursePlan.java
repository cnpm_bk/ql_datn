/**
 * 
 */
package Model.BEAN;

/**
 * @author NguyenBaAnh
 *
 */
public class CoursePlan {
	String courseId; // PK
	byte order; // PK
	String beginReportDate, endReportDate;
	
	public CoursePlan(String courseId, byte order, String beginReportDate, String endReportDate) {
		this.courseId = courseId;
		this.order = order;
		this.beginReportDate = beginReportDate;
		this.endReportDate = endReportDate;
	}
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public byte getOrder() {
		return order;
	}

	public void setOrder(byte order) {
		this.order = order;
	}

	public String getBeginReportDate() {
		return beginReportDate;
	}

	public void setBeginReportDate(String beginReportDate) {
		this.beginReportDate = beginReportDate;
	}

	public String getEndReportDate() {
		return endReportDate;
	}

	public void setEndReportDate(String endReportDate) {
		this.endReportDate = endReportDate;
	}
	
}
