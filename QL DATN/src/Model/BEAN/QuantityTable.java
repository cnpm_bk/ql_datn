/**
 * 
 */
package Model.BEAN;

/**
 * @author NguyenBaAnh
 *
 */
public class QuantityTable {
	private String instructorId, courseId;
	private byte maxNumberOfStudents, currentNumberOfStudents;

	public QuantityTable(String instructorId, String courseId, byte maxNumberOfStudents, byte currentNumberOfStudents) {
		super();
		this.instructorId = instructorId;
		this.courseId = courseId;
		this.maxNumberOfStudents = maxNumberOfStudents;
		this.currentNumberOfStudents = currentNumberOfStudents;
	}

	public String getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(String instructorId) {
		this.instructorId = instructorId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public byte getMaxNumberOfStudents() {
		return maxNumberOfStudents;
	}

	public void setMaxNumberOfStudents(byte maxNumberOfStudents) {
		this.maxNumberOfStudents = maxNumberOfStudents;
	}

	public byte getCurrentNumberOfStudents() {
		return currentNumberOfStudents;
	}

	public void setCurrentNumberOfStudents(byte currentNumberOfStudents) {
		this.currentNumberOfStudents = currentNumberOfStudents;
	}

	
}
