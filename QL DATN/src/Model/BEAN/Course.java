/**
 * 
 */
package Model.BEAN;

import java.util.ArrayList;

/**
 * @author NguyenBaAnh
 *
 */
public class Course {

	String courseId, courseName;
	boolean actived;
	String startDate, stopDate, beginRegistrationTime, endRegistrationTime;
	byte progressRate;
	int numberOfStudents, numberOfInstructors;
	ArrayList<CoursePlan> plan;

	public Course() {

	}

	public Course(String courseId, String courseName, boolean actived) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.actived = actived;
	}

	public Course(String courseId, String courseName, boolean actived, String startDate, String stopDate,
			String beginRegistrationTime, String endRegistrationTime, byte progressRate, int numberOfStudents,
			int numberOfInstructors, ArrayList<CoursePlan> plan) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.actived = actived;
		this.startDate = startDate;
		this.stopDate = stopDate;
		this.beginRegistrationTime = beginRegistrationTime;
		this.endRegistrationTime = endRegistrationTime;
		this.progressRate = progressRate;
		this.numberOfInstructors = numberOfInstructors;
		this.numberOfStudents = numberOfStudents;
		this.plan = plan;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public boolean isActived() {
		return actived;
	}

	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStopDate() {
		return stopDate;
	}

	public void setStopDate(String stopDate) {
		this.stopDate = stopDate;
	}

	public String getBeginRegistrationTime() {
		return beginRegistrationTime;
	}

	public void setBeginRegistrationTime(String beginRegistrationTime) {
		this.beginRegistrationTime = beginRegistrationTime;
	}

	public String getEndRegistrationTime() {
		return endRegistrationTime;
	}

	public void setEndRegistrationTime(String endRegistrationTime) {
		this.endRegistrationTime = endRegistrationTime;
	}

	public byte getProgressRate() {
		return progressRate;
	}

	public void setProgressRate(byte progressRate) {
		this.progressRate = progressRate;
	}

	public int getNumberOfStudents() {
		return numberOfStudents;
	}

	public void setNumberOfStudents(int numberOfStudents) {
		this.numberOfStudents = numberOfStudents;
	}

	public int getNumberOfInstructors() {
		return numberOfInstructors;
	}

	public void setNumberOfInstructors(int numberOfInstructors) {
		this.numberOfInstructors = numberOfInstructors;
	}

	public ArrayList<CoursePlan> getPlan() {
		return plan;
	}

	public void setPlan(ArrayList<CoursePlan> plan) {
		this.plan = plan;
	}
	
	
	

}
