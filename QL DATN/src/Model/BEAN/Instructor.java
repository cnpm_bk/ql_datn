/**
 * 
 */
package Model.BEAN;

import java.util.ArrayList;

/**
 * @author Nguyen Ba Anh
 *
 */
public class Instructor extends User {
	private String department, organization, levelOfStudy;
	private ArrayList<StudyField> studyField;
	private ArrayList<QuantityTable> qTable;

	public Instructor() {
		super();
	}

	/**
	 * @param userId
	 *            Mã người dùng
	 * @param fullName
	 *            Họ tên đầy đủ
	 * @param gender
	 *            Giới tính
	 * @param levelOfStudy
	 *            Trình độ học vấn
	 * @param studyField
	 *            Các hướng nghiên cứu
	 */
	public Instructor(String userId, String fullName, String gender, String levelOfStudy,
			ArrayList<StudyField> studyField) {
		super(userId, fullName, gender);
		this.levelOfStudy = levelOfStudy;
		this.studyField = studyField;
	}

	/**
	 * @param userId
	 *            Mã người dùng
	 * @param fullName
	 *            Họ tên đầy đủ
	 * @param gender
	 *            Giới tính
	 * @param address
	 *            Địa chỉ
	 * @param email
	 *            Thư điện tử
	 * @param password
	 *            Mật khẩu (!)
	 * @param userType
	 *            Kiểu người dùng
	 * @param dateOfBirth
	 *            Ngày sinh
	 * @param phoneNumber
	 *            Số điện thoại
	 * @param department
	 *            Khoa / Ban ngành
	 * @param organization
	 *            Trường / Tổ chức
	 * @param levelOfStudy
	 *            Trình độ học vấn
	 * @param studyField
	 *            Các hướng nghiên cứu
	 * @param qTable
	 *            Các bảng số lượng sinh viên đã/đang hướng dẫn
	 */
	public Instructor(String userId, String fullName, String gender, String address, String email, String password,
			String userType, String dateOfBirth, String phoneNumber, String department, String organization,
			String levelOfStudy, ArrayList<StudyField> studyField, ArrayList<QuantityTable> qTable) {
		super(userId, fullName, gender, address, email, password, userType, dateOfBirth, phoneNumber);
		this.department = department;
		this.organization = organization;
		this.levelOfStudy = levelOfStudy;
		this.studyField = studyField;
		this.qTable = qTable;
	}

	/**
	 * @return Khoa / Ban ngành
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param đặt
	 *            Khoa / Ban ngành
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return Trường / Tổ chức
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param đặt
	 *            Trường / Tổ chức
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return Trình độ
	 */
	public String getLevelOfStudy() {
		return levelOfStudy;
	}

	/**
	 * @param đặt
	 *            Trình độ
	 */
	public void setLevelOfStudy(String levelOfStudy) {
		this.levelOfStudy = levelOfStudy;
	}

	/**
	 * @return Các hướng nghiên cứu
	 */
	public ArrayList<StudyField> getStudyField() {
		return studyField;
	}

	/**
	 * @param đặt
	 *            Các hướng nghiên cứu
	 */
	public void setStudyField(ArrayList<StudyField> studyField) {
		this.studyField = studyField;
	}

	/**
	 * @return Bảng số lượng SV cần hướng dẫn
	 */
	public ArrayList<QuantityTable> getqTable() {
		return qTable;
	}

	/**
	 * @param đặt
	 *            Bảng số lượng SV cần hướng dẫn
	 */
	public void setqTable(ArrayList<QuantityTable> qTable) {
		this.qTable = qTable;
	}

	//=========================================================================
	/**
	 * Phương thức hỗ trợ, chuyển toàn bộ List hướng nghiên cứu của giảng viên
	 * thành dạng String
	 * 
	 * @return Chuỗi danh sách các hướng nghiên cứu của giảng viên
	 */
	public String getAllStudyFieldNames() {
		String buffer = "";
		for (StudyField s : this.studyField) {
			buffer += s.getFieldName() + ", ";
		}
		// buffer += "~";
		return buffer.replaceAll("(,\\s$)", "."); // Cuối câu đổi thành dấu .
	}

	/**
	 * Lấy số lượng sinh viên đã đăng kí chọn giảng viên để thực hiện đồ án
	 * 
	 * @param courseId
	 *            mã đợt mà giảng viên đang tham gia
	 * @return Số lượng sinh viên mà giảng viên sẽ hướng dẫn
	 */
	public byte getCurrentNumberOfStudentsByCourseId(String courseId) {
		byte num = 0;
		for (QuantityTable q : this.qTable) {
			if (q.getCourseId().equals(courseId)) {
				num = q.getCurrentNumberOfStudents();
				break;
			}
		}
		return num;
	}

	/**
	 * Lấy số lượng sinh viên tối đa mà giảng viên có thể hướng dẫn trong 1 đợt
	 * nào đó.
	 * 
	 * @param courseId
	 *            mã đợt mà giảng viên đang tham gia
	 * @return Số lượng sinh viên tối đa mà giảng viên có thể hướng dẫn
	 */
	public byte getMaxNumberOfStudentsByCourseId(String courseId) {
		byte num = 1;
		for (QuantityTable q : this.qTable) {
			if (q.getCourseId().equals(courseId)) {
				num = q.getMaxNumberOfStudents();
				break;
			}
		}
		return num;
	}

	/**
	 * Kiểm tra tình trạng số lượng sinh viên của một giảng viên trong ở 1 đợt
	 * nào đó đã đầy hay chưa?
	 * 
	 * @param courseId
	 *            mã đợt mà giảng viên đang tham gia
	 * @return true nếu số lượng sinh viên đăng kí của GV đã đầy, false nếu số
	 *         lượng sinh viên đăng kí của GV chưa đầy
	 */
	public boolean isQTableFull(String courseId) {
		QuantityTable q = null;
		for (QuantityTable qTable : this.qTable) {
			if (qTable.getCourseId().equals(courseId)) {
				q = qTable;
				break;
			}
		}
		if (q != null) {
			return (q.getCurrentNumberOfStudents() >= q.getMaxNumberOfStudents());
		} else {
			return false;
		}
	}

}
