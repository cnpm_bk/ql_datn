/**
 * 
 */
package Model.BEAN;

/**
 * @author NguyenBaAnh
 *
 */
public class Progress {
	String courseId, studentId;
	byte rateOfProgress;
	String reportDate, reportTitle, reportContent, optionalLink;
	boolean approvedByStudent, approvedByInstructor, approvedByManager;

	public Progress(String courseId, String studentId, byte rateOfProgress, String reportDate, String reportTitle,
			String reportContent, String optionalLink, boolean approvedByStudent, boolean approvedByInstructor,
			boolean approvedByManager) {
		this.courseId = courseId;
		this.studentId = studentId;
		this.rateOfProgress = rateOfProgress;
		this.reportDate = reportDate;
		this.reportTitle = reportTitle;
		this.reportContent = reportContent;
		this.optionalLink = optionalLink;
		this.approvedByStudent = approvedByStudent;
		this.approvedByInstructor = approvedByInstructor;
		this.approvedByManager = approvedByManager;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public byte getRateOfProgress() {
		return rateOfProgress;
	}

	public void setRateOfProgress(byte rateOfProgress) {
		this.rateOfProgress = rateOfProgress;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String getReportContent() {
		return reportContent;
	}

	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}

	public String getOptionalLink() {
		return optionalLink;
	}

	public void setOptionalLink(String optionalLink) {
		this.optionalLink = optionalLink;
	}

	public boolean isApprovedByStudent() {
		return approvedByStudent;
	}

	public void setApprovedByStudent(boolean approvedByStudent) {
		this.approvedByStudent = approvedByStudent;
	}

	public boolean isApprovedByInstructor() {
		return approvedByInstructor;
	}

	public void setApprovedByInstructor(boolean approvedByInstructor) {
		this.approvedByInstructor = approvedByInstructor;
	}

	public boolean isApprovedByManager() {
		return approvedByManager;
	}

	public void setApprovedByManager(boolean approvedByManager) {
		this.approvedByManager = approvedByManager;
	}

}
