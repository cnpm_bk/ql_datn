/**
 * 
 */
package Model.BO;

import java.util.ArrayList;

import Model.BEAN.Progress;
import Model.BEAN.Project;
import Model.BEAN.User;
import Model.DAO.ProjectDAO;

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class ProjectBO {
	ProjectDAO projectDAO;

	public ProjectBO() {
		projectDAO = new ProjectDAO();
	}
	
	/**
	 * Đăng kí đồ án: chọn giảng viên trong 1 đợt
	 * @param courseId
	 * @param instructorId
	 * @param studentId
	 * @return
	 */
	public boolean registerProject(String courseId, String instructorId, String studentId) {
		return projectDAO.registerProject(courseId, instructorId, studentId);
	}

	/**
	 * Lấy mã giảng viên mà sinh viên đã chọn trong 1 đợt
	 * @param courseId
	 * @param studentId
	 * @return
	 */
	public String getInstructorId(String courseId, String studentId) {
		return projectDAO.getInstructorId(courseId, studentId);
	}
	
	/**
	 * Hủy bỏ đăng kí, xóa giảng viên đã chọn
	 * @param courseId
	 * @param instructorId
	 * @param studentId
	 * @return
	 */
	public boolean removeProject(String courseId, String instructorId, String studentId) {
		return projectDAO.removeProject(courseId, instructorId, studentId);
	}
	
	/**
	 * Lấy toàn bộ danh sách đồ án mà sinh viên đã đăng kí trong 1 đợt
	 * @param courseId
	 * @return
	 */
	public ArrayList<Project> getProjectListByCourseId(String courseId) {
		return projectDAO.getProjectListByCourseId(courseId);
	}
	
	/**
	 * Lấy toàn bộ danh sách đồ án của một sinh viên trong toàn hệ thống
	 * @param studentId
	 * @return
	 */
	public ArrayList<Project> getProjectListByStudentId(String studentId) {
		// TODO Auto-generated method stub
		return projectDAO.getProjectListByStudentId(studentId);
	}

	/**
	 * Cập nhật thông tin đồ án
	 * @param projectInfo
	 * @return
	 */
	public boolean updateProjectInformation(Project projectInfo) {
		// TODO Auto-generated method stub
		return projectDAO.updateProjectInformation(projectInfo);
	}

	/**
	 * Nộp tiến độ
	 * @param progress
	 * @return
	 */
	public boolean updateProgress(Progress progress) {
		// TODO Auto-generated method stub
		// Lọc 1 số thông tin
		return projectDAO.updateProgress(progress);
	}

	// ================================================================
	// Quang edited from here

	/**
	 * Lấy đồ án trong một đợt hướng dẫn của sinh viên
	 * @param studentId
	 * @param courseId
	 * @return
	 */
	public Project getProjectByStudentIdAndCourseId(String studentId, String courseId) {
		return projectDAO.getProjectByStudentIdAndCourseId(studentId, courseId);
	}

	/**
	 * Lấy toàn bộ danh sách sinh viên trong một đợt hướng dẫn của một giảng viên
	 * @param instructorId
	 * @param courseId
	 * @return
	 */

	public ArrayList<User> getStudentByInstructorIdAndCourseId(String instructorId, String courseId) {
		return projectDAO.getStudentByInstructorIdAndCourseId(instructorId, courseId);
	}

	/**
	 * Thay đổi giảng viên hướng dẫn của một sinh viên trong một đợt
	 * @param studentId
	 * @param courseId
	 * @param newInstructorId
	 * @param oldInstructorId
	 * @return
	 */

	public boolean changeInstructorByStudentIdAndCourseId(String studentId, String courseId, String newInstructorId, String oldInstructorId) {
		return projectDAO.changeInstructorByStudentIdAndCourseId(studentId, courseId, newInstructorId, oldInstructorId);
	}
}
