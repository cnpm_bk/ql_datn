/**
 * Các thao tác liên quan đến giảng viên
 */
package Model.BO;

import java.util.ArrayList;

import Model.BEAN.Instructor;
import Model.DAO.InstructorDAO;
import Model.DAO.UserDAO;
import Utils.ResponseCode;
import Utils.SupportTool;

/**
 * @author NguyenBaAnh
 *
 */
public class InstructorBO {

	InstructorDAO instructorDAO;
	UserDAO userDAO;
	SupportTool tool;

	public InstructorBO() {
		instructorDAO = new InstructorDAO();
		userDAO = new UserDAO();
		tool = new SupportTool();
	}

	/**
	 *  Lấy 1 giảng viên theo mã người dùng
	 * @param userId
	 * @return
	 */
	public Instructor getInstructorById(String userId) {
		// Kiểm tra null
		if (tool.checkNull(userId)) {
			return null;
		}
		// Kiểm tra hợp lệ:
		// Không rỗng và tồn tại trong hệ thống, và phải là giảng viên.
		if (!tool.checkEmptyStrings(userId) && userDAO.isExist(userId)
				&& instructorDAO.validateInstructorById(userId)) {
			return instructorDAO.getInstructorById(userId);
		} else {
			return null; // Nếu nhập vào linh tinh thì cho về rỗng
		}
	}
	
	/**
	 *  Lấy danh sách giảng viên theo mã đợt
	 * @param courseId
	 * @return
	 */
	public ArrayList<Instructor> getInstructorListByCourseId(String courseId) {
		return instructorDAO.getInstructorListByCourseId(courseId);
	}

	/**
	 * Cập nhật thông tin giảng viên
	 * @param instructor thông tin giảng viên
	 * @return
	 */
	public ResponseCode updateInformation(Instructor instructor) {
		// Kiểm tra null
		if (tool.checkNull(instructor)) {
			return ResponseCode.NULL;
		}
		// Kiểm tra ngày sinh có đúng không
		//if (tool.checkTimeStamp(instructor.getDateOfBirth())) {
		//	return ResponseCode.TIME_NOT_VALID;
		//}
		// Kiểm tra họ tên có bị bỏ trống không
		if (tool.checkEmptyStrings(instructor.getFullName())) {
			return ResponseCode.EMPTY;
		}
		// Chuyển cho DAO xử lí
		if (instructorDAO.updateInformation(instructor)) {
			return ResponseCode.SUCCESS;
		} else {
			return ResponseCode.FAILED;
		}
	}

	/**
	 * Lấy toàn bộ giảng viên
	 * @return
	 */
	public ArrayList<Instructor> getAllInstructors() {
		return instructorDAO.getAllInstructors();
	}
	
	/**
	 * Đổi số lượng sinh viên mỗi giảng viên được hướng dẫn
	 * @param courseId
	 * @param instructorId
	 * @param maxNumberOfStudents
	 * @return
	 */
	public boolean changeMaxNumberOfStudents(String courseId, String instructorId, byte maxNumberOfStudents) {
		return instructorDAO.changeMaxNumberOfStudents(courseId, instructorId, maxNumberOfStudents);
	}
}
