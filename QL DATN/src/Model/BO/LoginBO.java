/**
 * Thao tác xác thực, đăng nhập và đăng xuất 
 */
package Model.BO;

import java.sql.SQLException;

import Model.BEAN.User;
import Model.DAO.*;

/**
 * @author Nguyen Ba Anh
 *
 */
public class LoginBO {
	private LoginDAO loginDAO;
	// Constructor
	public LoginBO() {
		loginDAO = new LoginDAO();
	}

	/**
	 *  Hàm kiểm tra tính hợp lệ của user
	 * @param userid
	 * @param pass
	 * @return
	 * @throws SQLException
	 */
	public boolean validate(String userid, String pass) throws SQLException {
		// Nếu tài khoản hoặc mật khẩu rỗng thì đăng nhập thất bại
		if (userid == null || pass == null || userid.equals("") || pass.equals("")) {
			return false;
		} else if (loginDAO.validate(userid, pass))
			return true;
		else
			return false;
	}

	/**
	 * Lấy ra người dùng đang đăng nhập vào hệ thống
	 * @return
	 */
	public User getCurrentUser() {
		return loginDAO.getCurrentUser();
	}
}
