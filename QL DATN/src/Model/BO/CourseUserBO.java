/**
 * Lớp thực hiện các chức năng liên quan đến người dùng của các đợt trong hệ thống
 */
package Model.BO;

import Model.DAO.CourseUserDAO;

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class CourseUserBO {
	CourseUserDAO courseUserDAO;

	public CourseUserBO() {
		courseUserDAO = new CourseUserDAO();
	}

	/**
	 * Kiểm tra xem một người có được kích hoạt hay không? Theo sự vận hành hệ
	 * thống thì mỗi người dùng chỉ được kích hoạt tại tối đa 1 đợt ở 1 thời
	 * điểm. Nếu trả về FALSE thì người dùng không có đợt nào hoạt động
	 * 
	 * @param userId
	 * @return
	 */
	public boolean isActivedUser(String userId) {
		return courseUserDAO.isActivedUser(userId);
	}

	/**
	 * Theo sự vận hành hệ thống thì mỗi người dùng chỉ được kích hoạt tại tối
	 * đa 1 đợt ở 1 thời điểm. Nếu trả về mã đợt thì người đó có tham
	 * gia vào hệ thống, trả về nul thì không tồn tại trong đợt nào.
	 * 
	 * @param userId
	 * @return
	 */
	public String getCourseIdByUserId(String userId) {
		return courseUserDAO.getCourseIdByUserId(userId);
	}


	/**
	 * Thêm giảng viên mới vào đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @param instructorId
	 *            mã giảng viên
	 * @return
	 */
	public boolean addInstructorIntoCourse(String courseId, String instructorId) {
		return courseUserDAO.addInstructorIntoCourse(courseId, instructorId);

	}

	/**
	 * Xóa giảng viên ra khỏi đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @param instructorId
	 *            mã giảng viên
	 * @return
	 */
	public boolean removeInstructorFromCourse(String courseId, String instructorId) {
		return courseUserDAO.removeInstructorFromCourse(courseId, instructorId);
	}

	// ===============================================================
	// Quang edited from here

	/**
	 * Xóa sinh viên ra khỏi đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @param studentId
	 *            mã sinh viên
	 * @return
	 */
	public boolean removeStudentFromCourse(String courseId, String studentId) {
		return courseUserDAO.remoreStudentFromCourse(courseId, studentId);
	}
}
