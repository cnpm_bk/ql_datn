/**
 * Các thao tác liên quan sinh viên
 */
package Model.BO;

import java.util.ArrayList;
import java.util.Vector;

import Model.BEAN.Student;
import Model.DAO.StudentDAO;

/**
 * @author NguyenBaAnh
 *
 */
public class StudentBO {

	/**
	 * 
	 */
	StudentDAO studentDAO;

	public StudentBO() {
		studentDAO = new StudentDAO();
	}

	/**
	 * Lấy ra 1 sinh viên theo mã người dùng
	 * @param userId
	 * @return
	 */
	public Student getStudentById(String userId) {
		// Nếu tồn tại người dùng có userId thì trả về, ko thì coi như null
		if (studentDAO.validateStudentById(userId))
			return studentDAO.getStudentById(userId);
		else
			return null;
	}

	/**
	 * Cập nhật thông tin sinh viên
	 * @param userId
	 * @param fullName
	 * @param dateOfBirth
	 * @param phoneNumber
	 * @param address
	 * @param email
	 * @return
	 */
	public boolean updateInformation(String userId, String fullName, String dateOfBirth,
			String phoneNumber, String address, String email) {
		// Kiểm tra các thông tin xem hợp lệ chưa, chưa hợp lệ chỗ nào thì cho về false
		//.... code cái này sau
		// FIXME
		// chuyển giao cho DAO xử lí
		if(studentDAO.updateInformation(userId, fullName, dateOfBirth, phoneNumber, address, email)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Lấy danh sách tất cả sinh viên của 1 đợt
	 * @param courseId
	 * @return
	 */
	public ArrayList<Student> getStudentListByCourseId(String courseId) {
		return studentDAO.getStudentListByCourseId(courseId);
	}

	/**
	 * Nhập sinh viên từ Vector (đc tách ra từ file Excel)
	 * @param courseId 
	 * @param studentVector
	 * @return
	 */
	public boolean addStudentsFromVector(String courseId, Vector studentVector) {
		//FIXME - kiểm tra header xem đúng định dạng chưa, nếu chưa thì trả lui
		return studentDAO.addStudentsFromVector(courseId, studentVector);
	}
}
