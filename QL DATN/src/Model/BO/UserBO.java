/**
 * Thao tác liên quan đến người dùng nói chung
 */
package Model.BO;

import Model.DAO.UserDAO;
import Utils.ResponseCode;

/**
 * @author NguyenBaAnh
 *
 */
public class UserBO {
	UserDAO userDAO;
	public UserBO() {
		userDAO = new UserDAO();
	}
	
	public boolean addUser() {
		return true;
	}
	
	/**
	 * Đổi mật khẩu
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @param confirmedPassword
	 * @return
	 */
	public ResponseCode changePassword(String userId, String oldPassword, String newPassword, String confirmedPassword) {
		if(oldPassword.equals("") || newPassword.equals("") || confirmedPassword.equals("")) {
			// Không nhập mà nhấn nút :| hoặc nhập ko đủ
			return ResponseCode.EMPTY;
		} else if (!newPassword.equals(confirmedPassword)) {
			// Nhập mà không khớp
			return ResponseCode.PASSWORD_NOT_MATCH;
		} else if(userDAO.changePassword(userId, oldPassword, newPassword)) {
			// Còn lại giao cho DAO xử lí
			return ResponseCode.SUCCESS;
		} else return ResponseCode.FAILED;
	}

}
