/**
 * 
 */
package Model.BO;

import java.util.ArrayList;

import Model.BEAN.Notice;
import Model.DAO.NoticeDAO;

/**
 * @author NguyenBaAnh
 *
 */
public class NoticeBO {
	NoticeDAO noticeDAO;
	
	public NoticeBO() {
		noticeDAO = new NoticeDAO();
	}

	/**
	 * Lấy tất cả thông báo
	 * @return
	 */
	public ArrayList<Notice> getAllNotices() {
		return noticeDAO.getAllNotices();
	}
	
	/**
	 * Lấy thông báo của mỗi người đã đăng
	 * @param userId
	 * @return
	 */
	public ArrayList<Notice> getUserNotices(String userId) {
		return noticeDAO.getUserNotices(userId);
	}

	/**
	 * Thêm thông báo mới
	 * @param notice
	 * @return
	 */
	public boolean addNewNotice(Notice notice) {
		return noticeDAO.addNewNotice(notice);
		
	}

	/**
	 * Xóa thông báo theo tiêu đề
	 * @param title (PK)
	 * @return
	 */
	public boolean deteleNoticeByTitle(String title) {
		return noticeDAO.deteleNoticeByTitle(title);	
	}

}
