/**
 * Lớp thực hiện các chức năng liên quan đến các đợt trong hệ thống
 */
package Model.BO;

import java.util.ArrayList;

import Model.BEAN.Course;
import Model.BEAN.CoursePlan;
import Model.DAO.CourseDAO;
import Utils.ResponseCode;
import Utils.SupportTool;

/**
 * @author NguyenBaAnh
 *
 */
public class CourseBO {
	CourseDAO courseDAO; // đối tượng CSDL
	SupportTool tool; // công cụ hỗ trợ

	public CourseBO() {
		courseDAO = new CourseDAO();
		tool = new SupportTool();
	}

	/**
	 * Lấy thông tin của đợt mới nhất trong hệ thống
	 * 
	 * @return
	 */
	public Course getTheNewestCourse() {
		return courseDAO.getTheNewestCourse();
	}

	/**
	 * Lấy thông tin một đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @return
	 */
	public Course getCourseById(String courseId) {
		if (courseId != null)
			return courseDAO.getCourseById(courseId);
		else
			return null;
	}

	/**
	 * Đăt thời gian đăng kí cho đợt
	 * 
	 * @param beginRegistrationTime
	 *            thời gian bắt đầu
	 * @param endRegistrationTime
	 *            thời gian kết thúc
	 * @param courseId
	 *            mã đợt
	 * @return SUCCESS thành công. FAILED thất bại. TIME_NOT_VALID nếu sai định
	 *         dạng thời gian. ILLOGICAL nếu thời gian bị xung đột.
	 */
	public ResponseCode changeRegistrationTime(String beginRegistrationTime, String endRegistrationTime,
			String courseId) {
		// Kiểm tra xem 2 ngày có hợp lệ không?
		if (tool.checkTimeStamp(beginRegistrationTime, endRegistrationTime)) {
			// Lấy thông tin đợt
			Course course = courseDAO.getCourseById(courseId);
			// Tiếp tục kiểm tra sự tương quan giữa 2 ngày,
			// thời gian đăng kí phải trước đợt hoạt động
			if (tool.checkValidTime(beginRegistrationTime, endRegistrationTime)
					&& tool.checkValidTime(endRegistrationTime, course.getStartDate())) {
				// Nếu vượt qua được thì giao cho DAO xử lí
				if (courseDAO.changeRegistrationTime(beginRegistrationTime, endRegistrationTime, courseId)) {
					return ResponseCode.SUCCESS;
				} else {
					return ResponseCode.FAILED;
				}
			} else {
				return ResponseCode.ILLOGICAL;
			} // KIỂM TRA TƯƠNG QUAN THỜI GIAN VỚI THỜI GIAN ĐỢT
				// FIXME

		} else {
			return ResponseCode.TIME_NOT_VALID;
		}

	}

	/**
	 * Lấy mã + tên đợt của tất cả các đợt trong hệ thống
	 * 
	 * @return
	 */
	public ArrayList<Course> getCourseIdAndName() {
		return courseDAO.getCourseIdAndName();
	}

	/**
	 * Đổi số lần tiến độ
	 * 
	 * @param courseId
	 *            mã đợt
	 * @param progressRate
	 *            số lần báo cáo
	 * @return SUCCESS thành công. FAILED thất bại. ILLOGICAL nếu số lần nộp quá
	 *         nhiều (>5) hoặc âm.
	 */
	public ResponseCode changeProgressRate(String courseId, byte progressRate) {
		if (progressRate <= 0 || progressRate > 5) {
			return ResponseCode.ILLOGICAL;
		} else if (courseDAO.changeProgressRate(courseId, progressRate)) {
			return ResponseCode.SUCCESS;
		} else {
			return ResponseCode.FAILED;
		}
	}

	/**
	 * Tạo mới đợt
	 * 
	 * @param newCourse
	 *            đợt mới
	 * @return
	 */
	public ResponseCode createNewCourse(Course newCourse) {
		// Kiểm tra null
		if (tool.checkNull(newCourse)) {
			return ResponseCode.NULL;
		}
		// Kiểm tra trống
		if (tool.checkEmptyStrings(newCourse.getCourseId(), newCourse.getCourseName(), newCourse.getStartDate(),
				newCourse.getStopDate())) {
			return ResponseCode.EMPTY;
		}
		// Kiểm tra số liệu ngày tháng hợp lệ không
		if (tool.checkTimeStamp(newCourse.getStartDate(), newCourse.getStopDate(), newCourse.getBeginRegistrationTime(),
				newCourse.getEndRegistrationTime())) {
			// Kiểm tra ràng buộc về ngày tháng
			if (tool.checkValidTime(newCourse.getStartDate(), newCourse.getStopDate())
					&& tool.checkValidTime(newCourse.getBeginRegistrationTime(), newCourse.getEndRegistrationTime())
					&& tool.checkValidTime(newCourse.getEndRegistrationTime(), newCourse.getStartDate())) {
				// Giao cho DAO xử lí
				if (courseDAO.createNewCourse(newCourse)) {
					return ResponseCode.SUCCESS;
				} else {
					return ResponseCode.FAILED;
				}

			} else {
				return ResponseCode.ILLOGICAL;
			}

		} else {
			return ResponseCode.TIME_NOT_VALID;
		}
	}

	/**
	 * Ngừng hoạt động của 1 đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @return
	 */
	public boolean stopCourseById(String courseId) {
		return courseDAO.stopCourseById(courseId);
	}

	/**
	 * Thay đổi lịch báo cáo tiến độ / nộp đồ án
	 * 
	 * @param coursePlan
	 * @param update 
	 * @return
	 */
	public ResponseCode changeCoursePlan(CoursePlan coursePlan, String update) {
		// Kiểm tra thời gian của so với giới hạn thời gian của đợt
		Course course = (new CourseBO()).getCourseById(coursePlan.getCourseId());
		if (course.getCourseId() != null) {
			if (tool.checkValidTime(course.getStartDate(), coursePlan.getBeginReportDate())
					&& tool.checkValidTime(coursePlan.getEndReportDate(), course.getStopDate())
					&& tool.checkValidTime(coursePlan.getBeginReportDate(), coursePlan.getEndReportDate())) {
				if (courseDAO.changeCoursePlan(coursePlan, update)) {
					return ResponseCode.SUCCESS;
				} else {
					return ResponseCode.FAILED;
				}
			} else {
				return ResponseCode.ILLOGICAL;
			}
		} else {
			return ResponseCode.NULL;
		}
	}

}
