/**
 * 
 */
package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author NguyenBaAnh
 *
 */
public class CourseUserDAO extends Database {
	public CourseUserDAO() {
		super();
	}

	public boolean isActivedUser(String userId) {
		String query = "select * from COURSE_USER where (userId='" + userId + "' and actived=true)";
		ResultSet rs;
		try {
			rs = execute(query);
			if(rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getCourseIdByUserId(String userId) {
		String query = "select courseId from COURSE_USER where (userId='" + userId + "' and actived=true)";
		ResultSet rs;
		try {
			rs = execute(query);
			if(rs.next()) {
				return rs.getString("courseId");
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Thêm 1 giảng viên mới vào đợt
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param instructorId
	 *            mã giảng viên (PK2)
	 * @return
	 */
	public boolean addInstructorIntoCourse(String courseId, String instructorId) {
		// Cập nhật bảng USER_COURSE
		String query = "insert into COURSE_USER(courseId, userId, actived) values('" + courseId + "','" + instructorId
				+ "', true)";
		// Cập nhật bảng COURSE (numberOfInstructors)
		String query2 = "update COURSE set numberOfInstructors=(numberOfInstructors+1) where courseId='" + courseId
				+ "'";
		// Cập nhật bảng số lượng QUANTITY_TABLE
		String query3 = "insert into QUANTITY_TABLE(instructorId, courseId, maxNumberOfStudents, currentNumberOfStudents) values('"
				+ instructorId + "','" + courseId + "',1,0)";
		try {
			update(query); // Thêm 1 giảng viên
			update(query2); // Lệnh 1 thành công thì cộng số lượng lên 1
			update(query3); // Lệnh 1,2 thành công thì đặt số lượng sinh viên
							// mặc định cho gv mới
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Xóa 1 giảng viên ra khỏi danh sách đợt
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param instructorId
	 *            mã giảng viên (PK2)
	 * @return
	 */
	public boolean removeInstructorFromCourse(String courseId, String instructorId) {
		String query = "delete * from QUANTITY_TABLE where courseID ='" + courseId + "' and instructorId='"
				+ instructorId + "' and currentNumberOfStudents=0";
		String query2 = "delete * from COURSE_USER where courseID ='" + courseId + "' and userId='" + instructorId
				+ "'";
		String query3 = "update COURSE set numberOfInstructors=(numberOfInstructors-1) where courseId='" + courseId
				+ "'";
		InstructorDAO instructorDAO = new InstructorDAO();
		if (instructorDAO.validateInstructorById(instructorId)) {
			try {
				if (update(query) != 0) { // Xóa bảng số lượng QUANTITY_TABLE
					update(query2); // Xóa thành công thì xóa bảng COURSE_USER
					update(query3); // Giảm số lượng ở bảng COURSE
					return true;
				} else {
					return false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	/**
	 * Thêm sinh viên vào 1 đợt
	 * 
	 * @param courseId
	 * @param studentId
	 * @return
	 */
	public boolean addStudentIntoCourse(String courseId, String studentId) {
		// Cập nhật bảng USER_COURSE
		String query = "insert into COURSE_USER(courseId, userId, actived) values('" + courseId + "','" + studentId
				+ "', true)";
		// Cập nhật bảng COURSE (numberOfStudents)
		String query2 = "update COURSE set numberOfStudents=(numberOfStudents+1) where courseId='" + courseId + "'";
		try {
			update(query); // Thêm 1 giảng viên
			update(query2); // Lệnh 1 thành công thì cộng số lượng lên 1
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Kiểm tra xem sinh viên có sẵn sàng cho đợt hướng dẫn đồ án mới hay không?
	 * @param studentId
	 * @return
	 */
	public boolean isStudentReady(String studentId) {
		StudentDAO studentDAO = new StudentDAO();
		if (studentDAO.validateStudentById(studentId)) {
			String query = "select * from COURSE_USER where userId='"+studentId+"' and actived=true";
			try {
				ResultSet rs = execute(query);
				if(rs.next()) { // Nếu sinh viên đang làm 1 đồ án nào đó
					return false; //thì chưa sẵn sàng cho đồ án mới
				} else {
					return true; // nếu không làm đồ án nào thì sẵn sàng
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	// ===========================================================
	// Quang edited from here

	/**
	 * Xóa 1 sinh viên ra khỏi danh sách đợt
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param studentId
	 *            mã sinh viên (PK2)
	 * @return
	 */

	public boolean remoreStudentFromCourse(String courseId, String studentId) {
		String query = "delete * from COURSE_USER where courseID ='" + courseId + "' and userId='"
				+ studentId + "'";
		String query2 = "update COURSE set numberOfStudents=(numberOfStudents-1) where courseId='" + courseId
				+ "'";
			try {
				if (update(query) != 0) { // Xóa bảng số lượng COURSE_USER
					update(query2); // Giảm số lượng ở bảng COURSE
					return true;
				} else {
					return false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
	}

}
