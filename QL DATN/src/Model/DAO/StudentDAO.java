package Model.DAO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;

import Model.BEAN.Student;
import Utils.SupportTool;

public class StudentDAO extends Database {
	Student student;

	public StudentDAO() {
		student = new Student();
	}

	/**
	 * Xác thực xem Mã người dùng có phải của sinh viên hay không
	 * 
	 * @param userId
	 *            mã người dùng
	 * @return true nếu là của sinh viên, false nếu không phải
	 */
	public boolean validateStudentById(String userId) {
		String query = "Select userId from STUDENT where userId='" + userId + "'";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				resultSet.close();
				return true;
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Lấy toàn bộ thông tin sinh viên từ Mã sinh viên
	 * 
	 * @param studentId
	 *            mã sinh viên
	 * @return
	 */
	public Student getStudentById(String studentId) {
		String query = "Select * from USER, STUDENT where STUDENT.userId='" + studentId
				+ "' and USER.userId=STUDENT.userId";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				Student student = new Student();
				student.setUserId(resultSet.getString("STUDENT.userId"));
				student.setFullName(resultSet.getString("fullName"));
				student.setGender(resultSet.getString("gender"));
				student.setDateOfBirth(resultSet.getString("dateOfBirth"));
				student.setEmail(resultSet.getString("email"));
				student.setPhoneNumber(resultSet.getString("phoneNumber"));
				student.setAddress(resultSet.getString("address"));
				student.setPassword(resultSet.getString("password"));
				student.setStudentClass(resultSet.getString("studentClass"));
				student.setDepartment(resultSet.getString("department"));
				student.setOrganization(resultSet.getString("organization"));
				return student;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	/**
	 * Cập nhật thông tin
	 * 
	 * @param userId
	 * @param fullName
	 * @param dateOfBirth
	 * @param phoneNumber
	 * @param address
	 * @param email
	 * @return true nếu thành công, false nếu thất bại
	 */
	public boolean updateInformation(String userId, String fullName, String dateOfBirth, String phoneNumber,
			String address, String email) {
		String query = "update USER set fullName='" + fullName + "', dateOfBirth='" + dateOfBirth + "', phoneNumber='"
				+ phoneNumber + "', address='" + address + "', email='" + email + "' where userId='" + userId + "'";
		try {
			update(query); // Cập nhật USER
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Lấy danh sách sinh viên thao đợt
	 * 
	 * @param courseId
	 *            mã đợt
	 * @return danh sách các sinh viên (đầy đủ thông tin) trong đợt
	 */
	public ArrayList<Student> getStudentListByCourseId(String courseId) {
		String query = "select * from COURSE_USER where courseId='" + courseId + "'";
		try {
			ResultSet rs = execute(query);
			ArrayList<Student> students = new ArrayList<Student>();
			while (rs.next()) {
				// Lấy mã người dùng
				String userId = rs.getString("userId");
				// Nếu là sinh viên thì thêm vào list
				if (this.validateStudentById(userId)) {
					students.add(this.getStudentById(userId));
				}
			}
			return students;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean addStudentsFromVector(String courseId, Vector studentVector) {
		// TODO Auto-generated method stub
		UserDAO userDAO = new UserDAO();
		CourseDAO courseDAO = new CourseDAO();
		StringBuilder log = new StringBuilder(); // nội dung file log
		SupportTool tool = new SupportTool();
		log.append("Ghi chép thao tác thêm sinh viên đợt " + courseId + "\n");
		log.append("Thời gian: " + tool.getCurrentDateTime().toString().substring(0, 19) + "\n\n");
		// Duyệt từng dòng
		row: for (int i = 1; i < studentVector.size(); i++) {
			// Tách từng dòng
			Vector student = (Vector) studentVector.elementAt(i);
			for (int j = 0; j < student.size(); j++) {
				HSSFCell myCell = (HSSFCell) student.elementAt(j);
				String stringCellValue = myCell.toString();
			}
			// Nếu là sinh viên mới
			if (!userDAO.isExist(String.valueOf(student.get(0)))) {
				log.append("Sinh viên mới [" + String.valueOf(student.get(0)) + "] : ");
				String query1 = String.format(
						"insert into USER(userId, fullName, gender, address, password, dateOfBirth)"
								+ "values('%s', '%s', '%s', '%s', '123456', '%s')",
						student.get(0), student.get(1), student.get(2), student.get(3), student.get(4));
				String query2 = String.format(
						"insert into STUDENT(userId, studentClass, department, organization)"
								+ "values('%s', '%s', '%s', '%s')",
						student.get(0), student.get(5), student.get(6), student.get(7));
				try {
					update(query1);
					update(query2);
					log.append("thêm " + String.valueOf(student.get(1)) + " vào hệ thống thành công.\n");
				} catch (SQLException e) {
					e.printStackTrace();
					log.append("thêm " + String.valueOf(student.get(1)) + " vào hệ thống  thất bại.\n");
					continue row;
				}
			}
			// Nếu đã tồn tại trong hệ thống
			log.append("Sinh viên đã tồn tại trong hệ thống [" + String.valueOf(student.get(0)) + "] : ");
			CourseUserDAO courseUserDAO = new CourseUserDAO();
			// Kiểm tra xem sinh viên có đang làm đồ án nào không, nếu có thì
			// không được phép!
			if (courseUserDAO.isStudentReady(String.valueOf(student.get(0)))) {
				if (courseUserDAO.addStudentIntoCourse(courseId, String.valueOf(student.get(0)))) {
					log.append("thêm " + String.valueOf(student.get(1)) + " vào đợt thành công.\n\n");
				} else {
					log.append("thêm " + String.valueOf(student.get(1)) + " vào đợt thất bại.\n\n");
				}
			} else {
				log.append("Sinh viên " + String.valueOf(student.get(1))
						+ " đang tiến hành đồ án khác, thêm sinh viên vào đợt thất bại.\n\n");
			}
		}
		// In file log
		try {
			String path = "D:/" + courseId + "-students.txt";
			Files.write(Paths.get(path), log.toString().getBytes("UTF-8"), StandardOpenOption.CREATE);
			String path2 = "D:/" + courseId + "-studentData.txt";
			Files.write(Paths.get(path2), tool.convertCellDataToString(studentVector).getBytes("UTF-8"),
					StandardOpenOption.CREATE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
}
