package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import Model.BEAN.User;

/**
 * @author Nguyen Ba Anh
 *
 */
public class LoginDAO extends Database {
	private User currentUser = null;
	// private Database database = null;
	UserDAO userDAO;
	public LoginDAO() {
		super(); // new Database();
		userDAO = new UserDAO();
	}

	public boolean validate(String userid, String pass) throws SQLException {
		// Tạo truy vấn với 2 thông tin nhập vào
		String query = "Select * from USER where userId='" + userid + "' and password='" + pass + "'";
		ResultSet resultSet = execute(query);
		// Nếu thông tin có trong CSDL
		if (resultSet.next()) {
			// Trích thông tin
			String userId = resultSet.getString("userId");
			String fullName = resultSet.getString("fullName");
			String gender = resultSet.getString("gender");
			String address = resultSet.getString("address");
			String password = resultSet.getString("password");
			String email = resultSet.getString("email");
			String dateOfBirth = resultSet.getString("dateOfBirth");
			String phoneNumber = resultSet.getString("phoneNumber");
			String userType = userDAO.getUserType(userId);

			// Đẩy vào Model.Bean
			setCurrentUser(
					new User(userId, fullName, gender, address, email, password, userType, dateOfBirth, phoneNumber));
			resultSet.close();
			return true;
		} else
			return false;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

}
