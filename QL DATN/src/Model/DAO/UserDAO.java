/**
 * 
 */
package Model.DAO;

import java.sql.SQLException;

/**
 * @author NguyenBaAnh
 *
 */
public class UserDAO extends Database {

	/**
	 * 
	 */
	public UserDAO() {
		super(); // new Database()
	}

	public boolean changePassword(String userId, String oldPassword, String newPassword) {
		// Truy vấn kiểm tra tính hợp lệ của tài khoản
		String check = "select * from USER where userId='" + userId + "' and password='" + oldPassword + "'";

		// Tạo truy vấn đổi mật khẩu
		String query = "update USER set password='" + newPassword + "' where userId='" + userId + "'";

		// Chạy truy vấn
		try {
			if (execute(check).next()) { // Nếu tài khoản và mật khẩu "chính
											// chủ"
				update(query); // thì đổi
			} else
				return false; // ngược lại trả về sai!
		} catch (SQLException e) {
			e.printStackTrace();
			return false; // Xảy ra lỗi cũng cho về sai luôn!
		}
		return true;
	}

	public boolean isExist(String userId) {
		String query = "Select userId from USER where userId='" + userId + "'";
		try {
			if (execute(query).next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	// Xác định kiểu người dùng
	public String getUserType(String userId) {
		String utype = "user";
		if (isExist(userId)) {
			try {
				if (execute("select userId from STUDENT where userId='" + userId + "'").next()) {
					utype = "student";
				}
				if (execute("select userId from INSTRUCTOR where userId='" + userId + "'").next()) {
					utype = "instructor";
				}
				if (execute("select userId from MANAGER where userId='" + userId + "'").next()) {
					if ("instructor".equals(utype)) {
						// Người dùng vừa là giảng viên, vừa là giáo vụ
						utype = "instructorAsManager";
					} else {
						utype = "manager";
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return utype;
	}
}
