/**
 * 
 */
package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Model.BEAN.Course;
import Model.BEAN.CoursePlan;

/**
 * @author NguyenBaAnh
 *
 */
public class CourseDAO extends Database {

	public CourseDAO() {
		super();
	}

	public Course getTheNewestCourse() {
		// Tạo truy vấn lấy đợt hiện tại, tức là đợt có ngày bắt đầu mới nhất
		String query = "select * from COURSE where startDate=(select max(startDate) from COURSE)";
		Course thisCourse = new Course();
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				String courseId = resultSet.getString("courseId");
				String courseName = resultSet.getString("courseName");
				String startDate = resultSet.getString("startDate");
				String stopDate = resultSet.getString("stopDate");
				String beginRegistrationTime = resultSet.getString("beginRegistrationTime");
				String endRegistrationTime = resultSet.getString("endRegistrationTime");
				boolean actived = Boolean.parseBoolean(resultSet.getString("actived"));
				byte progressRate = Byte.parseByte(resultSet.getString("progressRate"));
				int numberOfInstructors = Integer.parseInt(resultSet.getString("numberOfInstructors"));
				int numberOfStudents = Integer.parseInt(resultSet.getString("numberOfStudents"));
				// Lấy dữ liệu kế hoạch
				ArrayList<CoursePlan> plan = new ArrayList<CoursePlan>();// FIXME
				ResultSet rs = execute("select * from COURSE_PLAN where courseId='" + courseId + "'");
				while (rs.next()) {
					byte order = Byte.parseByte(rs.getString("planOrder"));
					String beginReportDate = rs.getString("beginReportDate");
					String endReportDate = rs.getString("endReportDate");
					plan.add(new CoursePlan(courseId, order, beginReportDate, endReportDate));
				}
				thisCourse = new Course(courseId, courseName, actived, startDate, stopDate, beginRegistrationTime,
						endRegistrationTime, progressRate, numberOfStudents, numberOfInstructors, plan);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return thisCourse;
	}

	/**
	 * Đổi thời gian đăng kí
	 * 
	 * @param beginRegistrationTime
	 * @param endRegistrationTime
	 * @param courseId
	 * @return
	 */
	public boolean changeRegistrationTime(String beginRegistrationTime, String endRegistrationTime, String courseId) {
		// Tạo truy vấn để cập nhật dữ liệu
		String query = "update COURSE set beginRegistrationTime='" + beginRegistrationTime + "', endRegistrationTime='"
				+ endRegistrationTime + "' where courseId = '" + courseId + "'";
		// Chạy truy vấn cập nhật
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false; // Cập nhật thất bại thì trả lui
		}
		return true;
	}

	/**
	 * Lấy thông tin đầy đủ 1 đợt
	 * 
	 * @param courseId
	 * @return
	 */
	public Course getCourseById(String courseId) {
		String query = "select * from COURSE where courseId = '" + courseId + "'";
		Course thisCourse = new Course();
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				String _courseId = resultSet.getString("courseId");
				String courseName = resultSet.getString("courseName");
				String startDate = resultSet.getString("startDate");
				String stopDate = resultSet.getString("stopDate");
				String beginRegistrationTime = resultSet.getString("beginRegistrationTime");
				String endRegistrationTime = resultSet.getString("endRegistrationTime");
				boolean actived = Boolean.parseBoolean(resultSet.getString("actived"));
				byte progressRate = Byte.parseByte(resultSet.getString("progressRate"));
				int numberOfInstructors = Integer.parseInt(resultSet.getString("numberOfInstructors"));
				int numberOfStudents = Integer.parseInt(resultSet.getString("numberOfStudents"));
				// Lấy dữ liệu kế hoạch
				ArrayList<CoursePlan> plan = new ArrayList<CoursePlan>(); // FIXME
				ResultSet rs = execute("select * from COURSE_PLAN where courseId='" + _courseId + "'");
				while (rs.next()) {
					byte order = Byte.parseByte(rs.getString("planOrder"));
					String beginReportDate = rs.getString("beginReportDate");
					String endReportDate = rs.getString("endReportDate");
					plan.add(new CoursePlan(_courseId, order, beginReportDate, endReportDate));
				}
				thisCourse = new Course(_courseId, courseName, actived, startDate, stopDate, beginRegistrationTime,
						endRegistrationTime, progressRate, numberOfStudents, numberOfInstructors, plan);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return thisCourse;
	}

	/**
	 * Lấy danh sách bao gồm mã + tên đợt
	 * 
	 * @return
	 */
	public ArrayList<Course> getCourseIdAndName() {
		String query = "select courseId, courseName, actived from COURSE order by startDate desc";
		try {
			ArrayList<Course> basicCourseList = new ArrayList<Course>();
			ResultSet resultSet = execute(query);
			while (resultSet.next()) {
				Course temp = new Course(resultSet.getString("courseId"), resultSet.getString("courseName"),
						Boolean.parseBoolean(resultSet.getString("actived")));
				basicCourseList.add(temp);
			}
			return basicCourseList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean changeProgressRate(String courseId, byte progressRate) {
		String query = "update COURSE set progressRate=" + progressRate + " where courseId='" + courseId + "'";
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Tạo mới đợt
	 * 
	 * @param newCourse
	 *            đợt mới
	 * @return TRUE nếu thành công. FALSE nếu thất bại.
	 */
	public boolean createNewCourse(Course newCourse) {
		String query = "insert into COURSE(courseId, courseName, actived, startDate, stopDate, beginRegistrationTime, endRegistrationTime, progressRate) values('"
				+ newCourse.getCourseId() + "','" + newCourse.getCourseName() + "','" + newCourse.isActived() + "','"
				+ newCourse.getStartDate() + "','" + newCourse.getStopDate() + "','"
				+ newCourse.getBeginRegistrationTime() + "','" + newCourse.getEndRegistrationTime() + "', "
				+ newCourse.getProgressRate() + ")";
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		;
		return true;
	}

	/**
	 * Ngừng đợt hoạt động
	 * 
	 * @param courseId
	 * @return
	 */
	public boolean stopCourseById(String courseId) {
		// ĐỔi trạng thái trong bảng COURSE
		String query = "update COURSE set actived=false where courseId='" + courseId + "'";
		// Ngừng kích hoạt giảng viên và sinh viên và đồ án liên quan đến đợt
		String query2 = "update COURSE_USER set actived=false where courseId='" + courseId + "'";
		// Ngừng tất cả đồ án của đợt
		String query3 = "update PROJECT set actived=false where courseId='" + courseId + "'";
		// FIXME - những thứ liên quan khác
		try {
			update(query);
			update(query2);
			update(query3);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Cập nhật lịch báo cáo và nộp đồ án của đợt
	 * 
	 * @param update
	 */
	public boolean changeCoursePlan(CoursePlan coursePlan, String update) {
		String query = "";
		if ("yes".equals(update)) {
			query = "update COURSE_PLAN set beginReportDate = '" + coursePlan.getBeginReportDate()
					+ "', endReportDate='" + coursePlan.getEndReportDate() + "' where (courseId='"
					+ coursePlan.getCourseId() + "' and planOrder='" + coursePlan.getOrder() + "')";
		} else {
			query = "insert into COURSE_PLAN(courseId, planOrder, beginReportDate, endReportDate) " + "values('"
					+ coursePlan.getCourseId() + "', " + coursePlan.getOrder() + " , '"
					+ coursePlan.getBeginReportDate() + "', '" + coursePlan.getEndReportDate() + "')";
		}
		try {
			update(query);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}
