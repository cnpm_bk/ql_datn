/**
 * 
 */
package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Model.BEAN.Notice;

/**
 * @author NguyenBaAnh
 *
 */
public class NoticeDAO extends Database {

	/**
	 * 
	 */
	public NoticeDAO() {
		super();
	}

	public ArrayList<Notice> getAllNotices() {
		String query = "Select * from NOTICE order by time desc";
		try {
			ArrayList<Notice> notices = new ArrayList<Notice>();
			ResultSet resultSet = execute(query);
			while (resultSet.next()) {
				Notice temp = new Notice(resultSet.getString("noticeId"), resultSet.getString("userId"),
						resultSet.getString("courseId"), resultSet.getString("title"), resultSet.getString("content"),
						resultSet.getString("time"));
				notices.add(temp);
			}
			resultSet.close();
			return notices;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Notice> getUserNotices(String userId) {
		String query = "Select * from NOTICE where userId = '" + userId + "' order by time desc";
		try {
			ArrayList<Notice> notices = new ArrayList<Notice>();
			ResultSet resultSet = execute(query);
			while (resultSet.next()) {
				Notice temp = new Notice(resultSet.getString("noticeId"), resultSet.getString("userId"),
						resultSet.getString("courseId"), resultSet.getString("title"), resultSet.getString("content"),
						resultSet.getString("time"));
				notices.add(temp);
			}
			resultSet.close();
			return notices;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean addNewNotice(Notice notice) {
		String query = "insert into NOTICE(noticeId, userId, courseId, title, content, time) values(0, '"
				+ notice.getUserId() + "', '" + notice.getCourseId() + "', '" + notice.getTitle() + "', '"
				+ notice.getContent() + "', '" + notice.getTime() + "')";
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deteleNoticeByTitle(String title) {
		// Xóa thông báo mới nhất, có liên quan
		String query = "delete * from NOTICE where title='" + title + "' and time=(select max(time) from NOTICE)";
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
