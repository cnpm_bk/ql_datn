package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Model.BEAN.Instructor;
import Model.BEAN.QuantityTable;
import Model.BEAN.StudyField;

public class InstructorDAO extends Database {
	Instructor instructor;
	UserDAO userDAO;

	public InstructorDAO() {
		instructor = new Instructor();
		userDAO = new UserDAO();
	}

	/**
	 * Xác nhận Mã người dùng nhập vào có phải là của 1 giảng viên hay không?
	 * 
	 * @param userId
	 *            Mã người dùng
	 * @return true nếu là của giảng viên, ngược lại false
	 */
	public boolean validateInstructorById(String userId) {
		try {
			if (execute("Select userId from INSTRUCTOR where userId='" + userId + "'").next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Lấy toàn bộ thông tin liên quan đến giảng viên theo Mã người dùng
	 * 
	 * @param userId
	 *            Mã người dùng (Mã giảng viên)
	 * @return Toàn bộ thông tin của giảng viên đã chọn
	 */
	public Instructor getInstructorById(String userId) {
		String query = "Select * from USER, INSTRUCTOR where INSTRUCTOR.userId='" + userId
				+ "' AND USER.userId=INSTRUCTOR.userId";
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) { // Dữ liệu...
				// Từ bảng USER
				String instructorId = resultSet.getString("INSTRUCTOR.userId");
				String fullName = resultSet.getString("fullName");
				String gender = resultSet.getString("gender");
				String dateOfBirth = resultSet.getString("dateOfBirth");
				String email = resultSet.getString("email");
				String phoneNumber = resultSet.getString("phoneNumber");
				String address = resultSet.getString("address");
				String password = resultSet.getString("password");
				// Kiểm tra kiểu người dùng
				String userType = userDAO.getUserType(instructorId);
				// Từ bảng INSTRUCTOR : USER
				String department = resultSet.getString("department");
				String organization = resultSet.getString("organization");
				String levelOfStudy = resultSet.getString("levelOfStudy");
				// Dữ liệu liên kết bảng StudyField
				ArrayList<StudyField> studyField = new ArrayList<StudyField>();
				String getStudyFields = "select FIELD.fieldId, FIELD.fieldName, FIELD.fieldDetail from INSTRUCTOR, STUDY_FIELD, FIELD where INSTRUCTOR.userId='"
						+ instructorId + "' and STUDY_FIELD.instructorId='" + instructorId
						+ "' and STUDY_FIELD.fieldId=FIELD.fieldId";
				ResultSet rs = execute(getStudyFields);
				while (rs.next()) {
					int fieldId = Integer.parseInt(rs.getString("FIELD.fieldId"));
					String fieldName = rs.getString("FIELD.fieldName");
					String fieldDetail = rs.getString("FIELD.fieldDetail");
					studyField.add(new StudyField(fieldId, fieldName, fieldDetail));
				}
				// Dữ liệu liên kết bảng QuantityTable số lượng SV đc hướng dẫn
				ArrayList<QuantityTable> qTable = new ArrayList<QuantityTable>();
				rs = execute("select * from QUANTITY_TABLE where instructorId='" + instructorId + "'");
				while (rs.next()) {
					//int tableId = Integer.parseInt(rs.getString("tableId"));
					String courseId = rs.getString("courseId");
					byte maxNumberOfStudents = Byte.parseByte(rs.getString("maxNumberOfStudents"));
					byte currentNumberOfStudents = Byte.parseByte(rs.getString("currentNumberOfStudents"));
					qTable.add(new QuantityTable(instructorId, courseId, maxNumberOfStudents,
							currentNumberOfStudents));
				}
				// Đẩy tất cả dữ liệu vào instructor
				instructor = new Instructor(userId, fullName, gender, address, email, password, userType, dateOfBirth,
						phoneNumber, department, organization, levelOfStudy, studyField, qTable);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instructor; // trả về
	}

	/**
	 * Cập nhật thông tin cho 1 giảng viên nào đó
	 * 
	 * @param instructor
	 *            Chứa các thông tin có thể sửa đổi của 1 giảng viên nào đó, sử
	 *            dụng hàm dựng dạng khuyết.
	 * @return true Nếu cập nhật thành công, false nếu cập nhật thất bại.
	 */
	public boolean updateInformation(Instructor instructor) {
		// Cập nhật bảng USER
		String query = "update USER set fullName='" + instructor.getFullName() + "', dateOfBirth='"
				+ instructor.getDateOfBirth() + "', phoneNumber='" + instructor.getPhoneNumber() + "', address='"
				+ instructor.getAddress() + "' where userId='" + instructor.getUserId() + "'";
		// Cập nhật bảng INSTRUCTOR
		String query2 = "update INSTRUCTOR set levelOfStudy='" + instructor.getLevelOfStudy() + "' where userId='"
				+ instructor.getUserId() + "'";
		// Cập nhật bảng STUDY_FIELD - hướng nghiên cứu
		// String query3 = "update STUDY_FIELD set";
		// FIXME
		try {
			update(query);
			update(query2);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Lấy toàn bộ danh sách giảng viên cùng toàn bộ thông tin liên quan của 1
	 * đợt nào đó
	 * 
	 * @param courseId
	 *            Mã đợt
	 * @return danh sách các giảng viên
	 */
	public ArrayList<Instructor> getInstructorListByCourseId(String courseId) {
		String query = "select * from COURSE_USER where courseId='" + courseId + "'";
		try {
			ResultSet rs = execute(query);
			ArrayList<Instructor> instructors = new ArrayList<Instructor>();
			while (rs.next()) {
				String instructorId = rs.getString("userId");
				if (this.validateInstructorById(instructorId)) {
					Instructor instructor = this.getInstructorById(instructorId);
					instructors.add(instructor);
				}
			}
			return instructors;
		} catch (SQLException e) {
			// Có lỗi thì bỏ qua và tiếp tục
			e.printStackTrace(); // In ra console
		}
		return null;
	}

	/**
	 * Lấy toàn bộ danh sách giảng viên đang có trong hệ thống cùng toàn bộ
	 * thông tin liên quan.
	 * 
	 * @return danh sách toàn bộ giảng viên
	 */
	public ArrayList<Instructor> getAllInstructors() {
		// Truy vấn lấy danh sách toàn bộ Mã giảng viên
		String query = "select userId from INSTRUCTOR";
		try {
			ArrayList<Instructor> instructorList = new ArrayList<Instructor>();
			ResultSet rs = execute(query);
			while (rs.next()) {
				// Tách từng Mã giảng viên
				String userId = rs.getString("userId");
				// Gọi phương thức lấy giảng viên
				Instructor instructor = this.getInstructorById(userId);
				// Đẩy vào List
				instructorList.add(instructor);
			}
			return instructorList;
		} catch (SQLException e) {
			// Có lỗi thì bỏ qua và tiếp tục
			e.printStackTrace(); // In ra console
		}
		return null;
	}

	/**
	 * Thay đổi số lượng sinh viên tối đa được phép hướng dẫn của 1 giảng viên
	 * 
	 * @param courseId
	 *            mã đợt
	 * @param instructorId
	 *            mã GB
	 * @param maxNumberOfStudents
	 *            số lượng sinh viên đặt vào
	 * @return true nếu thỏa mãn điều kiện: số đặt vào phải tối thiểu bằng số
	 *         lượng sinh viên đã đăng kí, không được thấp hơn, ngược lại trả về
	 *         false.
	 */
	public boolean changeMaxNumberOfStudents(String courseId, String instructorId, byte maxNumberOfStudents) {
		String query = "update QUANTITY_TABLE set maxNumberOfStudents=" + maxNumberOfStudents + " where courseId='"
				+ courseId + "' and instructorId='" + instructorId + "' and currentNumberOfStudents <= "
				+ maxNumberOfStudents;
		try {
			update(query);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
