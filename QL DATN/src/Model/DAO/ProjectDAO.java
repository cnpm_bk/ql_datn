/**
 * 
 */
package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.BEAN.Progress;
import Model.BEAN.Project;
import Model.BEAN.User;

/**
 * @author NguyenBaAnh, NgoTruongPhamQuang
 *
 */
public class ProjectDAO extends Database {

	public ProjectDAO() {
		super();
	}

	/**
	 * Đăng kí đồ án dành cho sinh viên của 1 đợt
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param instructorId
	 *            mã giảng viên đã chọn
	 * @param studentId
	 *            mã sinh viên (PK2)
	 * @return true nếu đăng kí thành công, ngược lại false.
	 */
	public boolean registerProject(String courseId, String instructorId, String studentId) {
		String query = "update QUANTITY_TABLE set currentNumberOfStudents = (currentNumberOfStudents+1) where (courseId='"
				+ courseId + "' and instructorId='" + instructorId
				+ "' and currentNumberOfStudents < maxNumberOfStudents)";
		String query2 = "insert into PROJECT(instructorId, studentId, courseId, actived) values('" + instructorId
				+ "','" + studentId + "','" + courseId + "',false)";
		try {
			update(query); // Cập nhật bảng số lượng sinh viên của giảng viên
			update(query2); // Nếu phù hợp, thì chạy truy vấn số 2
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false; // có lỗi, dừng ngay lập tức
		}
	}

	/**
	 * Lấy mã giảng viên đang hướng dẫn 1 sinh viên trong 1 đợt nào đó
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param studentId
	 *            mã sinh viên (PK2)
	 * @return mã giảng viên nếu thành công, không có thì trả về NULL
	 */
	public String getInstructorId(String courseId, String studentId) {
		String query = "select instructorId from PROJECT where courseId='" + courseId + "' and studentId='" + studentId
				+ "'";
		ResultSet rs = null;
		try {
			rs = execute(query);
			if (rs.next()) {
				return rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	/**
	 * Hủy đồ án / kết quả đăng kí
	 * 
	 * @param courseId
	 *            mã đợt (PK1)
	 * @param instructorId
	 *            mã giảng viên
	 * @param studentId
	 *            mã sinh viên (PK2)
	 * @return true nếu hủy thành công, false nếu thất bại
	 */
	public boolean removeProject(String courseId, String instructorId, String studentId) {
		String query = "delete * from PROJECT where instructorId='" + instructorId + "' and studentId='" + studentId
				+ "' and courseId='" + courseId + "'";
		String query2 = "update QUANTITY_TABLE set currentNumberOfStudents = (currentNumberOfStudents-1) where (courseId='"
				+ courseId + "' and instructorId='" + instructorId + "')";
		try {
			update(query);
			update(query2);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Lấy toàn bộ danh sách đồ án mà sinh viên đã đăng kí trong 1 đợt
	 * 
	 * @param courseId
	 * @return
	 */
	public ArrayList<Project> getProjectListByCourseId(String courseId) {
		String query = "select * from PROJECT where courseId = '" + courseId + "'";
		ResultSet rs = null;
		try {
			rs = execute(query);
			ArrayList<Project> projectList = new ArrayList<Project>();
			while (rs.next()) {
				Project p = new Project(rs.getString("instructorId"), rs.getString("studentId"),
						rs.getString("courseId"), Boolean.parseBoolean(rs.getString("actived")),
						rs.getString("projectTitle"), rs.getString("projectDescription"),
						Byte.parseByte(rs.getString("currentRateOfProgress")),
						Boolean.parseBoolean(rs.getString("approvedByManager")));
				projectList.add(p);
			}
			return projectList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Lấy toàn bộ danh sách đồ án của một sinh viên trong toàn hệ thống
	 * 
	 * @param studentId
	 * @return
	 */
	public ArrayList<Project> getProjectListByStudentId(String studentId) {
		String query = "select * from PROJECT where studentId='" + studentId + "'";
		ArrayList<Project> studentList = null;
		try {
			ResultSet rs = execute(query);
			studentList = new ArrayList<Project>();
			while (rs.next()) {
				Project p = new Project(rs.getString("instructorId"), studentId, rs.getString("courseId"),
						Boolean.parseBoolean(rs.getString("actived")), rs.getString("projectTitle"),
						rs.getString("projectDescription"), Byte.parseByte(rs.getString("currentRateOfProgress")),
						Boolean.parseBoolean(rs.getString("approvedByManager")));
				// Lấy bảng tiến độ của mỗi đồ án
				String query2 = "select * from PROGRESS where courseId='" + p.getCourseId() + "' and studentId='"
						+ studentId + "'";
				ResultSet rs2 = execute(query2);
				ArrayList<Progress> progresses = new ArrayList<Progress>();
				while (rs2.next()) {
					Progress progress = new Progress(p.getCourseId(), studentId,
							Byte.parseByte(rs2.getString("rateOfProgress")), rs2.getString("reportDate"),
							rs2.getString("reportTitle"), rs2.getString("reportContent"), rs2.getString("optionalLink"),
							Boolean.parseBoolean(rs2.getString("approvedByStudent")),
							Boolean.parseBoolean(rs2.getString("approvedByInstructor")),
							Boolean.parseBoolean(rs2.getString("approvedByManager")));
					progresses.add(progress);
				}
				p.setProgresses(progresses);
				studentList.add(p);
			}
			return studentList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return studentList;
	}

	/**
	 * Cập nhật thông tin về đồ án
	 * 
	 * @param projectInfo
	 * @return
	 */
	public boolean updateProjectInformation(Project projectInfo) {
		String query = "update PROJECT set projectTitle='" + projectInfo.getProjectTitle() + "', projectDescription='"
				+ projectInfo.getProjectDescription() + "' where studentId='" + projectInfo.getStudentId()
				+ "' and courseId='" + projectInfo.getCourseId() + "'";
		try {
			update(query);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Cập nhật tiến độ
	 * 
	 * @param progress
	 * @return
	 */
	public boolean updateProgress(Progress progress) {
		// Cập nhật bảng tiến độ
		String query = "insert into PROGRESS(studentId, courseId, rateOfProgress, reportDate, reportTitle, "
				+ "reportContent, optionalLink, approvedByStudent) " + "values('" + progress.getStudentId() + "', '"
				+ progress.getCourseId() + "', " + progress.getRateOfProgress() + ", '" + progress.getReportDate()
				+ "', '" + progress.getReportTitle() + "', '" + progress.getReportContent() + "', '"
				+ progress.getOptionalLink() + "', " + progress.isApprovedByStudent() + ")";
		// Cập nhật bảng đồ án
		String query2 = "update PROJECT set currentRateOfProgress = (currentRateOfProgress+1) where studentId='"
				+ progress.getStudentId() + "' and courseId='" + progress.getCourseId() + "'";
		try {
			update(query);
			// Kiểm tra xem số lần nộp tiến độ bằng bao nhiêu
			byte progressRate = Byte.parseByte(String.valueOf(
					execute("select progressRate from COURSE where courseId='" + progress.getCourseId() + "'")));
			if (progress.getRateOfProgress() < progressRate) {
				update(query2);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	// ==========================================================
	// Quang edit from here

	/**
	 * Lấy đồ án trong một đợt hướng dẫn của sinh viên
	 *
	 * @param studentId
	 * @param courseId
	 * @return
	 */
	public Project getProjectByStudentIdAndCourseId(String studentId, String courseId) {
		String query = "SELECT * from PROJECT where courseId='" + courseId + "' and studentId='" + studentId + "'";
		Project project = new Project();
		try {
			ResultSet resultSet = execute(query);
			if (resultSet.next()) {
				String projectTitle = resultSet.getString("projectTitle");
				String projectDescription = resultSet.getString("projectDescription");
				boolean actived = Boolean.parseBoolean(resultSet.getString("actived"));
				byte currentRateOfProgress = Byte.parseByte(resultSet.getString("currentRateOfProgress"));
				boolean approvedByManager = Boolean.parseBoolean(resultSet.getString("approvedByManager"));
				String instructorId = resultSet.getString("instructorId");
				project = new Project(instructorId, studentId, courseId, actived, projectTitle, projectDescription,
						currentRateOfProgress, approvedByManager);
				return project;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	/**
	 * Lấy toàn bộ danh sách sinh viên trong một đợt hướng dẫn của một giảng viên
	 *
	 * @param instructorId
	 * @param courseId
	 * @return
	 */

	public ArrayList<User> getStudentByInstructorIdAndCourseId(String instructorId, String courseId) {
		List<String> studentIdString = new ArrayList<String>();
		List<User> studentList = new ArrayList<User>();
		String query1 = "select studentId from PROJECT where instructorId='" + instructorId + "' and courseId='"
				+ courseId + "' ORDER BY studentId ASC";
		try {
			ResultSet rs = execute(query1);
			while (rs.next()) {
				studentIdString.add(rs.getString("studentId"));
			}
			for (String studentId : studentIdString) {
				String query2 = "select * from USER where userId='" + studentId + "'";
				rs = execute(query2);
				while (rs.next()) {
					studentList.add(new User(rs.getString("userId"), rs.getString("fullName"), rs.getString("gender"),
							rs.getString("address"), rs.getString("email"), rs.getString("password"), "student",
							rs.getString("dateOfBirth"), rs.getString("phoneNumber")));
				}
			}
			return (ArrayList<User>) studentList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Thay đổi giảng viên hướng dẫn của một sinh viên trong một đợt
	 *
	 * @param studentId
	 * @param courseId
	 * @param newInstructorId
	 * @param oldInstructorId
	 * @return
	 */

	public boolean changeInstructorByStudentIdAndCourseId(String studentId, String courseId, String newInstructorId, String oldInstructorId) {
		System.out.println("New instructorId: " + newInstructorId);
		String query = "update PROJECT set instructorId ='" + newInstructorId + "' where studentId ='" + studentId
				+ "' and courseId ='" + courseId + "'";
		String query2 = "update QUANTITY_TABLE set currentNumberOfStudents=(currentNumberOfStudents+1) where instructorId='" + newInstructorId + "'";
		String query3 = "update QUANTITY_TABLE set currentNumberOfStudents=(currentNumberOfStudents-1) where instructorId='" + oldInstructorId + "'";
		try {
			if ((update(query) != 0) && (update(query2) != 0) && (update(query3) != 0)) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

}
