/**
 * 
 */
package Model.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Model.BEAN.StudyField;

/**
 * @author NguyenBaAnh
 *
 */
public class StudyFieldDAO extends Database {
	public StudyFieldDAO() {
		super();
	}

	public ArrayList<StudyField> getAllStudyFields() {
		String query = "select * from FIELD";
		try {
			ResultSet rs = execute(query);
			ArrayList<StudyField> studyFields = new ArrayList<StudyField>();
			while (rs.next()) {
				StudyField sf = new StudyField(Integer.parseInt(rs.getString("fieldId")), rs.getString("fieldName"),
						rs.getString("fieldDetail"));
				studyFields.add(sf);
			}
			return studyFields;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<Integer> getInstructorStudyFieldIds(String instructorId) {
		String query = "select * from STUDY_FIELD where instructorId='" + instructorId + "'";
		try {
			ResultSet rs = execute(query);
			ArrayList<Integer> ids = new ArrayList<Integer>();
			while (rs.next()) {
				ids.add(new Integer(rs.getString("fieldId")));
			}
			return ids;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean updateStudyFields(String instructorId, int[] chosenFields) {
		// TODO Auto-generated method stub
		// Xóa tất cả hướng nghiên cứu của giảng viên
		String query = "delete * from STUDY_FIELD where instructorId='" + instructorId + "'";
		try {
			update(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		// Cập nhật lại hướng mới
		for (int i = 0; i < chosenFields.length; i++) {
			String query2 = "insert into STUDY_FIELD(fieldId, instructorId) values(" + chosenFields[i] + ",'"
					+ instructorId + "')";
			try {
				update(query2);
			} catch (SQLException e) {
				e.printStackTrace();				
				return false;
			}
		}
		return true;
	}

}
