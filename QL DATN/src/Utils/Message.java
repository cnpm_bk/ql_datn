/**
 * Thông điệp thông báo
 */
package Utils;

/**
 * @author NguyenBaAnh
 *
 */
public class Message {
	String redirectURL;
	String action;
	String message;

	/**
	 * Hỗ trợ hiển thị thông báo
	 * @param redirectURL	Địa chỉ quay về
	 * @param action		Tên hành động của địa chỉ
	 * @param message		Thông điệp chi tiết, mã HTML
	 */
	public Message(String redirectURL, String action, String message) {
		super();
		this.redirectURL = redirectURL;
		this.message = message;
		this.action = action;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
