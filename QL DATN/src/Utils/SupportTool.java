/**
 * Các công cụ tiện ích hỗ trợ về kiểm tra ràng buộc, ngày giờ, thao tác tệp v.v...
 */
package Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * Lớp hỗ trợ chứa các công cụ tiện ích.
 * 
 * @author NguyenBaAnh
 *
 */
public class SupportTool {
	/**
	 * Kiểm tra NULL
	 * 
	 * @param obj
	 *            Đối tượng nào đó
	 * @return true nếu obj==null, ngược lại false
	 */
	public boolean checkNull(Object obj) {
		if (obj == null) {
			return true;
		}
		return false;
	}

	/**
	 * Kiểm tra 1 dãy các chuỗi xem có cái nào rỗng không
	 * 
	 * @param str
	 *            Danh sách các chuỗi cần kiểm tra
	 * @return true nếu tồn tại ít nhất 1 chuỗi trống, false nếu tất cả các
	 *         chuỗi đều không trống
	 */
	public boolean checkEmptyStrings(String... str) {
		for (int i = 0; i < str.length; i++) {
			if ("".equals(str)) {
				return true;
			} else {
				continue;
			}
		}
		return false;
	}

	/**
	 * Kiểm tra sự hợp lệ của chuỗi Timestamp
	 * 
	 * @param stamp
	 *            Danh sách các Timestamp cần kiểm tra
	 * @return true nếu tất cả các chuỗi Timestamp đều đúng định dạng
	 *         "yyyy-mm-dd hh-ii-ss[.ffffff]", false nếu tồn tại ít nhất 1 chuỗi
	 *         không đúng định dạng
	 */
	public boolean checkTimeStamp(String... stamp) {
		try {
			for (int i = 0; i < stamp.length; i++) {
				Timestamp.valueOf(stamp[i]);
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false; // thất bại do không đúng định dạng!
		}
		return true;
	}

	/**
	 * Kiểm tra tương quan giữa 2 Timestamps
	 * 
	 * @param fisrtTimestamp
	 *            Thời điểm thứ nhất
	 * @param secondTimestamp
	 *            Thời điểm thứ 2
	 * @return true nếu cả 2 đều đúng định dạng và thời điểm fisrtTimestamp ở
	 *         trước thời điểm secondTimestamp. Ngược lại false.
	 */
	public boolean checkValidTime(String fisrtTimestamp, String secondTimestamp) {
		if (checkTimeStamp(fisrtTimestamp, secondTimestamp)) {
			Timestamp t1, t2;
			t1 = Timestamp.valueOf(fisrtTimestamp);
			t2 = Timestamp.valueOf(secondTimestamp);
			if (t1.before(t2)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Kiểm tra 1 chuỗi xem có phải số nguyên không
	 * 
	 * @param num
	 *            Một chuỗi nào đó
	 * @return true nếu chuỗi thể hiện số nguyên, ngược lại false
	 */
	public boolean checkInt(String num) {
		if (checkNull(num)) {
			return false;
		}
		try {
			Integer.parseInt(num);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Lấy ngày giờ hiện tại của hệ thống
	 */
	public Timestamp getCurrentDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return Timestamp.valueOf(dateFormat.format(date).toString());
	}
	
	/**
	 * Đọc file excel
	 */
	public Vector readExcelFile(String url) {
		Vector cellVectorHolder = new Vector();
        try {
            FileInputStream myInput = new FileInputStream(url);
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator rowIter = mySheet.rowIterator();
            while (rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                Iterator cellIter = myRow.cellIterator();
                Vector cellStoreVector = new Vector();
                while (cellIter.hasNext()) {
                    HSSFCell myCell = (HSSFCell) cellIter.next();
                    cellStoreVector.addElement(myCell);
                }
                cellVectorHolder.addElement(cellStoreVector);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cellVectorHolder;
	}
	
	/**
	 * Chuyển file excel ra String
	 */
	public String convertCellDataToString(Vector dataHolder) {
        //LinkedList<String> row = new LinkedList<>;
		StringBuilder sb = new StringBuilder();
        Vector row = new Vector();
        for (int i = 0; i < dataHolder.size(); i++) {
            Vector cellStoreVector = (Vector) dataHolder.elementAt(i);
            for (int j = 0; j < cellStoreVector.size(); j++) {
                HSSFCell myCell = (HSSFCell) cellStoreVector.elementAt(j);
                String stringCellValue = myCell.toString();
                sb.append(stringCellValue + "\t");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
	
	/**
	 * Lấy địa chỉ thư mục WebContent
	 */
	public String getBaseURL(HttpServletRequest request) throws ServletException, IOException {
		return request.getServletContext().getRealPath("/");
	}
}
