/**
 * Mã phản hồi hỗ trợ cho các lệnh rẽ nhánh
 */
package Utils;

public enum ResponseCode {
	SUCCESS, // Thành công
	FAILED,  // Thất bại
	EMPTY, // Dữ liệu rỗng
	NULL, // Dữ liệu không tồn tại
	NOT_ALLOWED, // Không cho phép
	THE_SAME_PASSWORD, // Mật khẩu mới giống mật khẩu cũ
	PASSWORD_NOT_MATCH, // Xác nhận mật khẩu không đúng
	TIME_NOT_VALID, // Định dạng ngày không đúng hoặc sai logic về ngày (ngày kết thúc ở trước ngày bắt đầu)
	ILLOGICAL,
	
}
